<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DashboardAdminTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testDashboardAdmin(){

        //load cenario
        $this->cenario();

        //testAdminLogin
        $this->loginAsAdmin();

        //testAdminCreateUsers
        $this->createUser();
        $this->cancelCreateUser();

        //testAdminEditUsers
        $this->editUser();
        $this->editUserProfile();

        //testAdminDeleteUsers
        $this->deleteUser();

        //testAdminBlockUsers
        $this->blockUnblockUser();

        //testAdminResetPassword
        $this->resetUserPassword();
    }

    public function cenario(){

        //Administradores = 2 => id's = [1,2]

        factory(User::class)->create([
            'name' => "Admin Um",
            'email' => "admin1@mail.pt",
            'password' => password_hash('admin123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'admin' => '1',
            'role' => '0',
            'activated' => '1'
        ]);

        factory(User::class)->create([
            'name' => "Admin Dois",
            'email' => "admin2@mail.pt",
            'password' => password_hash('admin123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'admin' => '1',
            'role' => '0',
            'activated' => '1'
        ]);

        //Diretores = 1+3 => id's = [3,4,5,6]

        factory(User::class)->create([
            'name' => "Diretor",
            'email' => "diretor@mail.pt",
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '1',
            'activated' => '1'
        ]);

        factory(User::class, 3)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '1',
            'activated' => '1'
        ]);

        //Gestores = 1+3 => id's = [7,8,9,10]

        factory(User::class)->create([
            'name' => "Gestor",
            'email' => "gestor@mail.pt",
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '2',
            'activated' => '1'
        ]);

        factory(User::class, 3)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '2',
            'activated' => '1'
        ]);

        //Utilizadores = 1+3 => id's = [11,12,13,14,15]

        factory(User::class)->create([
            'name' => "Utilizador",
            'email' => "utilizador@mail.pt",
            'password' => password_hash('secret', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '3',
            'activated' => '1'
        ]);

        factory(User::class, 4)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '3',
            'activated' => '1'
        ]);

    }

    public function loginAsAdmin(){
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->type('email', 'admin1@mail.pt')
                ->type('password', 'admin123')
                ->press('Login')
                ->assertPathIs('/home')
                ->assertSee('Lista de Utilizadores');
        });
    }

    public function createUser(){
        $this->browse(function (Browser $browser){
            $browser->press('Adicionar Utilizador')
                ->type('nome', 'Utilizador Teste')
                ->type('email', '2131219@my.ipleiria.pt')
                ->keys('#inputBirthDate', '01031995')
                ->select('sexo', 'M')
                ->type('morada', 'morada teste')
                ->select('tipo_de_conta', '3')
                ->select('pode_publicar_no_facebook', '0')
                ->press('Guardar')
                ->assertSee('Utilizador criado com sucesso. Foi enviado uma notificação para o email do novo utilizador para activação da sua conta.')
                ->clickLink('2')
                ->assertSee('2131219@my.ipleiria.pt')
                ->assertSee('Não ativa')
                ->clickLink('InPulsar Intranet')
                ->assertPathIs('/home');
        });
        $this->assertDatabaseHas('users', ['name' => 'Utilizador Teste']);
    }

    public function cancelCreateUser(){
        $this->browse(function (Browser $browser){
            $browser->press('Adicionar Utilizador')
                ->type('nome', 'Utilizador Teste')
                ->type('email', '2131219@my.ipleiria.pt')
                ->select('sexo', 'M')
                ->type('morada', 'morada teste')
                ->select('tipo_de_conta', '3')
                ->clickLink('Voltar')
                ->assertPathIs('/home');
        });
    }

    public function deleteUser(){
        $this->browse(function (Browser $browser) {
            $browser->press('#detalhes5')
                ->assertSee('Apagar')
                ->press('Apagar')
                ->pause(1000)
                ->assertSee('Confirme caso pretenda apagar os dados do utilizador.')
                ->press('Confirmar Apagar')
                ->pause(1000)
                ->assertSee('Utilizador apagado com sucesso')
                ->assertMissing('#detalhes5')
                ->assertPathIs('/home');
        });
        $this->assertSoftDeleted('users', ['id' => '5']);
    }

    public function editUser(){
        $this->browse(function (Browser $browser){
            $browser->press('#detalhes4')
                ->assertSee('Editar')
                ->press('Editar')
                ->assertSee('Editar Detalhes do Utilizador')
                ->type('nome', 'Utilizador Teste')
                ->press('Guardar')
                ->assertSee('Utilizador actualizado com sucesso.')
                ->assertSee('Utilizador Teste')
                ->press('Editar')
                ->type('nome', 'Utilizador Teste Dois')
                ->clickLink('Voltar')
                ->assertDontSee('Utilizador actualizado com sucesso.')
                ->assertDontSee('Utilizador Teste Dois')
                ->clickLink('Voltar')
                ->assertPathIs('/home');
        });
    }

    public function editUserProfile(){
        $this->browse(function (Browser $browser){
            $browser->press('#detalhes1')
                ->assertSee('Editar os meus dados')
                ->press('Editar os meus dados')
                ->assertSee('Editar Detalhes do Utilizador')
                ->type('nome', 'Admin Teste')
                ->press('Guardar')
                ->assertSee('Perfil actualizado com sucesso.')
                ->assertSee('Admin Teste')
                ->press('Editar')
                ->type('nome', 'Admin Teste Dois')
                ->clickLink('Voltar')
                ->assertDontSee('Perfil actualizado com sucesso.')
                ->assertDontSee('Admin Teste Dois')
                ->clickLink('Voltar')
                ->assertPathIs('/home')
                ->assertSee('Admin Teste')
                ->click('.dropdown')
                ->clickLink('Ver Perfil')
                ->assertSee('Editar os meus dados')
                ->press('Editar os meus dados')
                ->assertSee('Editar Detalhes do Utilizador')
                ->type('nome', 'Admin Teste Tres')
                ->press('Guardar')
                ->assertSee('Perfil actualizado com sucesso.')
                ->assertSee('Admin Teste Tres')
                ->assertPathIs('/show-profile')
                ->clickLink('Voltar')
                ->assertPathIs('/home')
                ->assertSee('Admin Teste Tres')
                ->click('.dropdown')
                ->clickLink('Ver Perfil')
                ->clickLink('Voltar')
                ->assertPathIs('/home');
        });
    }

    public function blockUnblockUser(){
        $this->browse(function (Browser $browser){
            $browser->press('#actions4')
                ->press('#block4')
                ->assertMissing('#block4')
                ->press('#actions4')
                ->press('#unblock4')
                ->assertMissing('#unblock4')
                ->assertPathIs('/home');
        });
    }

    public function resetUserPassword(){
        $this->browse(function (Browser $browser){
            $browser->press('#detalhes4')
                ->assertSee('Reinicializar Password')
                ->press('Reinicializar Password')
                ->assertSee('Detalhes do Utilizador')
                ->assertSee('Foi enviado email de reset da password ao utilizador.')
                ->clickLink('Voltar')
                ->assertPathIs('/home');
        });
    }

}
