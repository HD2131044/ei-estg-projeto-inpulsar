<?php

namespace Tests\Browser;

use App\Activity;
use App\Project;
use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DashboardUserTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testDashboardManager(){

        //load cenario
        $this->cenario();

        //ManagerLogin
        $this->loginAsUser();

        //testPosts
        $this->postCreate();
        $this->postEdit();
        $this->postDelete();
    }

    public function cenario(){

        //Administradores = 2 => id's = [1,2]

        factory(User::class)->create([
            'name' => "Admin Um",
            'email' => "admin1@mail.pt",
            'password' => password_hash('admin123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'admin' => '1',
            'role' => '0',
            'activated' => '1'
        ]);

        factory(User::class)->create([
            'name' => "Admin Dois",
            'email' => "admin2@mail.pt",
            'password' => password_hash('admin123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'admin' => '1',
            'role' => '0',
            'activated' => '1'
        ]);

        //Diretores = 1+3 => id's = [3,4,5,6]

        factory(User::class)->create([
            'name' => "Diretor",
            'email' => "diretor@mail.pt",
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '1',
            'activated' => '1'
        ]);

        factory(User::class, 3)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '1',
            'activated' => '1'
        ]);

        //Gestores = 1+3 => id's = [7,8,9,10]

        factory(User::class)->create([
            'name' => "Gestor",
            'email' => "gestor@mail.pt",
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '2',
            'activated' => '1'
        ]);

        factory(User::class, 3)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '2',
            'activated' => '1'
        ]);

        //Utilizadores = 1+3 => id's = [11,12,13,14,15]

        factory(User::class)->create([
            'name' => "Utilizador",
            'email' => "utilizador@mail.pt",
            'password' => password_hash('secret', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '3',
            'activated' => '1'
        ]);

        factory(User::class, 4)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '3',
            'activated' => '1'
        ]);

        factory(Project::class, 15)
            ->create()
            ->each(function($project) {
                //projectos em estado não concluído
                $project->status = 0;

                //associar o user do tipo Director que criou o projecto
                $project->users()->attach($project->creator_id);

                //o project pode ter vários users associados do tipo Gestor
                //considerando as nossas seed para Gestores => id's = [7,..,10]
                //projecto apenas vários Gestores já existentes
                $project->users()->attach(7);
                $project->users()->attach(8);
                $project->users()->attach(9);

                $project->save();
            });

        factory(Activity::class, 5)
            ->create()
            ->each(function($activity) {
                //actividades em estado não concluída
                $activity->status = 0;

                //considerando que uma activity está só associada a um project
                //considerando as nossas seed para Projects => id's = [1,..,5]
                //para garantir que o project com id =1 tem a actividade = 1 e 2 para correr sempre bem os testes
                if($activity->id == 1 || $activity->id == 2){
                    $projectData = Project::find(1);
                } else {
                    $projectData = Project::all()->random();
                }
                $activity->project_id = $projectData->id;
                $activity->creator_id = $projectData->users()->get()->random()->id;

                //activity pode ter vários users associados
                //associar o user do tipo Director/Manager que criou a activity ->CENÁRIO MANAGER ID 7
                $activity->users()->attach(7);

                //considerando que todos os Gestores do project tb estão associados à activity
                foreach ($projectData->users()->get() as $user) {
                    if($user)
                    {
                        if(!$activity->users()->find($user->id)){
                            $activity->users()->attach($user->id);
                        }
                    }
                }

                //considerando as nossas seed para Advanced + Simple users => id's = [11,..,15]
                //projecto com mais 2 participantes além dos gestores
                $participant1Id = User::all()->where('role', '=', '3')->random()->id;
                $participant2Id = User::all()->where('role', '=', '3')->random()->id;
                while($participant2Id == $participant1Id){
                    $participant2Id = User::all()->where('role', '=', '3')->random()->id;
                }
                if(!$activity->users()->find($participant1Id)){
                    $activity->users()->attach($participant1Id);
                }
                if(!$activity->users()->find($participant2Id)){
                    $activity->users()->attach($participant2Id);
                }
                $activity->save();

            });
    }

    public function loginAsUser(){
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->type('email', 'utilizador@mail.pt')
                ->type('password', 'secret')
                ->press('Login')
                ->assertPathIs('/home')
                ->assertSee('As Minhas Actividades');
        });
    }

    public function postCreate(){
        $this->browse(function (Browser $browser) {
            $browser->press('Detalhes')
            ->press('Adicionar Registo')
            ->type('#inputTitle', 'Registo Teste')
            ->keys('#inputDate', '02082018')
            ->type('#inputParticipants', '2')
            ->press('Guardar')
            ->pause(2000)
            ->assertSee('Registo criado com sucesso.')
            ->press('Adicionar Registo')
            ->type('#inputTitle', 'Registo Teste 2')
            ->keys('#inputDate', '02082018')
            ->type('#inputParticipants', '2')
            ->clickLink('Voltar')
            ->assertDontSee('Registo criado com sucesso.');
        });
        $this->assertDatabaseHas('posts', [
            'title' => 'Registo Teste'
        ]);
        $this->assertDatabaseMissing('posts', [
            'title' => 'Registo Teste 2'
        ]);
    }

    public function postEdit(){
        $this->browse(function (Browser $browser) {
            $browser->pause(2000)
                ->press('#detalhes1')
                ->press('Editar')
                ->type('#inputTitle', 'Registo Teste Editar')
                ->press('Guardar')
                ->pause(2000)
                ->assertSee('Registo actualizado com sucesso.')
                ->press('Editar')
                ->type('#inputTitle', 'Registo Teste Editar 2')
                ->clickLink('Voltar')
                ->assertDontSee('Registo actualizado com sucesso.');
        });
        $this->assertDatabaseHas('posts', [
            'id' => 1,
            'title' => 'Registo Teste Editar'
        ]);
        $this->assertDatabaseMissing('posts', [
            'id' => 1,
            'title' => 'Registo Teste Editar 2'
        ]);
    }

    public function postDelete(){
        $this->browse(function (Browser $browser) {
            $browser->press('Apagar')
                ->assertSee('Confirme caso pretenda apagar os dados do registo. Todos os anexos associados serão também apagados.')
                ->press('Confirmar Apagar')
                ->assertSee('Registo apagado com sucesso.');
        });
        $this->assertSoftDeleted('posts',[
            'id' => 1
        ]);
    }
}
