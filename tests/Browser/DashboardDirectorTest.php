<?php

namespace Tests\Browser;

use App\Activity;
use App\Project;
use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DashboardDirectorTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testDashboardDirector(){

        //load cenario
        $this->cenario();

        //DirectorLogin
        $this->loginAsDirector();

        //testDirectorCreateProjects
        $this->createProject();
        $this->cancelCreateProject();

        //testDirectorEditProjects
        $this->editProject();

        //testDirectorDeleteProjects
        $this->deleteProject();

        //testDirectorChangeProjectsState
        $this->projectToggleState();

        //testPosts
        $this->postCreate();
        $this->postEdit();
        $this->postDisable();
        $this->postDelete();

    }

    public function cenario(){

        //Administradores = 2 => id's = [1,2]

        factory(User::class)->create([
            'name' => "Admin Um",
            'email' => "admin1@mail.pt",
            'password' => password_hash('admin123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'admin' => '1',
            'role' => '0',
            'activated' => '1'
        ]);

        factory(User::class)->create([
            'name' => "Admin Dois",
            'email' => "admin2@mail.pt",
            'password' => password_hash('admin123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'admin' => '1',
            'role' => '0',
            'activated' => '1'
        ]);

        //Diretores = 1+3 => id's = [3,4,5,6]

        factory(User::class)->create([
            'name' => "Diretor",
            'email' => "diretor@mail.pt",
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '1',
            'activated' => '1'
        ]);

        factory(User::class, 3)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '1',
            'activated' => '1'
        ]);

        //Gestores = 1+3 => id's = [7,8,9,10]

        factory(User::class)->create([
            'name' => "Gestor",
            'email' => "gestor@mail.pt",
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '2',
            'activated' => '1'
        ]);

        factory(User::class, 3)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '2',
            'activated' => '1'
        ]);

        //Utilizadores = 1+3 => id's = [11,12,13,14,15]

        factory(User::class)->create([
            'name' => "Utilizador",
            'email' => "utilizador@mail.pt",
            'password' => password_hash('secret', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '3',
            'activated' => '1'
        ]);

        factory(User::class, 4)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '3',
            'activated' => '1'
        ]);

        factory(Project::class, 5)
            ->create()
            ->each(function($project) {
                //projecto em estado não concluído
                $project->status = 0;
                $project->submitted = 1;
                $project->times_submitted = 1;

                //associar o user do tipo Director que criou o projecto
                $project->users()->attach($project->creator_id);

                //o project pode ter vários users associados do tipo Gestor
                //considerando as nossas seed para Gestores => id's = [7,..,10]
                //projecto apenas vários Gestores
                //novos gestores
                $project->users()->saveMany(
                //users do tipo Gestor
                //projecto com 5 novos Gestores
                    factory(User::class, 5)->create([
                        'password' => password_hash('123123123', PASSWORD_DEFAULT),
                        'role' => '2',
                        'activated' => '1'
                    ]))->make();

                $project->save();
            });

        factory(Activity::class, 1)->create([
           'creator_id' => 2,
           'project_id' => 2,
           'status' => 0
        ]);

    }

    public function loginAsDirector(){
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->type('email', 'diretor@mail.pt')
                ->type('password', '123123123')
                ->press('Login')
                ->assertPathIs('/home')
                ->assertSee('Lista de Projectos');
        });
    }

    public function createProject(){
        $this->browse(function (Browser $browser){
            $browser->pause(3000)
                ->press('Adicionar Projecto')
                ->type('nome', 'Projeto Teste')
                ->type('descrição', 'ProjetoTeste')
                ->keys('#inputStartDate', '01082017')
                ->keys('#inputEndDate', '01102017')
                ->type('localização', 'morada teste')
                ->select('estado_do_projecto', '0')
                ->click('.sorting_1')
                ->press('Guardar')
                ->pause(1000)
                ->assertSee('Projecto criado com sucesso.')
                ->assertPathIs('/home');
        });
        $this->assertDatabaseHas('projects', ['name' => 'Projeto Teste']);
    }

    public function cancelCreateProject(){
        $this->browse(function (Browser $browser){
            $browser->pause(1000)
                ->press('Adicionar Projecto')
                ->type('nome', 'Projeto Teste')
                ->type('descrição', 'ProjetoTeste')
                ->keys('#inputStartDate', '01082017')
                ->keys('#inputEndDate', '01102017')
                ->type('localização', 'morada teste')
                ->select('estado_do_projecto', '0')
                ->clickLink('Voltar')
                ->assertDontSee('Projecto criado com sucesso.')
                ->assertPathIs('/home');
        });
    }

    public function editProject(){
        $this->browse(function (Browser $browser) {
            $browser->press('#detalhes1')
                ->clickLink('Voltar')
                ->press('#detalhes2')
                ->press('Editar')
                ->assertSee('Editar Detalhes do Projecto')
                ->type('#inputName', 'Projeto Teste')
                ->type('#inputDescription', 'ProjetoTeste')
                ->press('#addManager')
                ->pause(1000)
                ->press('#addManager')
                ->pause(1000)
                ->press('#addManager')
                ->pause(1000)
                ->press('Guardar')
                ->pause(2000)
                ->assertSee('Projecto actualizado com sucesso.')
                ->assertSee('Projeto Teste')
                ->press('Editar')
                ->assertSee('Editar Detalhes do Projecto')
                ->type('#inputName', 'Projeto Teste Dois')
                ->type('#inputDescription', 'ProjetoTesteDois')
                ->press('#removeManager')
                ->pause(1000)
                ->press('#removeManager')
                ->pause(1000)
                ->press('#removeManager')
                ->pause(1000)
                ->press('Guardar')
                ->pause(1000)
                ->assertSee('Projecto actualizado com sucesso.')
                ->assertSee('Projeto Teste Dois')
                ->press('Editar')
                ->assertSee('Editar Detalhes do Projecto')
                ->type('#inputName', 'Projeto Teste Tres')
                ->type('#inputDescription', 'ProjetoTeste')
                ->clickLink('Voltar')
                ->assertDontSee('Projecto actualizado com sucesso.')
                ->assertDontSee('Projeto Teste Tres')
                ->clickLink('Voltar')
                ->assertPathIs('/home');
        });
    }

    public function deleteProject(){
        $this->browse(function (Browser $browser) {
            $browser->press('#detalhes5')
                ->assertSee('Apagar')
                ->press('Apagar')
                ->assertSee('Confirme caso pretenda apagar os dados do projecto. Todos as actividades, registos e anexos associados serão também apagados.')
                ->press('Confirmar Apagar')
                ->pause(1000)
                ->assertPathIs('/home')
                ->assertSee('Projecto apagado com sucesso.')
                ->assertMissing('#detalhes5')
                ->assertPathIs('/home');
        });
        $this->assertSoftDeleted('projects', [
            'id' => '5'
        ]);
    }

    public function projectToggleState()
    {
        $this->browse(function (Browser $browser) {
            $browser->press('#toggleState2')
                ->click('#btn-running-2')
                ->assertSee('Confirme caso pretenda alterar o estado do projecto.')
                ->press('Decorrer')
                ->assertPathIs('/home')
                ->assertSee('Projecto passou ao estado de "A Decorrer" com sucesso.');
        });
        $this->assertDatabaseHas('projects', [
           'id' => 2,
            'status' => 1
        ]);
    }

    public function postCreate(){
        $this->browse(function (Browser $browser) {
            $browser->press('#detalhes2')
                ->press('#detalhes1')
                ->press('Adicionar Registo')
                ->type('#inputTitle', 'Registo Teste')
                ->keys('#inputDate', '01082018')
                ->type('#inputParticipants', '2')
                ->press('Guardar')
                ->pause(2000)
                ->assertSee('Registo criado com sucesso.')
                ->press('Adicionar Registo')
                ->type('#inputTitle', 'Registo Teste 2')
                ->keys('#inputDate', '02082018')
                ->type('#inputParticipants', '2')
                ->clickLink('Voltar')
                ->assertDontSee('Registo criado com sucesso.');
        });
        $this->assertDatabaseHas('posts', [
            'title' => 'Registo Teste'
        ]);
        $this->assertDatabaseMissing('posts', [
            'title' => 'Registo Teste 2'
        ]);
    }

    public function postEdit(){
        $this->browse(function (Browser $browser) {
            $browser->pause(2000)
                ->press('#detalhes1')
                ->press('Editar')
                ->type('#inputTitle', 'Registo Teste Editar')
                ->press('Guardar')
                ->pause(2000)
                ->assertSee('Registo actualizado com sucesso.')
                ->press('Editar')
                ->type('#inputTitle', 'Registo Teste Editar 2')
                ->clickLink('Voltar')
                ->assertDontSee('Registo actualizado com sucesso.');
        });
        $this->assertDatabaseHas('posts', [
            'id' => 1,
            'title' => 'Registo Teste Editar'
        ]);
        $this->assertDatabaseMissing('posts', [
            'id' => 1,
            'title' => 'Registo Teste Editar 2'
        ]);
    }

    public function postDisable()
    {
        $this->browse(function (Browser $browser) {
            $browser->press('Desactivar')
            ->assertSee('Registo desactivado com sucesso.');
        });

        $this->assertDatabaseHas('posts', [
           'id' => 1,
            'active' => 0
        ]);
    }

    public function postDelete(){
        $this->browse(function (Browser $browser) {
            $browser->press('Apagar')
                ->assertSee('Confirme caso pretenda apagar os dados do registo. Todos os anexos associados serão também apagados.')
                ->press('Confirmar Apagar')
                ->assertSee('Registo apagado com sucesso.');
        });
        $this->assertSoftDeleted('posts',[
            'id' => 1
        ]);
    }

}
