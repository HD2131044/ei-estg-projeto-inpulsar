<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testLogin(){

        //load cenario
        $this->cenario();

        //testAdminLogin
        $this->loginAsAdmin();

        //testDirectorLogin
        $this->loginAsDirector();

        //testManagerLogin
        $this->loginAsManager();

        //testUserLogin
        $this->loginAsUser();

        //testWrongLogin
        $this->wrongLogin();

    }

    public function cenario(){

        //Administradores = 2 => id's = [1,2]

        factory(User::class)->create([
            'name' => "Admin Um",
            'email' => "admin1@mail.pt",
            'password' => password_hash('admin123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'admin' => '1',
            'role' => '0',
            'activated' => '1'
        ]);

        factory(User::class)->create([
            'name' => "Admin Dois",
            'email' => "admin2@mail.pt",
            'password' => password_hash('admin123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'admin' => '1',
            'role' => '0',
            'activated' => '1'
        ]);

        //Diretores = 1+3 => id's = [3,4,5,6]

        factory(User::class)->create([
            'name' => "Diretor",
            'email' => "diretor@mail.pt",
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '1',
            'activated' => '1'
        ]);

        factory(User::class, 3)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '1',
            'activated' => '1'
        ]);

        //Gestores = 1+3 => id's = [7,8,9,10]

        factory(User::class)->create([
            'name' => "Gestor",
            'email' => "gestor@mail.pt",
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '2',
            'activated' => '1'
        ]);

        factory(User::class, 3)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '2',
            'activated' => '1'
        ]);

        //Utilizadores = 1+3 => id's = [11,12,13,14,15]

        factory(User::class)->create([
            'name' => "Utilizador",
            'email' => "utilizador@mail.pt",
            'password' => password_hash('secret', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '3',
            'activated' => '1'
        ]);

        factory(User::class, 4)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '3',
            'activated' => '1'
        ]);

    }

    public function loginAsAdmin(){
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->type('email', 'admin1@mail.pt')
                ->type('password', 'admin123')
                ->press('Login')
                ->assertPathIs('/home')
                ->assertSee('Lista de Utilizadores')
                ->click('.dropdown')
                ->clickLink('Sair');
        });
    }


    public function loginAsDirector(){
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->type('email', 'diretor@mail.pt')
                ->type('password', '123123123')
                ->press('Login')
                ->assertPathIs('/home')
                ->assertSee('Lista de Projectos')
                ->click('.dropdown')
                ->clickLink('Sair');
        });
    }

    public function loginAsManager(){
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->type('email', 'gestor@mail.pt')
                ->type('password', '123123123')
                ->press('Login')
                ->assertPathIs('/home')
                ->assertSee('Os Meus Projectos')
                ->click('.dropdown')
                ->clickLink('Sair');
        });
    }


    public function loginAsUser(){
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->type('email', 'utilizador@mail.pt')
                ->type('password', 'secret')
                ->press('Login')
                ->assertPathIs('/home')
                ->assertSee('As Minhas Actividades')
                ->click('.dropdown')
                ->clickLink('Sair');
        });
    }

    public function wrongLogin()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->type('email', 'fakeemail@mail.pt')
                ->type('password', 'admin123')
                ->press('Login')
                ->assertPathIs('/login');
        });
    }


}
