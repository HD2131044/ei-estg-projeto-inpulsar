<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ActivationLink extends Notification
{
    use Queueable;

    protected $user;
    protected $link;
    protected $password;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $link, $random_password)
    {
        $this->user = $user;
        $this->link = $link;
        $this->password = $random_password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Bem vindo à intranet da InPulsar.')
            ->line('Antes de poder fazer login na intranet da InPulsar tem de activar a sua conta.')
            ->action('Activar conta', $this->link)
            ->line('Após a activação da sua conta irá aceder automaticamente à intranet da InPulsar.')
            ->line('Para futuros acessos à intranet deverá utilizar as seguintes credenciais:')
            ->line('email: '.$this->user->email)
            ->line('password: '.$this->password)
            ->line('Se não solicitou a activação de conta na intranet da InPulsar, não é necessário nenhuma acção adicional.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
