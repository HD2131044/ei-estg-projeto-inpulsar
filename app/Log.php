<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Log extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Um attachment pretence a um user. [One To Many - Inverse]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Um attachment pretence a um project. [One To Many - Inverse]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Um attachment pretence a uma activity. [One To Many - Inverse]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    /**
     * Um attachment pretence a um post. [One To Many - Inverse]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * Um attachment pretence a um attachment. [One To Many - Inverse]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attachment()
    {
        return $this->belongsTo(Post::class);
    }

    //----------------------------------------------------------------

    public function logCreatorName()
    {
        $creator = User::withTrashed()->find($this->entry_by);
        if($creator){
            return $creator->name;
        }else{
            return "Utilizador apagado";
        }
    }

    public function store($flag, $args){

        $this->entry_by = Auth::user()->id;

        $now = new DateTime();
        $dateNow = $now->format('Y-m-d H:i:s');

        $this->date = $now;

        switch ($flag) {
            case 1: //criar utilizador
                $user = $args->get('user');
                $this->user_id = $user->id;
                $this->action = 'user_create';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' criou o utilizador '.$user->name.', com o perfil de '.$user->roleToStr().'.';
                break;
            case 2: //editar utilizador
                $user = $args->get('user');
                $this->user_id = $user->id;
                $this->action = 'user_edit';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' editou os dados do utilizador '.$user->name.'. Perfil após edição de dados: '.$user->roleToStr().'.';
                break;
            case 3: //apagar utilizador
                $user = $args->get('user');
                $this->user_id = $user->id;
                $this->action = 'user_delete';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' apagou os dados do utilizador '.$user->name.'.';
                break;
            case 4: //bloquer utilizador
                $user = $args->get('user');
                $this->user_id = $user->id;
                $this->action = 'user_block';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' bloqueou o utilizador '.$user->name.'.';
                break;
            case 5: //desbloquear utilizador
                $user = $args->get('user');
                $this->user_id = $user->id;
                $this->action = 'user_unblock';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' desbloqueou o utilizador '.$user->name.'.';
                break;
            case 6: //reeniciar password
                $user = $args->get('user');
                $this->user_id = $user->id;
                $this->action = 'user_reset_password';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' reeniciou a password do utilizador '.$user->name.'.';
                break;
            case 7: //enviar email de activação - manual
                $user = $args->get('user');
                $this->user_id = $user->id;
                $this->action = 'user_send_activation_mail';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' enviou email de activação de conta para o utilizador '.$user->name.'.';
                break;
            case 8: //criar projecto
                $project = $args->get('project');
                $this->project_id = $project->id;
                $this->action = 'project_create';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' criou o projecto '.$project->name.', com o estado de '.$project->statusToStr().'. Estado da submissão: '.$project->submittedToStr().'. Número de submissões: '.$project->times_submitted.'.';
                break;
            case 9: //editar projecto
                $project = $args->get('project');
                $this->project_id = $project->id;
                $this->action = 'project_edit';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' editou os dados do projecto '.$project->name.'. Estado após edição de dados: '.$project->statusToStr().'. Estado da submissão: '.$project->submittedToStr().'. Número de submissões: '.$project->times_submitted.'.';
                break;
            case 10: //apagar projecto
                $project = $args->get('project');
                //tb fica associado ao project por causa do softdelete
                $this->project_id = $project->id;
                $this->action = 'project_delete';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' apagou os dados do projecto '.$project->name.'.';
                break;
            case 11: //submeter projecto
                $project = $args->get('project');
                $this->project_id = $project->id;
                $this->action = 'project_submit';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' submeteu a candidatura do projecto '.$project->name.'. Estado alterado para: '.$project->statusToStr().'. Estado da submissão: '.$project->submittedToStr().'. Número de submissões: '.$project->times_submitted.'.';
                break;
            case 12: //cancelar projecto
                $project = $args->get('project');
                $this->project_id = $project->id;
                $this->action = 'project_cancel';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' passou o estado do projecto '.$project->name.' a cancelado. Estado alterado para: '.$project->statusToStr().'. Estado da submissão: '.$project->submittedToStr().'. Número de submissões: '.$project->times_submitted.'.';
                break;
            case 13: //decorrer projecto
                $project = $args->get('project');
                $this->project_id = $project->id;
                $this->action = 'project_running';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' passou o estado do projecto '.$project->name.' a decorrer. Estado alterado para: '.$project->statusToStr().'. Estado da submissão: '.$project->submittedToStr().'. Número de submissões: '.$project->times_submitted.'.';
                break;
            case 14: //concluir projecto
                $project = $args->get('project');
                $this->project_id = $project->id;
                $this->action = 'project_close';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' passou o estado do projecto '.$project->name.' a concluído. Estado alterado para: '.$project->statusToStr().'. Estado da submissão: '.$project->submittedToStr().'. Número de submissões: '.$project->times_submitted.'.';
                break;
            case 15: //resubmeter projecto
                $project = $args->get('project');
                $this->project_id = $project->id;
                $this->action = 'project_pending';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' passou o estado do projecto '.$project->name.' a pendente, de forma a poder resubmeter a sua candidatura. Estado alterado para: '.$project->statusToStr().'. Estado da submissão: '.$project->submittedToStr().'. Número de submissões: '.$project->times_submitted.'.';
                break;
            case 16: //criar actividade
                $project = $args->get('project');
                $activity = $args->get('activity');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                $this->action = 'activity_create';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' criou a actividade '.$activity->name.', com o estado de '.$activity->statusToStr().'.';
                break;
            case 17: //editar actividade
                $project = $args->get('project');
                $activity = $args->get('activity');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                $this->action = 'activity_edit';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' editou os dados da actividade '.$activity->name.'. Estado após edição de dados: '.$activity->statusToStr().'.';
                break;
            case 18: //apagar actividade
                $project = $args->get('project');
                $activity = $args->get('activity');
                $this->project_id = $project->id;
                //para ficar apenas associada ao projecto
                //tb fica associado à activity por causa do softdelete
                $this->activity_id = $activity->id;
                $this->action = 'activity_delete';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' apagou os dados da actividade '.$activity->name.'.';
                break;
            case 19: //pendente actividade
                $project = $args->get('project');
                $activity = $args->get('activity');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                $this->action = 'activity_pending';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' passou o estado da actividade '.$activity->name.' a pendente. Estado alterado para: '.$activity->statusToStr().'.';
                break;
            case 20: //decorrer actividade
                $project = $args->get('project');
                $activity = $args->get('activity');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                $this->action = 'activity_running';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' passou o estado da actividade '.$activity->name.' a decorrer. Estado alterado para: '.$activity->statusToStr().'.';
                break;
            case 21: //concluir actividade
                $project = $args->get('project');
                $activity = $args->get('activity');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                $this->action = 'activity_close';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' passou o estado da actividade '.$activity->name.' a conluída. Estado alterado para: '.$activity->statusToStr().'.';
                break;
            case 22: //concluir automáticamente actividade
                $project = $args->get('project');
                $activity = $args->get('activity');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                $this->action = 'activity_auto_close';
                $this->entry_text = $dateNow . ' - ' . 'PROCESSO AUTOMÁTICO: Projecto foi colocado em estado concluído, passando o estado da actividade '.$activity->name.' a conluída. Estado alterado para: '.$activity->statusToStr().'.';
                break;
            case 23: //reverte estado automático da actividade
                $project = $args->get('project');
                $activity = $args->get('activity');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                $this->action = 'activity_error_revert_auto_close';
                $this->entry_text = $dateNow . ' - ' . 'ERRO - PROCESSO AUTOMÁTICO: Conclusão automática do projecto encontrou um erro. A actividade '.$activity->name.' reverteu ao estado anterior. Estado alterado para: '.$activity->statusToStr().'.';
                break;
            case 24: //reverte estado automático da actividade em modo de edição
                $project = $args->get('project');
                $activity = $args->get('activity');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                $this->action = 'activity_revert_auto_close';
                $this->entry_text = $dateNow . ' - ' . 'PROCESSO AUTOMÁTICO: A actividade '.$activity->name.' reverteu ao estado anterior. Estado alterado para: '.$activity->statusToStr().'.';
                break;
            case 25: //criar post
                $project = $args->get('project');
                $activity = $args->get('activity');
                $post = $args->get('post');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                $this->post_id = $post->id;
                $this->action = 'post_create';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' criou o registo '.$post->title.', com o estado de Activo.';
                break;
            case 26: //editar post
                $project = $args->get('project');
                $activity = $args->get('activity');
                $post = $args->get('post');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                $this->post_id = $post->id;
                $this->action = 'post_edit';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' editou os dados do registo '.$post->title.'. Estado após edição de dados: '.$post->activeToStr().'.';
                break;
            case 27: //apagar post
                $project = $args->get('project');
                $activity = $args->get('activity');
                $post = $args->get('post');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                //para ficar apenas associada ao projecto/actividade
                //tb fica associado ao post por causa do softdelete
                $this->post_id = $post->id;
                $this->action = 'post_delete';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' apagou os dados do registo '.$post->title.'.';
                break;
            case 28: //post activado
                $project = $args->get('project');
                $activity = $args->get('activity');
                $post = $args->get('post');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                $this->post_id = $post->id;
                $this->action = 'post_active';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' alterou o estado do registo '.$post->title.', para activo. Estado alterado para: '.$post->activeToStr().'.';
                break;
            case 29: //post desactivado
                $project = $args->get('project');
                $activity = $args->get('activity');
                $post = $args->get('post');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                $this->post_id = $post->id;
                $this->action = 'post_inactive';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' alterou o estado do registo '.$post->title.', para inactivo. Estado alterado para: '.$post->activeToStr().'.';
                break;
            case 30: //anexo inserido
                $project = $args->get('project');
                $attachment = $args->get('attachment');
                $this->project_id = $project->id;
                $this->attachment_id = $attachment->id;
                //para ficar associado ao projecto/actividade/post
                if($attachment->activity_id){
                    $this->activity_id = $attachment->activity_id;
                }
                if($attachment->post_id){
                    $this->post_id = $attachment->post_id;
                }
                $this->action = 'attachement_insert';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' inseriu o anexo '.$attachment->original_filename.'.';
                break;
            case 31: //anexo apagado
                $project = $args->get('project');
                $attachment = $args->get('attachment');
                $this->project_id = $project->id;
                //para ficar só associado ao projecto/actividade/post
                //tb fica associado ao attachment por causa do softdelete
                $this->attachment_id = $attachment->id;
                if($attachment->activity_id){
                    $this->activity_id = $attachment->activity_id;
                }
                if($attachment->post_id){
                    $this->post_id = $attachment->post_id;
                }
                $this->action = 'attachment_delete';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' apagou o anexo '.$attachment->original_filename.'.';
                break;
            case 32: //anexo download
                $project = $args->get('project');
                $attachment = $args->get('attachment');
                $this->project_id = $project->id;
                $this->attachment_id = $attachment->id;
                //para ficar associado ao projecto/actividade/post
                if($attachment->activity_id){
                    $this->activity_id = $attachment->activity_id;
                }
                if($attachment->post_id){
                    $this->post_id = $attachment->post_id;
                }
                $this->action = 'attachment_download';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' fez download do anexo '.$attachment->original_filename.'.';
                break;
            case 33: //anexo preview
                $project = $args->get('project');
                $attachment = $args->get('attachment');
                $this->project_id = $project->id;
                $this->attachment_id = $attachment->id;
                //para ficar associado ao projecto/actividade/post
                if($attachment->activity_id){
                    $this->activity_id = $attachment->activity_id;
                }
                if($attachment->post_id){
                    $this->post_id = $attachment->post_id;
                }
                $this->action = 'attachment_preview';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' pré-visualizou o anexo '.$attachment->original_filename.'.';
                break;
            case 34: //anexo download all
                $project = $args->get('project');
                $this->project_id = $project->id;
                $particular_message = " fez download de todos os anexos activos do projeto ".$project->name.'.';
                //para ficar associado ao projecto/actividade/post
                if($args->get('activity')){
                    $activity = $args->get('activity');
                    $this->activity_id = $activity->id;
                    $particular_message = " fez download de todos os anexos activos da actividade ".$activity->name.'.';
                }
                if($args->get('post')){
                    $post = $args->get('post');
                    $this->post_id = $post->id;
                    $particular_message = " fez download de todos os anexos activos do registo ".$post->title.'.';
                }
                $this->action = 'attachment_download_all';
                $this->entry_text = $dateNow . ' - ' . Auth::user()->name . $particular_message;
                break;
            case 35: //apagar actividade automática após projecto apagado
                $project = $args->get('project');
                $activity = $args->get('activity');
                $this->project_id = $project->id;
                //para ficar apenas associada ao projecto
                //tb fica associado à activity por causa do softdelete
                $this->activity_id = $activity->id;
                $this->action = 'project_delete_auto_activity_delete';
                $this->entry_text = $dateNow . ' - ' . 'PROCESSO AUTOMÁTICO: Projecto '.$project->name.' foi apagado. A actividade '.$activity->name.' foi apagada.';
                break;
            case 36: //apagar post automático após projecto/activity apagado
                $project = $args->get('project');
                $activity = $args->get('activity');
                $post = $args->get('post');
                $this->project_id = $project->id;
                $this->activity_id = $activity->id;
                //para ficar apenas associada ao projecto/actividade
                //tb fica associado ao post por causa do softdelete
                $this->post_id = $post->id;
                //tipos de action 'project_delete_auto_post_delete' ou 'activity_delete_auto_post_delete'
                $this->action = $args->get('action');
                if($this->action == 'project_delete_auto_post_delete'){
                    $this->entry_text = $dateNow . ' - ' . 'PROCESSO AUTOMÁTICO: Projecto '.$project->name.' foi apagado. O registo '.$post->title.' foi apagado.';
                }elseif($this->action == 'activity_delete_auto_post_delete'){
                    $this->entry_text = $dateNow . ' - ' . 'PROCESSO AUTOMÁTICO: Actividade '.$activity->name.' foi apagada. O registo '.$post->title.' foi apagado.';
                }
                break;
            case 37: //anexo apagado automático após projecto/activity/post apagado
                $project = $args->get('project');
                $attachment = $args->get('attachment');
                $this->project_id = $project->id;
                //para ficar só associado ao projecto/actividade/post
                //tb fica associado ao attachment por causa do softdelete
                $this->attachment_id = $attachment->id;
                if($attachment->activity_id){
                    $this->activity_id = $attachment->activity_id;
                    $activity = Activity::withTrashed()->where('id', '=', $this->activity_id)->first();
                }
                if($attachment->post_id){
                    $this->post_id = $attachment->post_id;
                    $post = Post::withTrashed()->where('id', '=', $this->post_id)->first();
                }
                //tipos de action 'project_delete_auto_attachment_delete' ou 'activity_delete_auto_attachment_delete' ou 'post_delete_auto_attachment_delete'
                $this->action = $args->get('action');
                if($this->action == 'project_delete_auto_attachment_delete'){
                    $this->entry_text = $dateNow . ' - ' . 'PROCESSO AUTOMÁTICO: Projecto '.$project->name.' foi apagado. O anexo '.$attachment->original_filename.' foi apagado.';
                }elseif($this->action == 'activity_delete_auto_attachment_delete'){
                    $this->entry_text = $dateNow . ' - ' . 'PROCESSO AUTOMÁTICO: Actividade '.$activity->name.' foi apagada. O anexo '.$attachment->original_filename.' foi apagado.';
                }elseif($this->action == 'post_delete_auto_attachment_delete'){
                    $this->entry_text = $dateNow . ' - ' . 'PROCESSO AUTOMÁTICO: Registo '.$post->title.' foi apagado. O anexo '.$attachment->original_filename.' foi apagado.';
                }
                break;
            case 38: //partilha para o Facebook de projecto/activity/post
                $project = $args->get('project');
                $this->project_id = $project->id;
                //para ficar associado ao projecto/actividade/post
                if($args->get('activity')){
                    $activity = $args->get('activity');
                    $this->activity_id = $activity->id;
                }
                if($args->get('post')){
                    $post = $args->get('post');
                    $this->post_id = $post->id;
                }
                //tipos de action 'project_facebook_publish' ou 'activity_facebook_publish' ou 'post_facebook_publish'
                $this->action = $args->get('action');
                if($this->action == 'project_facebook_publish'){
                    $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' publicou o projecto '.$project->name.' no Facebook.';
                }elseif($this->action == 'activity_facebook_publish'){
                    $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' publicou a actividade '.$activity->name.' no Facebook.';
                }elseif($this->action == 'post_facebook_publish'){
                    $this->entry_text = $dateNow . ' - ' . Auth::user()->name . ' publicou o registo '.$post->title.' no Facebook.';
                }
                break;
        }

        $this->save();
    }

}
