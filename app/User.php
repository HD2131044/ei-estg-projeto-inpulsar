<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use DateTime;

class User extends Authenticatable
{
    use Notifiable;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Um user pode ter vários posts. [One To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * Um user pode ter vários attachments. [One To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    /**
     * Um user pode ter vários logs. [One To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    /**
     * Projects associados ao user. [Many To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'user_project');
    }

    /**
     * Activities associadas ao user. [Many To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function activities()
    {
        return $this->belongsToMany(Activity::class, 'user_activity');
    }

    //----------------------------------------------------------------

    public function isAdmin()
    {
        return $this->admin == 1;
    }

    public function isDirector()
    {
        return $this->role == 1;
    }

    public function isManager()
    {
        if($this->role == 1 || $this->role == 2)
        {
            return true;
        }
        return false;
    }

    public function isPostOwner(Post $post)
    {
        if($this->id == $post->user_id)
        {
            return true;
        }
        return false;
    }

    public function isBlocked()
    {
        return $this->blocked == 1;
    }

    public function isActivated()
    {
        return $this->activated == 1;
    }

    public function roleToStr()
    {
        if($this->admin == 1)
        {
            return 'Gestor de conta';
        } else
        {
            switch ($this->role)
            {
                case 1:
                    return 'Diretor';
                case 2:
                    return 'Coordenador';
                case 3:
                    return 'Técnico do projecto';
            }
        }
        return 'Não autenticado';
    }

    public function blockedToStr()
    {
        switch ($this->blocked)
        {
            case 0:
                return 'Não';
            case 1:
                return 'Sim';
        }
        return '';
    }

    public function activatedToStr()
    {
        switch ($this->activated)
        {
            case 0:
                return 'Não ativa';
            case 1:
                return 'Ativa';
        }
        return '';
    }

    public function getAge()
    {
        $birthdate = new DateTime($this->birth_date);
        $now = new DateTime('today');
        $age = $birthdate->diff($now)->y;
        return $age;
    }

    public function getGender()
    {
        if($this->gender == 'M')
        {
            return 'Masculino';
        }
        return 'Feminino';
    }

    public function canPublish()
    {
        if($this->publisher == '1')
        {
            return 'Sim';
        }
        return 'Não';
    }

    public function isPublisher()
    {
        return $this->publisher == 1;
    }

    public function getFoto(){
        if($this->profile_photo){
            $path = $this->profile_photo;
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            return $base64;
        } /*else {
            //sem upload da photo (sem profile_photo)
            $path = storage_path('app')."/public/photos/photoNotAvailable.jpg";
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            return $base64;
        }*/
        return "";

    }

    public function getUserLogs()
    {
        //return $this->logs()->orderBy('id', 'desc')->paginate(10, ['*'], 'user_logs');
        //para datatables
        return $this->logs()->orderBy('id', 'desc')->get();
    }

}
