<?php

namespace App\Http\Middleware;

use App\Activity;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Closure;

class UserOwnsAttachmentAndPostIsActiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $project = $request->project;
        $attachment = $request->attachment;
        $activity = Activity::find($attachment->activity_id);

        if(Auth::user()->isDirector()
            || (Auth::user()->isManager() && $project->containsUser(Auth::user())))
        {
            return $next($request);
        }elseif($activity->containsUser(Auth::user()) && $attachment->belongsToUser(Auth::user()) && $attachment->post_id){
            $post = Post::find($attachment->post_id);
            if($post->isActive() && !$activity->isClosed()){
                return $next($request);
            }elseif($post->isActive() && $activity->isClosed()){
                $message = ['error.message' => 'Não é possível apagar o anexo. O projecto/actividade a que o anexo está associado encontra-se num estado que não pertime editar o anexo.'];
                return redirect()->route('home')->with($message);
            }else{
                $message = ['error.message' => 'Sem permissões.'];
                return redirect()->route('home')->with($message);
            }
        }

        $message = ['error.message' => 'Sem permissões.'];
        return redirect()->route('home')->with($message);
    }
}
