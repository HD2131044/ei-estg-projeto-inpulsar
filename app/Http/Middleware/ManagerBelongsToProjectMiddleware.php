<?php

namespace App\Http\Middleware;

use App\Project;
use Illuminate\Support\Facades\Auth;
use Closure;

class ManagerBelongsToProjectMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if($request->project instanceof Project){
            $project = $request->project;
        } else {
            $project = Project::find(intval($request->project));
        }

        if(Auth::user()->isDirector()
            || (Auth::user()->isManager() && $project->containsUser(Auth::user())))
        {
            return $next($request);
        }

        $message = ['error.message' => 'Sem permissões.'];
        return redirect()->route('home')->with($message);
    }
}
