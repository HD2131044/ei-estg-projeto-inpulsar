<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class UserCanEditUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user;

        if(Auth::user()->isAdmin() && !$user->isAdmin())
        {
            return $next($request);
        }

        $message = ['error.message' => 'Sem permissões.'];
        return redirect()->route('home')->with($message);
    }

}