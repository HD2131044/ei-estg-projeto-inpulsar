<?php

namespace App\Http\Middleware;

use App\Activity;
use App\Attachment;
use App\Post;
use App\Project;
use Illuminate\Support\Facades\Auth;
use Closure;

class UserCanDownloadAttachmentAndPostIsActiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->project instanceof Project){
            $project = $request->project;
        }else{
            $project = Project::find(intval($request->project));
        }

        if($request->attachment instanceof Attachment){
            $attachment = $request->attachment;
        }else{
            $attachment = Attachment::find(intval($request->project));
        }

        $activity = Activity::find($attachment->activity_id);

        if(Auth::user()->isDirector()
            || (Auth::user()->isManager() &&  $project->containsUser(Auth::user())))
        {
            return $next($request);
        }elseif($activity->containsUser(Auth::user()) && !$attachment->post_id){
            return $next($request);
        }elseif($activity->containsUser(Auth::user()) && $attachment->post_id){
            $post = Post::find($attachment->post_id);
            if($post->isActive()){
                return $next($request);
            }else{
                $message = ['error.message' => 'Sem permissões.'];
                return redirect()->route('home')->with($message);
            }
        }

        $message = ['error.message' => 'Sem permissões.'];
        return redirect()->route('home')->with($message);

    }
}