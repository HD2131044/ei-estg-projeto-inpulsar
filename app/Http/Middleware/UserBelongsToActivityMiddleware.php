<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class UserBelongsToActivityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $project = $request->project;
        $activity = $request->activity;

        if(Auth::user()->isDirector()
            || (Auth::user()->isManager() && $project->containsUser(Auth::user()))
            || $activity->containsUser(Auth::user()))
        {
            return $next($request);
        }

        $message = ['error.message' => 'Sem permissões.'];
        return redirect()->route('home')->with($message);
    }
}
