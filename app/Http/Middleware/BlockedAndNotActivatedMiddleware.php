<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BlockedAndNotActivatedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->isActivated()) {
            $message = ['error.message' => 'A sua conta encontra-se por activar. Proceda à activação da mesma via link que lhe foi enviado por email.'];
            return redirect()->route('logout')->with($message);
        }

        if (Auth::user()->isBlocked()) {
            $message = ['error.message' => 'A sua conta encontra-se bloqueada! Contacte um "Gestor de conta".'];
            return redirect()->route('logout')->with($message);
        }

        return $next($request);
    }
}