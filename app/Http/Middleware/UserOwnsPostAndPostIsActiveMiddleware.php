<?php

namespace App\Http\Middleware;

use App\Activity;
use App\Post;
use App\Project;
use Illuminate\Support\Facades\Auth;
use Closure;

class UserOwnsPostAndPostIsActiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->project instanceof Project){
            $project = $request->project;
        } else {
            $project = Project::find(intval($request->project));
        }
        if($request->activity instanceof Activity){
            $activity = $request->activity;
        } else {
            $activity = Activity::find(intval($request->activity));
        }
        if($request->post instanceof Post){
            $post = $request->post;
        } else {
            $post = Post::find(intval($request->post));
        }

        if (Auth::user()->isDirector()
            || (Auth::user()->isManager() && $project->containsUser(Auth::user())))
        {
            return $next($request);
        }elseif($activity->containsUser(Auth::user()) && $post->belongsToUser(Auth::user()) && $post->isActive()) {
            return $next($request);
        }

        $message = ['error.message' => 'Sem permissões.'];
        return redirect()->route('home')->with($message);

    }
}
