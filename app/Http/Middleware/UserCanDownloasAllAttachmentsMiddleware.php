<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class UserCanDownloasAllAttachmentsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $project = $request->project;

        if ($request->activity) {
            $activity = $request->activity;

            if ($request->post) {
                $post = $request->post;
                //download de anexos de post
                if((Auth::user()->isDirector()
                    || (Auth::user()->isManager() &&  $project->containsUser(Auth::user()))) && ($post->getNumberActiveAttachments() > 0))
                {
                    return $next($request);
                }elseif ($activity->containsUser(Auth::user()) && $post->isActive() && ($post->getNumberActiveAttachments() > 0)) {
                    return $next($request);
                } else {
                    $message = ['error.message' => 'Sem permissões.'];
                    return redirect()->route('home')->with($message);
                }

            } else {
                //download de anexos de actividade
                if((Auth::user()->isDirector()
                        || (Auth::user()->isManager() &&  $project->containsUser(Auth::user()))) && ($activity->getNumberActiveAttachments() > 0))
                {
                    return $next($request);
                }elseif ($activity->containsUser(Auth::user()) && ($activity->getNumberActiveAttachments() > 0)) {
                    return $next($request);
                } else {
                    $message = ['error.message' => 'Sem permissões.'];
                    return redirect()->route('home')->with($message);
                }
            }

        } else {
            //download de anexos de projecto
            if ((Auth::user()->isDirector()
                    || (Auth::user()->isManager() && $project->containsUser(Auth::user()))) && ($project->getNumberActiveAttachments() > 0)
            ) {
                return $next($request);
            } else {
                $message = ['error.message' => 'Sem permissões.'];
                return redirect()->route('home')->with($message);
            }
        }

        $message = ['error.message' => 'Sem permissões.'];
        return redirect()->route('home')->with($message);

    }
}