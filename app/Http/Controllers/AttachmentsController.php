<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Attachment;
use App\Log;
use App\Post;
use App\Project;
use Illuminate\Http\Request;
use DateTime;
use Illuminate\Support\Facades\File;
use Response;
use ZipArchive;


class AttachmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
/*    public function index()
    {
        //
    }*/

    /**
     * Display the specified resource.
     *
     * @param  Project $project, Attachment $attachment
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project, Attachment $attachment)
    {
        $filename = $attachment->filename;
        $destinationPath = $attachment->media_path;
        $target = $destinationPath.'/'.$filename;

        //Adicionar ao Log -> Flag 33 -> preview anexo
        /*$log = new Log;
        $args = collect([]);
        $args->put('project', $project);
        $args->put('attachment', $attachment);
        $log->store(33, $args);*/

      return Response::make(file_get_contents($target), 200, [
            'Content-Type' => $attachment->mime_type,
            'Content-Length' => $attachment->size,
            'Content-Disposition' => 'inline; filename="'.$attachment->original_filename.'"'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Project $project, Attachment $attachment
     * @return \Illuminate\Http\Response
     */
    public static function destroy(Project $project, Attachment $attachment)
    {
        $message = ['error.message' => 'Erro ao apagar anexo.'];

        $filename = $attachment->filename;
        $destinationPath = $attachment->media_path;
        $target = $destinationPath.'/'.$filename;

        $attachment_aux = $attachment;

        if($attachment->delete()){
            $message = ['success.message' => 'Anexo apagado com sucesso'];
            File::Delete($target);

            //Adicionar ao Log -> Flag 31 -> apagar anexo
            $log = new Log;
            $args = collect([]);
            $args->put('project', $project);
            $args->put('attachment', $attachment_aux);
            $log->store(31, $args);
        }

        return redirect()->back()->with($message);
    }

    public function download(Project $project, Attachment $attachment)
    {
            $message = ['error.message' => 'Erro ao descarregar anexo.'];

            $filename = $attachment->filename;
            $destinationPath = $attachment->media_path;
            $target = $destinationPath.'/'.$filename;
            if(asset($target)){
                $message = ['success.message' => 'Anexo descarregado com sucesso'];

                //Adicionar ao Log -> Flag 32 -> download anexo
                $log = new Log;
                $args = collect([]);
                $args->put('project', $project);
                $args->put('attachment', $attachment);
                $log->store(32, $args);

                return response()->download($target, $attachment->original_filename);
            }
            return redirect()->back()->with($message);
    }

    public function downloadAllProjectAttachments(Project $project)
    {
        $result = $this->downloadAllAttachments($project);
        if(!$result){
            $message = ['error.message' => 'Erro ao descarregar todos os anexos activos.'];
            return redirect()->back()->with($message);
        }
        return redirect()->back();
    }

    public function downloadAllActivityAttachments(Project $project, Activity $activity)
    {
        $result = $this->downloadAllAttachments($project, $activity);
        if(!$result){
            $message = ['error.message' => 'Erro ao descarregar todos os anexos activos.'];
            return redirect()->back()->with($message);
        }
        return redirect()->back();
    }

    public function downloadAllPostAttachments(Project $project, Activity $activity, Post $post)
    {
        $result = $this->downloadAllAttachments($project, $activity, $post);
        if(!$result){
            $message = ['error.message' => 'Erro ao descarregar todos os anexos activos.'];
            return redirect()->back()->with($message);
        }
        return redirect()->back();
    }

    public function downloadAllAttachments(Project $project, Activity $activity = null, Post $post = null)
    {
        $now = new DateTime();
        $dateNow = $now->format('YmdHis');

        $zip = new ZipArchive();
        $zip_file = "InPulsar_#".$project->id."_".$dateNow.".zip";
        $zip_file_size = 0;
        $result = $zip->open($zip_file, ZipArchive::CREATE);

        if($result){
            $object = $project;

            if($activity){
                $object = $activity;
            }

            if($post){
                $object = $post;
            }

            foreach ($object->getActiveAttachments()->get() as $attachment) {
                $filename = $attachment->filename;
                $destinationPath = $attachment->media_path;
                $target = $destinationPath.'/'.$filename;
                if(file_exists($target)){
                    $zip->addFile($target, $attachment->original_filename);
                    $zip_file_size += $attachment->size;
                }
            }

            $zip->close();

            // Stream the file to the client
            header("Content-Type: application/zip");
            if(filesize($zip_file)){
                header("Content-Length: " . filesize($zip_file));
            }else{
                header("Content-Length: " . $zip_file_size);
            }
            header('Content-disposition: attachment; filename='.$zip_file);
            readfile($zip_file);

            unlink($zip_file);

            //Adicionar ao Log -> Flag 34 -> download todos os anexos activos
            $log = new Log;
            $args = collect([]);
            $args->put('project', $project);
            if($activity){
                $args->put('activity', $activity);
            }
            if($post){
                $args->put('post', $post);
            }
            $log->store(34, $args);
        }

        return $result;
    }

    public function getImagePreview(Request $request){
        $image = Attachment::find($request->input('imageId'));
        $response = ['success' => false, 'fileName' => null, 'image' => null];
        if(file_exists($image->media_path . '/' . $image->filename)){
            $content = base64_encode(file_get_contents($image->media_path . '/' . $image->filename));
            $response = ['success' => true, 'fileName' => $image->original_filename, 'image' => $content];
        }
        return response()->json($response);
    }
}
