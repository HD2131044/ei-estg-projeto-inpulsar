<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Attachment;
use App\Log;
use App\Project;
use App\User;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

class ActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function index()
//    {
//        //$activities = Project::all();
//        //data mais próxima
//        $activities = Activity::latest()->paginate(10);
//
//        return view('home', compact('activities'));
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Project $project
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project)
    {
        return view('activities.create', compact('project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Project $project
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project)
    {
        if($project->isClosed() || $project->isCancelled()){
            $message = 'O estado do projecto não permite criar nova actividade.';
            $status = 'error.message';
        }else{
            $activity = new Activity;

            $response = $this->createActivityData($project, $activity);

            if(!empty($response)){
                if($response->status() != 200){
                    return $response;
                }
            }

            $message = 'Erro ao criar actividade.';
            $status = 'error.message';

            if($activity->save()){

                if (Input::hasFile('foto'))
                {
                    $file = Input::file('foto');
                    if($file->isValid()){
                        $file_size = $file->getSize();
                        $file_mimeType = $file->getMimeType();
                        $destinationPath = storage_path('app').'\project' . $project->id;
                        //$splitName = explode('.', $file->getFilename(), 2);
                        $splitName = 'Activity' . $activity->id . 'Foto';
                        $extension = $file->getClientOriginalExtension();
                        $filename = $splitName.'.'.$extension;
                        $upload_sucess = $file->move($destinationPath, $filename);
                        if($upload_sucess){
                            $activity->activity_photo = $destinationPath . '\\' .  $filename;
                            $activity->save();
                        }
                    }
                }

                $message = 'Actividade criada com sucesso.';
                $status = 'success.message';
                //$activity->users()->attach(Auth::user()->id);

                //Adicionar ao Log -> Flag 16 -> criar actividade
                $log = new Log;
                $args = collect([]);
                $args->put('project', $project);
                $args->put('activity', $activity);
                $log->store(16, $args);

                if(count(request('users'))) {
                    foreach (json_decode(request('users')) as $user){
                        if(!$activity->users()->find($user->id)){
                            $activity->users()->attach($user->id);
                        }
                        if(!$project->users()->find($user->id)){
                            $project->users()->attach($user->id);
                        }
                    }
                }
                if($project->getManagersCount()){
                    foreach ($project->getManagers() as $user){
                        if(!$activity->users()->find($user->id)){
                            $activity->users()->attach($user->id);
                        }
                    }
                }
            }

            if(count(request('anexos'))){
                if(!$this->multipleAttachmentsUpload($activity)) {
                    $message = 'Erro ao criar actividade.';
                    $status = 'error.message';
                    $this->destroy($project, $activity);
                }
            }
        }

        Session::flash($status, $message);
        return response()->json(['success' => true]);

    }

    public function createActivityData(Project $project, Activity $activity)
    {
        /*        $now = new DateTime('today');
                $dateNow = $now->format('Y-m-d');*/
        $validator = Validator::make(request()->all(), [
                'data_de_início' => 'required',
            ]);

        if($validator->fails()){
            Session::flash('errors', $validator->getMessageBag()->toArray());
            return response()->json('error', 422);
        }

        /*$this->validate(request(), [
            'data_de_início' => 'required',
        ]);*/

        $startDate = request('data_de_início');

        $validator = Validator::make(request()->all(), [
            //'nome' => 'required|regex:/^[a-zA-Z ]+$/',
            'nome' => 'required',
            'descrição' => 'nullable|max:200',
            'data_de_início' => 'required|after_or_equal:'.$project->start_date,
            'data_de_fim' => 'nullable|after:'.$startDate,
            'localização' => 'nullable|max:100',
            'url_actividade' => 'nullable|url',
            'estado_da_actividade' => 'digits_between:0,2'
        ]);

        if($validator->fails()){
            Session::flash('errors', $validator->getMessageBag()->toArray());
            return response()->json('error', 422);
        }
        /*$this->validate(request(), [
            //'nome' => 'required|regex:/^[a-zA-Z ]+$/',
            'nome' => 'required',
            'descrição' => 'nullable|max:200',
            //'data_de_início' => 'required|after_or_equal:' . $dateNow,
            'data_de_fim' => 'nullable|after:' . $startDate,
            'localização' => 'nullable|max:100',
            'url_actividade' => 'nullable|url',
            'estado_da_actividade' => 'digits_between:0,2'
        ]);*/

        $activity->creator_id = Auth::user()->id;
        $activity->project_id = $project->id;
        $activity->name = request('nome');
        $activity->description = request('descrição');
        $activity->start_date = request('data_de_início');
        $activity->end_date = request('data_de_fim');
        $activity->location = request('localização');
        $activity->activity_url = request('url_actividade');
        $activity->status = request('estado_da_actividade');

    }

    public function multipleAttachmentsUpload(Activity $activity){

        //$now = new DateTime('NOW');
        //$dateNow = $now->format('YmdHis');
        
        if (Input::hasFile('anexos'))
        {
            $files = Input::file('anexos');
            $file_count = count($files);

            foreach(range(0, $file_count-1) as $index) {
                $this->validate(request(), [
                    'anexos.'.$index = 'file'
                ]);
            }

            $uploadCount = 0;
            foreach ($files as $file){
                if($file->isValid()){
                    $file_size = $file->getSize();
                    $file_mimeType = $file->getMimeType();
                    $destinationPath = storage_path('app').'\project'.$activity->project_id;
                    $splitName = explode('.', $file->getFilename(), 2);
                    $extension = $file->getClientOriginalExtension();
                    $filename = $splitName[0].'.'.$extension;
                    $upload_sucess = $file->move($destinationPath, $filename);
                    if($upload_sucess){
                        $attachment = new Attachment();
                        $attachment->user_id = Auth::user()->id;
                        $attachment->project_id = $activity->project_id;
                        $attachment->activity_id = $activity->id;
                        $attachment->filename = $filename;
                        $attachment->original_filename = $file->getClientOriginalName();
                        $attachment->size = $file_size;
                        $attachment->mime_type = $file_mimeType;
                        $attachment->media_path = $destinationPath;
                        $attachment->date =  new DateTime('NOW');
                        if($attachment->save()){
                            $uploadCount ++;

                            //Adicionar ao Log -> Flag 30 -> inserir anexo
                            $log = new Log;
                            $args = collect([]);
                            $project = Project::find($attachment->project_id);
                            $args->put('project', $project);
                            $args->put('attachment', $attachment);
                            $log->store(30, $args);
                        }
                    }
                }
            }
            if($uploadCount == $file_count){
                return true;
            }
        }
        return false;
    }

    /**
     * Display the specified resource.
     *
     * @param  Project $project, Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project, Activity $activity)
    {
        $galleryImages = collect([]);
        if($activity->getNumberActiveImages()){
            $galleryImages = $activity->getActiveImages()->get();
        }
        return view('activities.show', compact(['project', 'activity', 'galleryImages']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Project $project, Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Activity $activity)
    {
        return view('activities.edit', compact(['project', 'activity']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Project $project, Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Activity $activity)
    {
        $response = $this->editActivityData($project, $activity);
        if(!empty($response)){
            if($response->status() != 200){
                return $response;
            }
        };

        $status = 'error.message';
        $message = 'Erro na actualização da actividade.';

        if (Input::hasFile('foto'))
        {
            $file = Input::file('foto');
            if($file->isValid()){
                $file_size = $file->getSize();
                $file_mimeType = $file->getMimeType();
                $destinationPath = storage_path('app').'\project' . $project->id;
                //$splitName = explode('.', $file->getFilename(), 2);
                $splitName = 'Activity' . $activity->id . 'Foto';
                $extension = $file->getClientOriginalExtension();
                $filename = $splitName.'.'.$extension;
                $upload_sucess = $file->move($destinationPath, $filename);
                if($upload_sucess){
                    $activity->activity_photo = $destinationPath . '\\' .  $filename;
                }
            }
        }

        if(count(request('anexosARemover'))){
            foreach (request('anexosARemover') as $anexo){
                AttachmentsController::destroy($project, Attachment::find(json_decode($anexo, true)['id']));
            }
        }
        if(count(request('anexos'))){
            if($this->multipleAttachmentsUpload($activity)) {
                if ($activity->update()) {
                    $status = 'success.message';
                    $message = 'Actividade actualizada com sucesso.';

                    //Adicionar ao Log -> Flag 17 -> editar actividade
                    $log = new Log;
                    $args = collect([]);
                    $args->put('project', $project);
                    $args->put('activity', $activity);
                    $log->store(17, $args);

                    if(count(request('users'))){
                        $activity->users()->detach(array_column($activity->users()->where('role', '=', '3')->get(['id'])->toArray(), 'id'));

                        foreach (request('users') as $userId){
                            $activity->users()->attach($userId);
                            if(!$project->users()->find($userId)){
                                $project->users()->attach($userId);
                            }
                        }
                    } else {
                        $activity->users()->detach(array_column($activity->users()->where('role', '=', '3')->get(['id'])->toArray(), 'id'));
                    }
                }
            }
        }else{
            if ($activity->update()) {
                $status = 'success.message';
                $message = 'Actividade actualizada com sucesso.';

                //Adicionar ao Log -> Flag 17 -> editar actividade
                $log = new Log;
                $args = collect([]);
                $args->put('project', $project);
                $args->put('activity', $activity);
                $log->store(17, $args);

                if(count(request('users'))){
                    $activity->users()->detach(array_column($activity->users()->where('role', '=', '3')->get(['id'])->toArray(), 'id'));

                    foreach (request('users') as $userId){
                        $activity->users()->attach($userId);
                        if(!$project->users()->find($userId)){
                            $project->users()->attach($userId);
                        }
                    }
                } else {
                    $activity->users()->detach(array_column($activity->users()->where('role', '=', '3')->get(['id'])->toArray(), 'id'));
                }
            }
        }
        Session::flash($status, $message);
        return response()->json('success', 200);
    }

    public function editActivityData(Project $project, Activity $activity)
    {
/*        if(request('data_de_início') != $activity->start_date){
            $now = new DateTime('today');
            $dateNow = $now->format('Y-m-d');

            $this->validate(request(), [
                'data_de_início' => 'required|after_or_equal:' . $dateNow,
            ]);
            $activity->start_date = request('data_de_início');
        }*/

        $validator = Validator::make(request()->all(),
            [
                'data_de_início' => 'required',
            ]);

        if($validator->fails()){
            Session::flash('errors', $validator->getMessageBag()->toArray());
            return response()->json('error', 422);
        }
        $startDate = request('data_de_início');

        $validator = Validator::make(request()->all(),
            [
                'nome' => 'required',
                'descrição' => 'nullable|max:200',
                'data_de_início' => 'required|after_or_equal:'.$project->start_date,
                'data_de_fim' => 'nullable|after:'.$startDate,
                'localização' => 'nullable|max:100',
                'url_actividade' => 'nullable|url',
                'estado_da_actividade' => 'digits_between:0,2'
            ]);

        if($validator->fails()){
            Session::flash('errors', $validator->getMessageBag()->toArray());
            return response()->json('error  ', 422);
        }

        $activity->name = request('nome');
        $activity->description = request('descrição');
        $activity->start_date = request('data_de_início');
        $activity->end_date = request('data_de_fim');
        $activity->location= request('localização');
        $activity->activity_url = request('url_actividade');
        $activity->status = request('estado_da_actividade');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Project $project, Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Activity $activity)
    {
        $message = ['error.message' => 'Erro ao apagar actividade.'];

        $activity_aux = $activity;

        //apanhar todos os posts
        $posts = collect([]);
        if($activity->posts()->count()){
            $posts = $activity->posts()->get();
        }
        //apanhar todos os anexos
        $attachments = collect([]);
        if($activity->attachments()->count()){
            $attachments = $activity->attachments()->get();
        }

        if($activity->delete()){
            $message = ['success.message' => 'Actividade apagada com sucesso.'];

            //Adicionar ao Log -> Flag 18 -> apagar activity
            $log = new Log;
            $args = collect([]);
            $args->put('project', $project);
            $args->put('activity', $activity_aux);
            $log->store(18, $args);

            //registar todos os logs (resultantes do delete cascate)
            //posts
            if($posts->count() > 0){
                foreach($posts as $post){
                    $post_aux = $post;
                    if($post->delete()){
                        $activity = Activity::withTrashed()->where('id', '=', $post_aux->activity_id)->first();
                        $project = Project::withTrashed()->where('id', '=', $activity->project_id)->first();
                        //Adicionar ao Log -> Flag 36 -> apagar post automático após actividade apagada
                        $log = new Log;
                        $args = collect([]);
                        $args->put('project', $project);
                        $args->put('activity', $activity);
                        $args->put('post', $post_aux);
                        $args->put('action', 'activity_delete_auto_post_delete');
                        $log->store(36, $args);
                    }
                }
            }
            //anexos
            if($attachments->count() > 0){
                foreach($attachments as $attachment){
                    $attachment_aux = $attachment;
                    if($attachment->delete()){
                        $project = Project::withTrashed()->where('id', '=', $attachment_aux->project_id)->first();
                        //Adicionar ao Log -> Flag 37 -> anexo apagado automático após actividade apagada
                        $log = new Log;
                        $args = collect([]);
                        $args->put('project', $project);
                        $args->put('attachment', $attachment_aux);
                        $args->put('action', 'activity_delete_auto_attachment_delete');
                        $log->store(37, $args);
                    }
                }
            }

        }

        return redirect('/projects/' . $project->id)->with($message);

    }

    public function getUsers(){
        $allUsers = User::where('role', '=', '3')->get()->toArray();
        return response()->json(['data' => $allUsers]);
    }

    public function fetchTableData($project, $activity){
        $users = Activity::find($activity)->users()->where('role', '=', 3)->get();
        $allUsers = User::where('role', '=', 3)->get()->diff($users);
        $attachments = Activity::find($activity)->attachments()->get();
        return response()->json(['users' => $users, 'allUsers' => $allUsers, 'attachments' => $attachments]);
    }

    public function fetchUsers($project, $activity){
        $users = Activity::find($activity)->users()->where('role', '=', 3)->get()->toArray();
        return response()->json(['data' => $users]);
    }

    public function fetchAllUsers($project, $activity){
        $users = Activity::find($activity)->users()->where('role', '=', 3)->get();
        $allUsers = User::where('role', '=', 3)->get()->diff($users);
        return response()->json(['data' => $allUsers]);
    }

    public function changeActivityToPendingState(Project $project, Activity $activity)
    {
        $message = ['error.message' => 'Erro na mudança de estado da actividade para o estado "Pendente".'];
        if($project->isClosed() || $project->isCancelled()){
            $message = ['error.message' => 'O projecto encontra-se no estado concluído/cancelado, pelo que não pode alterar o estado da actividade.'];
        } else{
            if($activity->status != 0 && $activity->status != 2){
                $activity->status = 0;
                if($activity->save()){
                    $message = ['success.message' => 'Actividade passou ao estado de "Pendente" com sucesso.'];

                    //Adicionar ao Log -> Flag 19 -> pendente actividade
                    $log = new Log;
                    $args = collect([]);
                    $args->put('project', $project);
                    $args->put('activity', $activity);
                    $log->store(19, $args);
                }
            }elseif($activity->status == 0){
                $message = ['success.message' => 'Actividade encontra-se no estado "Pendente".'];
            }
        }
        return redirect()->back()->with($message);
    }

    public function changeActivityToRunningState(Project $project, Activity $activity)
    {
        $message = ['error.message' => 'Erro na mudança de estado da actividade para o estado "A Decorrer".'];
        if($project->isClosed() || $project->isCancelled()){
            $message = ['error.message' => 'O projecto encontra-se no estado concluído/cancelado, pelo que não pode alterar o estado da actividade.'];
        } else{
            if($activity->status != 1 && $activity->status != 2){
                $activity->status = 1;
                if($activity->save()){
                    $message = ['success.message' => 'Actividade passou ao estado de "A Decorrer" com sucesso.'];

                    //Adicionar ao Log -> Flag 20 -> decorrer actividade
                    $log = new Log;
                    $args = collect([]);
                    $args->put('project', $project);
                    $args->put('activity', $activity);
                    $log->store(20, $args);
                }
            }elseif($activity->status == 1){
                $message = ['success.message' => 'Actividade encontra-se no estado "A Decorrer".'];
            }
        }
        return redirect()->back()->with($message);
    }

    public function changeActivityToClosedState(Project $project, Activity $activity)
    {
        $message = ['error.message' => 'Erro na mudança de estado da actividade para o estado "Concluído".'];
        if($project->isClosed() || $project->isCancelled()){
            $message = ['error.message' => 'O projecto encontra-se no estado concluído/cancelado, pelo que não pode alterar o estado da actividade.'];
        } else{
            if($activity->status != 2){
                $activity->status = 2;
                if($activity->save()){
                    $message = ['success.message' => 'Actividade passou ao estado de "Concluída" com sucesso.'];

                    //Adicionar ao Log -> Flag 21 -> concluir actividade
                    $log = new Log;
                    $args = collect([]);
                    $args->put('project', $project);
                    $args->put('activity', $activity);
                    $log->store(21, $args);
                }
            }elseif($activity->status == 2){
                $message = ['success.message' => 'Actividade encontra-se no estado "Concluída".'];
            }
        }
        return redirect()->back()->with($message);
    }
}
