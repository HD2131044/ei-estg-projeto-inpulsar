<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Attachment;
use App\Log;
use App\Post;
use App\Project;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function index()
//    {
//        $posts = Post::all();
//        //data mais próxima
//        $posts = Post::latest()->paginate(10);
//
//        return view('home', compact('posts'));
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Project $project
     * @param  Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project, Activity $activity)
    {
        return view('posts.create', compact(['project', 'activity']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Project $project
     * @param  Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Activity $activity)
    {
        if($project->isClosed() || $project->isCancelled() || $activity->isClosed()){
            $message = 'O estado do projecto/actividade não permite criar novo registo.';
            $status = 'error.message';
        }else{
            $post = new Post;

            $response = $this->createPostData($activity, $post);

            if(!empty($response)){
                if($response->status() != 200){
                    return $response;
                }
            }

            $message = 'Erro ao criar registo.';
            $status = 'error.message';

            if($post->save()){
                $message = 'Registo criado com sucesso.';
                $status = 'success.message';

                //Adicionar ao Log -> Flag 25 -> criar post
                $log = new Log;
                $args = collect([]);
                $args->put('project', $project);
                $args->put('activity', $activity);
                $args->put('post', $post);
                $log->store(25, $args);
            }

            if(count(request('anexos'))){
                if(!$this->multipleAttachmentsUpload($activity, $post)) {
                    $message = 'Erro ao criar registo.';
                    $status = 'error.message';
                    $this->destroy($project, $activity, $post);
                }
            }
        }

        Session::flash($status, $message);
        return response()->json(['success' => true]);
    }

    public function createPostData (Activity $activity, Post $post)
    {
        $validator = Validator::make(request()->all(), [
            //'título' => 'required|regex:/^[a-zA-Z ]+$/',
            'título' => 'required',
            'texto' => 'nullable',
            'data' => 'required|after_or_equal:'.$activity->start_date,
            'localização' => 'nullable|max:100',
            'url_registo' => 'nullable|url',
            'número_de_participantes' => 'required'
        ]);

        if($validator->fails()){
            Session::flash('errors', $validator->getMessageBag()->toArray());
            return response()->json('error', 422);
        }

        $post->user_id = Auth::user()->id;
        $post->activity_id = $activity->id;
        $post->title = request('título');
        $post->text = request('texto');
        $post->date = request('data');
        $post->location= request('localização');
        $post->post_url = request('url_registo');
        $post->number_participants = request('número_de_participantes');
    }

    public function multipleAttachmentsUpload(Activity $activity, Post $post){

        //$now = new DateTime('NOW');
        //$dateNow = $now->format('YmdHis');

        if (Input::hasFile('anexos'))
        {
            $files = Input::file('anexos');
            $file_count = count($files);

            foreach(range(0, $file_count-1) as $index) {
                $this->validate(request(), [
                    'anexos.'.$index = 'file'
                ]);
            }

            $uploadCount = 0;
            foreach ($files as $file){
                if($file->isValid()){
                    $file_size = $file->getSize();
                    $file_mimeType = $file->getMimeType();
                    $destinationPath = storage_path('app').'\project'.$activity->project_id;
                    $splitName = explode('.', $file->getFilename(), 2);
                    $extension = $file->getClientOriginalExtension();
                    $filename = $splitName[0].'.'.$extension;
                    $upload_sucess = $file->move($destinationPath, $filename);
                    if($upload_sucess){
                        $attachment = new Attachment();
                        $attachment->user_id = Auth::user()->id;
                        $attachment->project_id = $activity->project_id;
                        $attachment->activity_id = $activity->id;
                        $attachment->post_id = $post->id;
                        $attachment->filename = $filename;
                        $attachment->original_filename = $file->getClientOriginalName();
                        $attachment->size = $file_size;
                        $attachment->mime_type = $file_mimeType;
                        $attachment->media_path = $destinationPath;
                        $attachment->date =  new DateTime('NOW');
                        if($attachment->save()){
                            $uploadCount ++;

                            //Adicionar ao Log -> Flag 30 -> inserir anexo
                            $log = new Log;
                            $args = collect([]);
                            $project = Project::find($attachment->project_id);
                            $args->put('project', $project);
                            $args->put('attachment', $attachment);
                            $log->store(30, $args);
                        }
                    }
                }
            }
            if($uploadCount == $file_count){
                return true;
            }
        }
        return false;
    }

    /**
     * Display the specified resource.
     *
     * @param  Project $project
     * @param  Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project, Activity $activity, Post $post)
    {
        $galleryImages = collect([]);
        if($post->getNumberActiveImages()){
            $galleryImages = $post->getActiveImages()->get();
        }
        return view('posts.show', compact(['project', 'activity', 'post', 'galleryImages']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Project $project, Activity $activity, Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Activity $activity, Post $post)
    {
        return view('posts.edit', compact(['project', 'activity', 'post']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Project $project, Activity $activity, Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Activity $activity, Post $post)
    {
        $response = $this->editPostData($activity, $post);

        if(!empty($response)){
            if($response->status() != 200){
                return $response;
            }
        }

        $message = 'Erro na actualização do registo.';
        $status = 'error.message';

        if(count(request('anexosARemover'))){
            foreach (request('anexosARemover') as $anexo){
                AttachmentsController::destroy($project, Attachment::find(json_decode($anexo, true)['id']));
            }
        }

        if(count(request('anexos'))){ //com anexos
            if($this->multipleAttachmentsUpload($activity, $post)) {
                if ($post->update()) {
                    $message = 'Registo actualizado com sucesso.';
                    $status = 'success.message';

                    //Adicionar ao Log -> Flag 26 -> editar post
                    $log = new Log;
                    $args = collect([]);
                    $args->put('project', $project);
                    $args->put('activity', $activity);
                    $args->put('post', $post);
                    $log->store(26, $args);

                }
            }
        } else { //sem anexos
            if ($post->update()) {
                $message = 'Registo actualizado com sucesso.';
                $status = 'success.message';

                //Adicionar ao Log -> Flag 26 -> editar post
                $log = new Log;
                $args = collect([]);
                $args->put('project', $project);
                $args->put('activity', $activity);
                $args->put('post', $post);
                $log->store(26, $args);
            }
        }

        Session::flash($status, $message);
        return response()->json(['success' => true]);

    }

    public function editPostData(Activity $activity, Post $post)
    {
        $validator = Validator::make(request()->all(), [
            //'título' => 'required|regex:/^[a-zA-Z ]+$/',
            'título' => 'required',
            'texto' => 'nullable',
            'data' => 'required|after_or_equal:'.$activity->start_date,
            'localização' => 'nullable|max:100',
            'url_registo' => 'nullable|url',
            'número_de_participantes' => 'required'
        ]);

        if($validator->fails()){
            Session::flash('errors', $validator->getMessageBag()->toArray());
            return response()->json('error', 422);
        }

        $post->title = request('título');
        $post->text = request('texto');
        $post->date = request('data');
        $post->location= request('localização');
        $post->post_url = request('url_registo');
        $post->number_participants = request('número_de_participantes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Project $project, Activity $activity, Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Activity $activity, Post $post)
    {
        $message = ['error.message' => 'Erro ao apagar registo.'];

        $post_aux = $post;

        //apanhar todos os anexos
        $attachments = collect([]);
        if($post->attachments()->count()){
            $attachments = $post->attachments()->get();
        }

        if($post->delete()){
            $message = ['success.message' => 'Registo apagado com sucesso.'];

            //Adicionar ao Log -> Flag 27 -> apagar post
            $log = new Log;
            $args = collect([]);
            $args->put('project', $project);
            $args->put('activity', $activity);
            $args->put('post', $post_aux);
            $log->store(27, $args);

            //registar todos os logs (resultantes do delete cascate)
            //anexos
            if($attachments->count() > 0){
                foreach($attachments as $attachment){
                    $attachment_aux = $attachment;
                    if($attachment->delete()){
                        $project = Project::withTrashed()->where('id', '=', $attachment_aux->project_id)->first();
                        //Adicionar ao Log -> Flag 37 -> anexo apagado automático após actividade apagada
                        $log = new Log;
                        $args = collect([]);
                        $args->put('project', $project);
                        $args->put('attachment', $attachment_aux);
                        $args->put('action', 'post_delete_auto_attachment_delete');
                        $log->store(37, $args);
                    }
                }
            }

        }
        return redirect()->route('activity.show', compact(['project', 'activity']))->with($message);
    }

    public function fetchTableData($project, $activity, $post){
        $attachments = Post::find($post)->attachments()->get();
        return response()->json(['attachments' => $attachments]);
    }

    public function changePostToActiveState(Project $project, Activity $activity, Post $post)
    {
        $message = ['error.message' => 'Erro ao activar registo.'];

        if ($post->active == 0) {
            $post->active = 1;
            if($post->save()){
                $message = ['success.message' => 'Registo activado com sucesso.'];

                //Adicionar ao Log -> Flag 28 -> post activo
                $log = new Log;
                $args = collect([]);
                $args->put('project', $project);
                $args->put('activity', $activity);
                $args->put('post', $post);
                $log->store(28, $args);
            }
        }else{
            $message = ['success.message' => 'Registo encontra-se activo.'];
        }
        return redirect()->back()->with($message);
    }

    public function changePostToInactiveState(Project $project, Activity $activity, Post $post)
    {
        $message = ['error.message' => 'Erro ao desactivar registo.'];

        if ($post->active == 1) {
            $post->active = 0;
            if($post->save()){
                $message = ['success.message' => 'Registo desactivado com sucesso.'];

                //Adicionar ao Log -> Flag 29 -> post inactivo
                $log = new Log;
                $args = collect([]);
                $args->put('project', $project);
                $args->put('activity', $activity);
                $args->put('post', $post);
                $log->store(29, $args);
            }
        }else{
            $message = ['success.message' => 'Registo encontra-se inactivo.'];
        }
        return redirect()->back()->with($message);
    }
}
