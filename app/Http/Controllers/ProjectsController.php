<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Log;
use App\Post;
use App\Project;
use App\User;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Attachment;
use Illuminate\Support\Facades\Validator;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function index()
//    {
//        //$projects = Project::all();
//        //data mais próxima
//        $projects = Project::latest()->paginate(10);
//
//        return view('home', compact('projects'));
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $project = new Project;

        $response = $this->createProjectData($project);

        if(!empty($response)){
            if($response->status() != 200){
                return $response;
            }
        }

        $message = 'Erro ao criar projecto.';
        $status = 'error.message';

        if($project->save()){

            if (Input::hasFile('foto'))
            {
                $file = Input::file('foto');
                if($file->isValid()){
                    $file_size = $file->getSize();
                    $file_mimeType = $file->getMimeType();
                    $destinationPath = storage_path('app').'\project' . $project->id;
                    //$splitName = explode('.', $file->getFilename(), 2);
                    $splitName = 'Project' . $project->id . 'Foto';
                    $extension = $file->getClientOriginalExtension();
                    $filename = $splitName.'.'.$extension;
                    $upload_sucess = $file->move($destinationPath, $filename);
                    if($upload_sucess){
                        $project->project_photo = $destinationPath . '\\' .  $filename;
                        $project->save();
                    }
                }
            }

            $message = 'Projecto criado com sucesso.';
            $status = 'success.message';
            //$project->users()->attach(Auth::user()->id);

            //Adicionar ao Log -> Flag 8 -> criar projecto
            $log = new Log;
            $args = collect([]);
            $args->put('project', $project);
            $log->store(8, $args);

            if(count(request('managers'))){
                foreach (json_decode(request('managers')) as $user){
                    $project->users()->attach($user->id);
                }
            }
        };

        if(count(request('anexos'))){
            if(!$this->multipleAttachmentsUpload($project)) {
                $message = 'Erro ao criar projecto.';
                $status = 'error.message';
                $this->destroy($project);
            }
        }

        Session::flash($status, $message);
        return response()->json(['success' => true]);
    }

    public function createProjectData(Project $project)
    {
        $validator = Validator::make(request()->all(), [
            'data_de_início' => 'required',
        ]);

        if($validator->fails()){
            Session::flash('errors', $validator->getMessageBag()->toArray());
            return response()->json('error', 422);
        }

        $startDate = request('data_de_início');

        $validator = Validator::make(request()->all(), [
            //'nome' => 'required|regex:/^[a-zA-Z ]+$/',
            'nome' => 'required',
            'descrição' => 'nullable|max:200',
            //'data_de_início' => 'required|after_or_equal:' . $dateNow,
            'data_de_fim' => 'nullable|after:' . $startDate,
            'localização' => 'nullable|max:100',
            'url_projecto' => 'nullable|url',
            'estado_do_projecto' => 'required|digits_between:0,2',
            'estado_da_candidatura' => 'required|boolean',
            'vezes_que_a_candidatura_foi_efectuada' => 'required'
        ]);

        if($validator->fails()){
            Session::flash('errors', $validator->getMessageBag()->toArray());
            return response()->json('error', 422);
        }

        $project->creator_id = Auth::user()->id;
        $project->name = request('nome');
        $project->description = request('descrição');
        $project->start_date = request('data_de_início');
        $project->end_date = request('data_de_fim');
        $project->location= request('localização');
        $project->project_url = request('url_projecto');
        $project->status = request('estado_do_projecto');
        $project->submitted = request('estado_da_candidatura');
        $project->times_submitted = request('vezes_que_a_candidatura_foi_efectuada');
        //se projecto não está no estado pendente é garante que foi submetida uma candidatura
        /*if($project->status != 0){
            $project->submitted = 1;
            $project->times_submitted = 1;
        }*/
    }

    public function multipleAttachmentsUpload(Project $project){

        //$now = new DateTime('NOW');
        //$dateNow = $now->format('YmdHis');

        if (request()->hasFile('anexos'))
        {
            $files = request('anexos');
            $file_count = count($files);

            foreach(range(0, $file_count-1) as $index) {
                $this->validate(request(), [
                    'anexos.'.$index = 'file'
                ]);
            }

            $attachments = $project->attachments()->get()->keyBy('original_filename');

            $uploadCount = 0;
            foreach ($files as $file){
                if(!array_key_exists($file->getClientOriginalName(), $attachments)){
                    if($file->isValid()){
                        $file_size = $file->getSize();
                        $file_mimeType = $file->getMimeType();
                        $destinationPath = storage_path('app').'\project'.$project->id;
                        $splitName = explode('.', $file->getFilename(), 2);
                        $extension = $file->getClientOriginalExtension();
                        $filename = $splitName[0].'.'.$extension;
                        $upload_sucess = $file->move($destinationPath, $filename);
                        if($upload_sucess){
                            $attachment = new Attachment();
                            $attachment->user_id = Auth::user()->id;
                            $attachment->project_id = $project->id;
                            $attachment->filename = $filename;
                            $attachment->original_filename = $file->getClientOriginalName();
                            $attachment->size = $file_size;
                            $attachment->mime_type = $file_mimeType;
                            $attachment->media_path = $destinationPath;
                            $attachment->date =  new DateTime('NOW');
                            if($attachment->save()){
                                $uploadCount ++;

                                //Adicionar ao Log -> Flag 30 -> inserir anexo
                                $log = new Log;
                                $args = collect([]);
                                $args->put('project', $project);
                                $args->put('attachment', $attachment);
                                $log->store(30, $args);
                            }
                        }
                    }
                } else {
                    foreach ($attachments as $attachment){
                        
                    }
                }
            }
            if($uploadCount == $file_count){
                return true;
            }
        }
        return false;
    }

    /**
     * Display the specified resource.
     *
     * @param  Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $galleryImages = collect([]);
        if($project->getNumberActiveImages()){
            $galleryImages = $project->getActiveImages()->get();
        }
        return view('projects.show', compact(['project', 'galleryImages']));
    }

    public function showProjectInfo(Project $project, Activity $activity)
    {
        return view('projects.show-minimum-detail', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        return view('projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project)
    {
        $old_status = $project->status;
        $collectionWithMessageState = collect([]);

        $response = $this->editProjectData($project);

        if(!empty($response)){
            if($response->status() != 200){
                return $response;
            }
        }

        //todas as actividades passam a status concluido
        if($old_status != 2 && $project->status == 2) {
            $collectionWithMessageState = $this->updateCloseAllProjectActivities($project);
        }elseif ($old_status == 2 && $project->status < 2){//reverte todas as actividades para o status anterior
            $collectionWithMessageState = $this->revertUpdateCloseAllProjectActivities($project);
        }else{
            $collectionWithMessageState = $this->normalUpdataCase($project);
        }

        $status = $collectionWithMessageState[0];
        $message = $collectionWithMessageState[1];

        \Session::flash($status, $message);
        return response()->json(['success' => true]);

    }

    public function normalUpdataCase(Project $project){

        $collectionWithMessageState = collect([]);

        $status = 'error.message';
        $message = 'Erro na actualização do projecto.';

        if(count(request('anexosARemover'))){
            foreach (request('anexosARemover') as $anexo){
                AttachmentsController::destroy($project, Attachment::find(json_decode($anexo, true)['id']));
            }
        }

        if (Input::hasFile('foto'))
        {
            $file = Input::file('foto');
            if($file->isValid()){
                $file_size = $file->getSize();
                $file_mimeType = $file->getMimeType();
                $destinationPath = storage_path('app').'\project' . $project->id;
                //$splitName = explode('.', $file->getFilename(), 2);
                $splitName = 'Project' . $project->id . 'Foto';
                $extension = $file->getClientOriginalExtension();
                $filename = $splitName.'.'.$extension;
                $upload_sucess = $file->move($destinationPath, $filename);
                if($upload_sucess){
                    $project->project_photo = $destinationPath . '\\' .  $filename;
                }
            }
        }

        if (count(request('anexos'))) {
            if ($this->multipleAttachmentsUpload($project)) {
                if ($project->update()) {
                    $status = 'success.message';
                    $message = 'Projecto actualizado com sucesso.';

                    //Adicionar ao Log -> Flag 9 -> editar projecto
                    $log = new Log;
                    $args = collect([]);
                    $args->put('project', $project);
                    $log->store(9, $args);

                    if(count(request('managers'))){
                        $project->users()->detach(array_column($project->users()->where('role', '<', '3')->get(['id'])->toArray(), 'id'));

                        foreach (request('managers') as $managerId){
                            if(!$project->users()->find($managerId)){
                                $project->users()->attach($managerId);
                            }
                        }
                    } else {
                        $project->users()->detach(array_column($project->users()->where('role', '<', '3')->get(['id'])->toArray(), 'id'));
                    }
                }
            }
        } else {
            if ($project->update()) {
                $status = 'success.message';
                $message = 'Projecto actualizado com sucesso.';

                //Adicionar ao Log -> Flag 9 -> editar projecto
                $log = new Log;
                $args = collect([]);
                $args->put('project', $project);
                $log->store(9, $args);

                if(count(request('managers'))){
                    $project->users()->detach(array_column($project->users()->where('role', '<', '3')->get(['id'])->toArray(), 'id'));
                    foreach (request('managers') as $managerId){
                        if(!$project->users()->find($managerId)){
                            $project->users()->attach($managerId);
                        }
                    }
                } else {
                    $project->users()->detach(array_column($project->users()->where('role', '<', '3')->get(['id'])->toArray(), 'id'));
                }
            }
        }

        $collectionWithMessageState->push($status);
        $collectionWithMessageState->push($message);

        return $collectionWithMessageState;
    }


    public function updateCloseAllProjectActivities(Project $project){

        $collectionWithMessageState = collect([]);

        $status = 'error.message';
        $message = 'Erro na actualização do projecto.';

        if ($project->closeAllActivities()) {

            if(count(request('anexosARemover'))){
                foreach (request('anexosARemover') as $anexo){
                    AttachmentsController::destroy($project, Attachment::find(json_decode($anexo, true)['id']));
                }
            }

            if (count(request('anexos'))) {
                if ($this->multipleAttachmentsUpload($project)) {
                    if ($project->update()) {
                        $status = 'success.message';
                        $message = 'Projecto actualizado com sucesso.';

                        //Adicionar ao Log -> Flag 9 -> editar projecto
                        $log = new Log;
                        $args = collect([]);
                        $args->put('project', $project);
                        $log->store(9, $args);

                        if(count(request('managers'))){
                            $project->users()->detach(array_column($project->users()->where('role', '<', '3')->get(['id'])->toArray(), 'id'));

                            foreach (request('managers') as $managerId){
                                if(!$project->users()->find($managerId)){
                                    $project->users()->attach($managerId);
                                }
                            }
                        } else {
                            $project->users()->detach(array_column($project->users()->where('role', '<', '3')->get(['id'])->toArray(), 'id'));
                        }
                    } else {
                        $project->revertCloseAllActivities();
                        $status = 'error.message';
                        $message = 'Erro na mudança de estado do projecto para o estado "Concluído".';
                    }
                }
            } else {
                if ($project->update()) {
                    $status = 'success.message';
                    $message = 'Projecto actualizado com sucesso.';

                    //Adicionar ao Log -> Flag 9 -> editar projecto
                    $log = new Log;
                    $args = collect([]);
                    $args->put('project', $project);
                    $log->store(9, $args);

                    if(count(request('managers'))){
                        $project->users()->detach(array_column($project->users()->where('role', '<', '3')->get(['id'])->toArray(), 'id'));

                        foreach (request('managers') as $managerId){
                            if(!$project->users()->find($managerId)){
                                $project->users()->attach($managerId);
                            }
                        }
                    } else {
                        $project->users()->detach(array_column($project->users()->where('role', '<', '3')->get(['id'])->toArray(), 'id'));
                    }
                } else {
                    $project->revertCloseAllActivities();
                    $status = 'error.message';
                    $message = 'Erro na mudança de estado do projecto para o estado "Concluído".';
                }
            }
        } else {
            $status = 'error.message';
            $message = 'Erro na mudança de estado do projecto para o estado "Concluído".';
        }

        $collectionWithMessageState->push($status);
        $collectionWithMessageState->push($message);

        return $collectionWithMessageState;
    }


    public function revertUpdateCloseAllProjectActivities(Project $project)
    {
        $collectionWithMessageState = collect([]);

        $status = 'error.message';
        $message = 'Erro na actualização do projecto.';

        if ($project->revertCloseAllActivities()) { //TRANSAÇÃO OK
            if(count(request('anexosARemover'))){
                foreach (request('anexosARemover') as $anexo){
                    AttachmentsController::destroy($project, Attachment::find(json_decode($anexo, true)['id']));
                }
            }

            if (count(request('anexos'))) {
                if ($this->multipleAttachmentsUpload($project)) {
                    if ($project->update()) {

                        $status = 'success.message';
                        $message = 'Projecto actualizado com sucesso.';

                        //Adicionar ao Log -> Flag 9 -> editar projecto
                        $log = new Log;
                        $args = collect([]);
                        $args->put('project', $project);
                        $log->store(9, $args);

                        if(count(request('managers'))){
                            $project->users()->detach(array_column($project->users()->where('role', '<', '3')->get(['id'])->toArray(), 'id'));

                            foreach (request('managers') as $managerId){
                                if(!$project->users()->find($managerId)){
                                    $project->users()->attach($managerId);
                                }
                            }
                        } else {
                            $project->users()->detach(array_column($project->users()->where('role', '<', '3')->get(['id'])->toArray(), 'id'));
                        }
                    } else { //TRANSAÇÃO KO
                        $status = 'error.message';
                        $message = 'Erro na mudança de estado do projecto para o estado "Concluído".';
                    }
                }
            } else {
                if ($project->update()) {
                    $status = 'success.message';
                    $message = 'Projecto actualizado com sucesso.';

                    //Adicionar ao Log -> Flag 9 -> editar projecto
                    $log = new Log;
                    $args = collect([]);
                    $args->put('project', $project);
                    $log->store(9, $args);

                    if(count(request('managers'))){
                        $project->users()->detach(array_column($project->users()->where('role', '<', '3')->get(['id'])->toArray(), 'id'));

                        foreach (request('managers') as $managerId){
                            if(!$project->users()->find($managerId)){
                                $project->users()->attach($managerId);
                            }
                        }
                    } else {
                        $project->users()->detach(array_column($project->users()->where('role', '<', '3')->get(['id'])->toArray(), 'id'));
                    }
                } else { //TRANSAÇÃO KO
                    $status = 'error.message';
                    $message = 'Erro na mudança de estado do projecto para o estado "Concluído".';
                }
            }

        } else { //TRANSAÇÃO KO
            $status = 'error.message';
            $message = 'Erro na mudança de estado do projecto para o estado "Concluído".';
        }

        $collectionWithMessageState->push($status);
        $collectionWithMessageState->push($message);

        return $collectionWithMessageState;

    }

    public function editProjectData(Project $project)
    {
        $old_status = $project->status;
        $old_submitted = $project->submitted;
        $old_times_submitted = $project->times_submitted;
/*        if(request('data_de_início') != $project->start_date){
            $now = new DateTime('today');
            $dateNow = $now->format('Y-m-d');

            $this->validate(request(), [
                'data_de_início' => 'required|after_or_equal:' . $dateNow,
            ]);
            $project->start_date = request('data_de_início');
        }*/
        $validator = Validator::make(request()->all(), [
            'data_de_início' => 'required',
        ]);

        if($validator->fails()){
            Session::flash('errors', $validator->getMessageBag()->toArray());
            return response()->json('error', 422);
        }

        $startDate = request('data_de_início');

        $validator = Validator::make(request()->all(), [
            //'nome' => 'required|regex:/^[a-zA-Z ]+$/',
            'nome' => 'required',
            'descrição' => 'nullable|max:200',
            'data_de_fim' => 'nullable|after:' . $startDate,
            'localização' => 'nullable|max:100',
            'url_projecto' => 'nullable|url',
            'estado_do_projecto' => 'required|digits_between:0,2',
            'estado_da_candidatura' => 'required|boolean',
            'vezes_que_a_candidatura_foi_efectuada' => 'required'
        ]);

        if($validator->fails()){
            Session::flash('errors', $validator->getMessageBag()->toArray());
            return response()->json('error', 422);
        }

        $project->name = request('nome');
        $project->description = request('descrição');
        $project->start_date = request('data_de_início');
        $project->end_date = request('data_de_fim');
        $project->location= request('localização');
        $project->project_url = request('url_projecto');
        $project->status = request('estado_do_projecto');
        $project->submitted = request('estado_da_candidatura');
        $project->times_submitted = request('vezes_que_a_candidatura_foi_efectuada');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $message = ['error.message' => 'Erro ao apagar projecto.'];

        $project_aux = $project;

        //apanhar todas as activities
        $activities = collect([]);

        //apanhar todos os posts
        $posts = collect([]);

        if($project->activities()->count()){
            $activities = $project->activities()->get();
            foreach($activities as $activity){
                if($activity->posts()->count()){
                    foreach ($activity->posts()->get() as $post){
                        $posts->push($post);
                    }
                }
            }
        }
        //apanhar todos os anexos
        $attachments = collect([]);
        if($project->attachments()->count()){
            $attachments = $project->attachments()->get();
        }

        if($project->delete()){
            $message = ['success.message' => 'Projecto apagado com sucesso.'];

            //Adicionar ao Log -> Flag 10 -> apagar projecto
            $log = new Log;
            $args = collect([]);
            $args->put('project', $project_aux);
            $log->store(10, $args);

            //registar todos os logs (resultantes do delete cascate)
            //actividades
            if($activities->count() > 0){
                foreach($activities as $activity){
                    $activity_aux = $activity;
                    if($activity->delete()){
                        //Adicionar ao Log -> Flag 35 -> apagar actividade automática após projecto apagado
                        $log = new Log;
                        $args = collect([]);
                        $args->put('project', $project_aux);
                        $args->put('activity', $activity_aux);
                        $log->store(35, $args);
                    }
                }

            }
            //posts
            if($posts->count() > 0){
                foreach($posts as $post){
                    $post_aux = $post;
                    if($post->delete()){
                        $activity = Activity::withTrashed()->where('id', '=', $post_aux->activity_id)->first();
                        //Adicionar ao Log -> Flag 36 -> apagar post automático após projecto apagado
                        $log = new Log;
                        $args = collect([]);
                        $args->put('project', $project_aux);
                        $args->put('activity', $activity);
                        $args->put('post', $post_aux);
                        $args->put('action', 'project_delete_auto_post_delete');
                        $log->store(36, $args);
                    }
                }
            }
            //anexos
            if($attachments->count() > 0){
                foreach($attachments as $attachment){
                    $attachment_aux = $attachment;
                    if($attachment->delete()){
                        //Adicionar ao Log -> Flag 37 -> anexo apagado automático após projecto apagado
                        $log = new Log;
                        $args = collect([]);
                        $args->put('project', $project_aux);
                        $args->put('attachment', $attachment_aux);
                        $args->put('action', 'project_delete_auto_attachment_delete');
                        $log->store(37, $args);
                    }
                }
            }

        }
        return redirect()->route('home')->with($message);
    }

    public function fetchTableData($project){
        $managers = Project::find($project)->users()->where('role', '<=', 2)->where('role', '>', 0)->get();
        $allManagers = User::where('role', '<=', '2')->where('role', '>', 0)->get()->diff($managers);
        $attachments = Project::find($project)->attachments()->get();
        return response()->json(['managers' => $managers, 'allManagers' => $allManagers, 'attachments' => $attachments]);
    }

    public function fetchManagers($project){
        $managers = Project::find($project)->users()->where('role', '<=', 2)->where('role', '>', 0)->get()->toArray();
        return response()->json(['data' => $managers]);
    }

    public function fetchAllManagers($project){
        $managers = Project::find($project)->users()->where('role', '<=', '2')->where('role', '>', 0)->get();
        $allManagers = User::where('role', '<=', '2')->where('role', '>', 0)->get()->diff($managers);

        return response()->json(['data' => $allManagers]);
    }

    public function getManagers(){
        $allManagers = User::where('role', '<=', '2')->where('role', '>', 0)->get()->toArray();
        return response()->json(['data' => $allManagers]);
    }

    public function removeManager(Project $project, User $user){
        Project::find($project)->users()->detach($user);
        return response()->json(['success' => 'true']);
    }

    public function addManager(Project $project, User $user){
        Project::find($project)->users()->attach($user);
        return response()->json(['success' => 'true']);
    }

    public function fetchAttachments($project){
        return Project::find($project)->attachments();
    }

    public function changeProjectToPendingState(Project $project)
    {
        $message = ['error.message' => 'Erro na mudança de estado do projecto para o estado "Pendente".'];
        if($project->status == 0){
            $message = ['success.message' => 'Projecto encontra-se no estado "Pendente", de forma a poder resubmeter a sua candidatura.'];
        }else{
            //passar de cancelado para concluido -> re-submissão da candidatura
            if($project->isSubmitted()){
                if($project->status != 0 && $project->status != 1 && $project->status != 2)
                {
                    $project->status = 0;
                    $project->submitted = 0;
                    if($project->save()){
                        $message = ['success.message' => 'Projecto passou ao estado de "Pendente" com sucesso, de forma a poder resubmeter a sua candidatura.'];

                        //Adicionar ao Log -> Flag 15 -> resubmeter projecto
                        $log = new Log;
                        $args = collect([]);
                        $args->put('project', $project);
                        $log->store(15, $args);
                    }
                }
            }
        }
        return redirect()->back()->with($message);
    }

    public function changeProjectToRunningState(Project $project)
    {
        $message = ['error.message' => 'Erro na mudança de estado do projecto para o estado "A Decorrer".'];
        if($project->status == 1){
            $message = ['success.message' => 'Projecto encontra-se no estado "A Decorrer".'];
            return redirect()->back()->with($message);
        }else{
            if($project->isSubmitted()) {
                if($project->status != 1 && $project->status != 2 && $project->status != 3)
                {
                    $project->status = 1;
                    if($project->save()){
                        $message = ['success.message' => 'Projecto passou ao estado de "A Decorrer" com sucesso.'];

                        //Adicionar ao Log -> Flag 13 -> decorrer projecto
                        $log = new Log;
                        $args = collect([]);
                        $args->put('project', $project);
                        $log->store(13, $args);
                    }
                }
                return redirect()->back()->with($message);
            }else{
                $message = ['error.message' => 'Erro na mudança de estado do projecto para o estado "A Decorrer". Tem de submeter a candidatura do projeto primeiro.'];
                return redirect()->back()->with($message);
            }
        }
    }

    public function changeProjectToClosedState(Project $project)
    {
        $message = ['error.message' => 'Erro na mudança de estado do projecto para o estado "Concluído".'];
        if($project->status == 2){
            $message = ['success.message' => 'Projecto encontra-se no estado "Concluído".'];
            return redirect()->back()->with($message);
        }else{
            //Não é necessário validar a submissão pois se está a decorrer -> estado passar a concluir não depende da submissão
            /*if($project->isSubmitted()) {*/
                if ($project->status != 2 && $project->status != 0 && $project->status != 3){
                    //primeiro verificar se muda todas as actividades
                    //sim - TRANSAÇÃO OK -> if($project->closeAllActivities())
                    if ($project->closeAllActivities()) {
                        //muda o estado do projeto
                        $project->status = 2;
                        //grava projeto if($project->save())
                        //sim ok com msg de sucesso
                        if ($project->save()) {
                            $message = ['success.message' => 'Projecto passou ao estado de "Concluído" com sucesso.'];

                            //Adicionar ao Log -> Flag 14 -> concluir projecto
                            $log = new Log;
                            $args = collect([]);
                            $args->put('project', $project);
                            $log->store(14, $args);

                        } else { //não reverte tudo outra vez -> TOTAL
                            $project->revertCloseAllActivities();
                            $message = ['error.message' => 'Erro na mudança de estado do projecto para o estado "Concluído".'];
                        }
                    }
                }
                return redirect()->back()->with($message);
            /*}else{
                $message = ['error.message' => 'Erro na mudança de estado do projecto para o estado "Concluído". Tem de submeter a candidatura do projeto primeiro.'];
                return redirect()->back()->with($message);
            }*/
        }
    }

    public function changeProjectToCancelledState(Project $project)
    {
        $message = ['error.message' => 'Erro na mudança de estado do projecto para o estado "Cancelado".'];
        if($project->status == 3){
            $message = ['success.message' => 'Projecto encontra-se no estado "Cancelado".'];
            return redirect()->back()->with($message);
        }else{
            if($project->isSubmitted()){
                if($project->status != 3 && $project->status != 1 && $project->status != 2)
                {
                    $project->status = 3;
                    if($project->save()){
                        $message = ['success.message' => 'Projecto passou ao estado de "Cancelado" com sucesso.'];

                        //Adicionar ao Log -> Flag 12 -> cancelar projecto
                        $log = new Log;
                        $args = collect([]);
                        $args->put('project', $project);
                        $log->store(12, $args);
                    }
                }
                return redirect()->back()->with($message);
            }else{
                $message = ['error.message' => 'Erro na mudança de estado do projecto para o estado "Cancelado". Tem de submeter a candidatura do projeto primeiro.'];
                return redirect()->back()->with($message);
            }
        }
    }

    public function submitProject(Project $project)
    {
        $message = ['error.message' => 'Erro na submissão da candidatura do projecto.'];
        if(!$project->isSubmitted()){
            $project->submitted = 1;
            $project->times_submitted += 1;
            if($project->save()){
                $message = ['success.message' => 'Submissão da candidatura do projecto com sucesso.'];

                //Adicionar ao Log -> Flag 11 -> submeter projecto
                $log = new Log;
                $args = collect([]);
                $args->put('project', $project);
                $log->store(11, $args);
            }
        }else{
            $message = ['success.message' => 'Submissão da candidatura do projecto encontra-se efectuada.'];
        }
        return redirect()->back()->with($message);
    }
}
