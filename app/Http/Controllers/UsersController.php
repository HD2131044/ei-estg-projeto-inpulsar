<?php

namespace App\Http\Controllers;

use App\ActivationService;

use App\Log;
use App\User;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Password;

class UsersController extends Controller
{
    protected $activationService;

    public function __construct(ActivationService $activationService)
    {
        $this->activationService = $activationService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function index() //chamado no HomeController
//    {
//        //$users = User::all();
//        $users = User::paginate(10);
//
//        return view('home', compact('users'));
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $user = new User;
        $random_password = str_random(9);

        $this->createUserData($user, $random_password);

        $message = ['error.message' => 'Erro ao criar utilizador.'];

        if($user->save()){

            if (Input::hasFile('foto'))
            {
                $file = Input::file('foto');

                if($file->isValid()){
                    $file_size = $file->getSize();
                    $file_mimeType = $file->getMimeType();
                    $destinationPath = storage_path('app').'\users';
                    //$splitName = explode('.', $file->getFilename(), 2);
                    $splitName = 'User' . $user->id . 'Foto';
                    $extension = $file->getClientOriginalExtension();
                    $filename = $splitName.'.'.$extension;
                    $upload_sucess = $file->move($destinationPath, $filename);
                    if($upload_sucess){
                        $user->profile_photo = $destinationPath . '\\' .  $filename;
                        $user->save();
                    }
                }
            }

            if($this->activationService->sendActivationMail($user, $random_password)){
                $message = ['success.message' => 'Utilizador criado com sucesso. Foi enviado uma notificação para o email do novo utilizador para activação da sua conta.'];

                //Adicionar ao Log -> Flag 1 -> adicionar utilizador
                $log = new Log;
                $args = collect([]);
                $args->put('user', $user);
                $log->store(1, $args);
            }
        };

        return redirect()->route('home')->with($message);
    }

    public function createUserData(User $user, $random_password)
    {
        $now = new DateTime('today');
        $dateNow = $now->format('Y-m-d');

        $this->validate(request(), [
            'nome' => 'required|min:5',
            'email' => 'required|email|unique:users,email',
            'data_de_nascimento' => 'required|before:' . $dateNow,
            'sexo' => 'required',
            'morada' => 'required|max:100',
            'foto' => 'nullable|image',
            'url_pessoal' => 'nullable|url', //ou validar com 'active_url'
            'descrição' => 'nullable|max:200',
            'tipo_de_conta' => 'required|digits_between:1,3',
            'pode_publicar_no_facebook' => 'required'
        ]);

        $user->name = request('nome');
        $user->email = request('email');
        $user->password = password_hash($random_password, PASSWORD_DEFAULT);
        $user->birth_date = request('data_de_nascimento');
        $user->gender = request('sexo');
        $user->address = request('morada');
        $user->profile_url = request('url_pessoal');
        $user->description = request('descrição');
        $user->role = request('tipo_de_conta');
        $user->publisher = request('pode_publicar_no_facebook');
        //$user->blocked = request('estado_bloqueado');

    }

    /**
     * Display the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    public function showProfile()
    {
        $user = Auth::user();
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    public function editProfile()
    {
        $user = Auth::user();
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(User $user)
    {
        $this->editUserData($user);
        $message = ['error.message' => 'Erro na actualização do utilizador.'];

        if ($user->update()) {
            $message = ['success.message' => 'Utilizador actualizado com sucesso.'];

            //Adicionar ao Log -> Flag 2 -> editar utilizador
            $log = new Log;
            $args = collect([]);
            $args->put('user', $user);
            $log->store(2, $args);
        }

        return redirect()->route('user.show', compact('user'))->with($message);
    }

    public function updateprofile()
    {
        $user = Auth::user();

        $this->editUserData($user);
        $message = ['error.message' => 'Erro na actualização do Perfil.'];


        if ($user->update()) {
            $message = ['success.message' => 'Perfil actualizado com sucesso.'];

            //Adicionar ao Log -> Flag 2 -> editar utilizador
            $log = new Log;
            $args = collect([]);
            $args->put('user', $user);
            $log->store(2, $args);
        }

        return redirect()->route('user.show-profile')->with($message);
    }

    public function editUserData(User $user)
    {
        $now = new DateTime('today');
        $dateNow = $now->format('Y-m-d');

        if(request('email') != $user->email){
            $this->validate(request(), [
                'email' =>'required|email|unique:users,email'
            ]);
            $user->email = request('email');
        }
        $this->validate(request(), [
            'nome' => 'required|min:5',
            'data_de_nascimento' => 'required|before:' . $dateNow,
            'sexo' => 'required',
            'morada' => 'required|max:100',
            'foto' => 'nullable|image',
            'url_pessoal' => 'nullable|url', //ou validar com 'active_url'
            'descrição' => 'nullable|max:200',
            'tipo_de_conta' => 'required|digits_between:1,3',
            'pode_publicar_no_facebook' => 'required',
        ]);

        $user->name = request('nome');
        $user->birth_date = request('data_de_nascimento');
        $user->gender = request('sexo');
        $user->address = request('morada');
        $user->profile_url = request('url_pessoal');
        $user->description = request('descrição');
        $user->role = request('tipo_de_conta');
        $user->publisher = request('pode_publicar_no_facebook');
        if (Input::hasFile('foto'))
        {
            $file = Input::file('foto');

            if($file->isValid()){
                $file_size = $file->getSize();
                $file_mimeType = $file->getMimeType();
                $destinationPath = storage_path('app').'\users';
                //$splitName = explode('.', $file->getFilename(), 2);
                $splitName = 'User' . $user->id . 'Foto';
                $extension = $file->getClientOriginalExtension();
                $filename = $splitName.'.'.$extension;
                $upload_sucess = $file->move($destinationPath, $filename);
                if($upload_sucess){
                    $user->profile_photo = $destinationPath . '\\' .  $filename;
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @@param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $message = ['error.message' => 'Erro ao apagar utilizador.'];

        $id_user = $user->id;

        if($user->delete()){
            $message = ['success.message' => 'Utilizador apagado com sucesso'];

            //Adicionar ao Log -> Flag 3 -> apagar utilizador
            $log = new Log;
            $args = collect([]);
            $args->put('user', $user);
            $log->store(3, $args);

            //para substituir o email (uma vez que é campo único não permitindo uma futura nova criação"
            $user_deleted = User::withTrashed()->where('id', '=', $id_user)->first();
            $now = new DateTime();
            $dateNow = $now->format('YmdHis');
            $user_deleted->email = "deleted-".$dateNow."-".$user_deleted->email;
            $user_deleted->save();

        }

        return redirect()->route('home')->with($message);
    }

    public function changeUserToBlockedState(User $user)
    {
        $message = ['error.message' => 'Erro ao bloquear utilizador.'];

        if($user->blocked == 0){
            $user->blocked = 1;
            if($user->save()){
                $message = ['success.message' => 'Utilizador bloqueado com sucesso.'];

                //Adicionar ao Log -> Flag 4 -> bloquear utilizador
                $log = new Log;
                $args = collect([]);
                $args->put('user', $user);
                $log->store(4, $args);
            }
        }else{
            $message = ['success.message' => 'Utilizador encontra-se bloqueado.'];
        }
        return redirect()->back()->with($message);
    }

    public function changeUserToUnblockedState(User $user){

        $message = ['error.message' => 'Erro ao desbloquear utilizador.'];

        if($user->blocked == 1){
            $user->blocked = 0;
            if($user->save()){
                $message = ['success.message' => 'Utilizador desbloqueado com sucesso.'];

                //Adicionar ao Log -> Flag 5 -> desbloquear utilizador
                $log = new Log;
                $args = collect([]);
                $args->put('user', $user);
                $log->store(5, $args);
            }
        }else{
            $message = ['success.message' => 'Utilizador encontra-se desbloqueado.'];
        }
        return redirect()->back()->with($message);
    }

    public function adminActivationEmail(User $user){

        $message = ['error.message' => 'Erro na criação de email com link de activação.'];

        if(!$user->isActivated() && !$user->isBlocked()){
            $random_password = str_random(9);
            $user->password = password_hash($random_password, PASSWORD_DEFAULT);

            if ($user->update()) {
                if ($this->activationService->sendActivationMail($user, $random_password)){
                    $message = ['success.message' => 'Email com link de activação enviado para o utilizador.'];

                    //Adicionar ao Log -> Flag 7 -> enviado email de activação ao utilizador -> forma manual
                    $log = new Log;
                    $args = collect([]);
                    $args->put('user', $user);
                    $log->store(7, $args);

                }else{
                    $message = ['error.message' => 'A conta do utilizador já foi activada.'];
                };
            }
        }

        if($user->isActivated()){
            $message = ['error.message' => 'A conta do utilizador já foi activada.'];
        }

        if($user->isBlocked()){
            $message = ['error.message' => 'A conta do utilizador está bloqueada! Não foi enviado email com link de activação ao utilizador.'];
        }

        return redirect()->back()->with($message);

    }

    public function adminPasswordReset(User $user)
    {

        $message = ['error.message' => 'Erro ao fazer reset à password do utilizador.'];

        //apenas se user não está bloqueado e tem conta activa
        if(!$user->isBlocked() && $user->isActivated()){
            //Gerar password aleatória na BD - para evitar a entrada com a password antiga
            $random_password = str_random(9);
            $user->password = password_hash($random_password, PASSWORD_DEFAULT);

            if(!$user->update()){
                $message = ['error.message' => 'Erro ao fazer reset à password do utilizador.'];
                return redirect()->back()->with($message);
            }

            //Chamar resetPassword
            $this->sendResetLinkEmailAdmin($user);

            $message = ['success.message' => 'Foi enviado email de reset da password ao utilizador.'];

            //Adicionar ao Log -> Flag 6 -> reeniciar password do utilizador
            $log = new Log;
            $args = collect([]);
            $args->put('user', $user);
            $log->store(6, $args);
        }

        if(!$user->isActivated()){
            $message = ['error.message' => 'A conta do utilizador encontra-se por activar! Não foi enviado email de reset da password ao utilizador.'];
        }

        if($user->isBlocked()){
            $message = ['error.message' => 'A conta do utilizador está bloqueada! Não foi enviado email de reset da password ao utilizador.'];
        }

        return redirect()->back()->with($message);

    }

    protected function sendResetLinkEmailAdmin(User $user)
    {
        $array = ['email'=>$user->email]; //sendResetLink recebe como parâmetro um array de strings com keys email
        $response = $this->broker()->sendResetLink(
            $array
        );

        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse($response)
            : $this->sendResetLinkFailedResponseAdmin($response);
    }

    protected function sendResetLinkResponse($response)
    {
        return back()->with('status', trans($response));
    }

    protected function sendResetLinkFailedResponseAdmin($response)
    {
        return back()->withErrors(
            ['email' => trans($response)]
        );
    }

    public function broker()
    {
        return Password::broker();
    }

}
