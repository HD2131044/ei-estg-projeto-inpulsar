<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Log;
use App\Post;
use App\Project;
use App\User;
use Illuminate\Http\Request;

class LogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index()
    {
        //
    }*/

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function showUserLog(User $user)
    {
        $logs = $user->getUserLogs();
        return view('logs.show', compact(['logs', 'user']));
    }

    /**
     * Display the specified resource.
     *
     * @param  Project  $project
     * @return \Illuminate\Http\Response
     */
    public function showProjectLog(Project  $project)
    {
        $logs = $project->getProjectLogs();
        return view('logs.show', compact(['logs', 'project']));
    }

    /**
     * Display the specified resource.
     *
     * @param  Project  $project, Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function showActivityLog(Project $project, Activity $activity)
    {
        $logs = $activity->getActivityLogs();
        return view('logs.show', compact(['logs', 'project', 'activity']));
    }

    /**
     * Display the specified resource.
     *
     * @param  Project  $project, Activity  $activity, Post  $post
     * @return \Illuminate\Http\Response
     */
    public function showPostLog(Project $project, Activity $activity, Post $post)
    {
        $logs = $post->getPostLogs();
        return view('logs.show', compact(['logs', 'project', 'activity', 'post']));
    }


    public function publishFacebookLog(Request $request){

        if(str_contains($request->url, 'posts')){
            $splitName = explode('/', $request->url);
            $post_id = $splitName[(count($splitName)-1)];
            $post = Post::find($post_id);
            $activity = Activity::find($post->activity_id);
            $project = Project::find($activity->project_id);

            //Adicionar ao Log -> Flag 38 -> partilha para o Facebook de registo
            $log = new Log;
            $args = collect([]);
            $args->put('project', $project);
            $args->put('activity', $activity);
            $args->put('post', $post);
            $args->put('action', 'post_facebook_publish');
            $log->store(38, $args);

        }elseif(str_contains($request->url, 'activities')){
            $splitName = explode('/', $request->url);
            $activity_id = $splitName[(count($splitName)-1)];
            $activity = Activity::find($activity_id);
            $project = Project::find($activity->project_id);

            //Adicionar ao Log -> Flag 38 -> partilha para o Facebook de actividade
            $log = new Log;
            $args = collect([]);
            $args->put('project', $project);
            $args->put('activity', $activity);
            $args->put('action', 'activity_facebook_publish');
            $log->store(38, $args);

        }else{
            $splitName = explode('/', $request->url);
            $project_id = $splitName[(count($splitName)-1)];
            $project = Project::find($project_id);

            //Adicionar ao Log -> Flag 38 -> partilha para o Facebook de projecto
            $log = new Log;
            $args = collect([]);
            $args->put('project', $project);
            $args->put('action', 'project_facebook_publish');
            $log->store(38, $args);
        }

    }

}
