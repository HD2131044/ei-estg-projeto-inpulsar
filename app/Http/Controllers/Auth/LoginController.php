<?php

namespace App\Http\Controllers\Auth;

use App\ActivationService;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $activationService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ActivationService $activationService)
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->activationService = $activationService;
    }

    public function logout()
    {
        if(\session()->has('error.message')){
            $message = \session('error.message');
            Auth::logout();
            return redirect('/')->with(['error.message' => $message]);
        }

        if(\session()->has('success.message')){
            $message = \session('success.message');
            Auth::logout();
            return redirect('/')->with(['success.message' => $message]);
        }

        Auth::logout();
        return redirect('/');
    }

    public function activateUser($token)
    {
        if ($user = $this->activationService->activateUser($token)) {
            auth()->login($user);
            return redirect($this->redirectPath());
        }

        if(Auth::user()){
            return redirect()->route('home');
        }

        $message = ['error.message' => 'A sua conta já se encontra activa. Proceda ao login utilizando as suas credenciais de acesso.']; //se passar até aqui nunca mostra na vista pq o if a seguir é sempre capturado

        if(!$this->isTokenStillActive($token)){
            $message = ['error.message' => 'O token para o link de activação encontra-se inválido. Caso já tenha activado a sua conta proceda ao login utilizando as suas credenciais de acesso. Se continuar sem conseguir aceder à intranet contacte um "Gestor de conta" e solicite novo link para activação da sua conta.'];
        }

        return redirect()->route('logout')->with($message);

    }

    private function isTokenStillActive($token){

        $checkTokenValid = null;
        $checkTokenValid = DB::table('user_activations')->where('token', $token)->first();

        if($checkTokenValid){
            return true;
        }

        return false;
    }

}
