<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Project;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'blocked']);
    }

    public function getData(){
        $arrayData = [];
        $title = '';
        if (Auth::user()->isAdmin()){
            //get all users
            $arrayData = User::all();
            $title = 'Lista de Utilizadores';
        } elseif (Auth::user()->isDirector()){
            //get all projects
            $arrayData = Project::all();
            $title = 'Lista de Projectos';
        } elseif (Auth::user()->isManager()){
            //get my Projects
            $arrayData = Auth::user()->projects()->get();
            $title = 'Os Meus Projectos';
        } else{
            //get my Activities
            $arrayData = Auth::user()->activities()->get();
            $title = 'As Minhas Actividades';
        }
        return view('home', compact(['arrayData', 'title']));
    }

}
