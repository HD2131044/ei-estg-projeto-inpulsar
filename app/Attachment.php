<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attachment extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Um attachment pretence a um user. [One To Many - Inverse]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Um attachment pretence a um project. [One To Many - Inverse]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Um attachment pretence a uma activity. [One To Many - Inverse]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    /**
     * Um attachment pretence a um post. [One To Many - Inverse]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * Um attachment pode ter vários logs. [One To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    //----------------------------------------------------------------

    public function attachmentOwnerName()
    {
        $owner = User::withTrashed()->find($this->user_id);
        if($owner){
            return $owner->name;
        }else{
            return "Utilizador apagado";
        }
    }

    public function belongsToUser(User $user){
        return $this->user_id == $user->id;
    }

    public function isClosed(){
        if($this->activity_id == null){
            return Project::find($this->project_id)->isClosed();
        }else {
            return Activity::find($this->activity_id)->isClosed();
        }
    }

    public function isActive(){
        if(!$this->post_id){
            //attachment da actividade ou project
            return true;
        }
        //attachment de post (necessita pesquisar dentro dos posts sofdeleted->withTrashed() por causa do log)
        return Post::withTrashed()->where('id', '=', $this->post_id)->first()->isActive();
    }

    public function activeToStr(){
        if($this->isActive()){
            return 'Activo';
        }
        return 'Inactivo';
    }

    public function uploadedWith(){
        if($this->post_id){
            return "Registo: #id-".$this->post_id;
        }elseif($this->activity_id){
            return "Actividade: #id-".$this->activity_id;
        }else{
            return "Projecto: #id-".$this->project_id;
        }
    }

}
