<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DateTime;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Project extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Um project pode ter várias activities. [One To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    /**
     * Um project pode ter vários attachments. [One To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    /**
     * Um project pode ter vários logs. [One To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    /**
     * Users associados ao project. [Many To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_project');
    }

    //----------------------------------------------------------------

    public function isPending()
    {
        return $this->status == 0;
    }

    public function isRunning()
    {
        return $this->status == 1;
    }

    public function isClosed()
    {
        return $this->status == 2;
    }

    public function isCancelled()
    {
        return $this->status == 3;
    }

    public function isDateEnded()
    {
        if($this->end_date){
            $endDate = new DateTime($this->end_date);
            $now = new DateTime('today');
            return $endDate->diff($now)->days > 0 ? "OK": "KO";
        }
        return "Insira Data Fim";
    }

    public function projectCreatorName()
    {
        $creator = User::withTrashed()->find($this->creator_id);
        if($creator){
            return $creator->name;
        }else{
            return "Utilizador apagado";
        }

    }

    public function getAllUsers(){
        return $this->users()
            ->orderBy('role', 'asc')
            ->paginate(10, ['*'], 'users');
    }

    public function getManagers(){
        return $this->users()->get()->where('role', '<', '3');
    }

    public function getManagersCount(){
        return $this->users()->where('role', '<', '3')->count();
    }

    public function getUsers(){
        return $this->users()->get()->where('role', '=', '3');
    }

    public function getUsersCount(){
        return $this->users()->where('role', '=', '3')->count();
    }

    public function containsUser(User $user){
        return $this->users()->get()->contains($user);
    }

    public function getManagersTextToTable(){
        if($this->getManagersCount() > 1){
            return 'Tem '.$this->getManagersCount().' coordenadores associados';
        }elseif ($this->getManagersCount() == 1){
            $manager = $this->getManagers()->first();
            return $manager->name;
        }else {
            return "Sem coordenador(es) associado(s)";
        }
    }

    public function statusToStr()
    {
        switch ($this->status)
        {
            case 0:
                return 'Pendente';
            case 1:
                return 'A Decorrer';
            case 2:
                return 'Concluído';
            case 3:
                return 'Cancelado';
        }
        return '';
    }


    public function getActivities()
    {
        return $this->activities()->paginate(10, ['*'], 'activities');
    }

    public function getNumberActivities()
    {
        return $this->activities()->count();
    }

    public function getPosts()
    {
        $projectPosts_ids = collect([]);
        if($this->activities()->count()){
            foreach($this->activities()->get() as $activity){
                if($activity->getNumberPosts()){
                    foreach($activity->posts()->get() as $post){
                        $projectPosts_ids->push($post->id);
                    }
                }
            }
        }
        if($projectPosts_ids){
            $projectPosts = Post::whereIn('id', $projectPosts_ids)
                ->orderBy('id', 'asc')
                ->paginate(10, ['*'], 'posts');

            return $projectPosts;
        }
        return null;
    }

    public function getNumberPosts()
    {
        $numberPost = 0;
        if($this->activities()->count()){
            foreach($this->activities()->get() as $activity){
                $numberPost += $activity->getNumberPosts();
            }
        }
        return $numberPost;
    }

    public function getAttachments()
    {
        return $this->attachments()->paginate(10, ['*'], 'attachments');
    }

    public function getNumberAttachments()
    {
        return $this->attachments()->count();
    }

    public function getActiveAttachments()
    {
        $projectActiveAttachments_ids = collect([]);
        if($this->attachments()->count()){
            foreach($this->attachments()->get() as $attachment){
                if($attachment->isActive()){
                    $projectActiveAttachments_ids->push($attachment->id);
                }
            }
        }
        if($projectActiveAttachments_ids){
            $projectActiveAttachments = Attachment::whereIn('id', $projectActiveAttachments_ids);
            return $projectActiveAttachments;
        }
        return null;
    }

    public function getNumberActiveAttachments()
    {
        $count = 0;
        if($this->getNumberAttachments()){
            foreach($this->attachments()->get() as $attachment){
                if($attachment->isActive()){
                    $count ++;
                }
            }
        }
        return $count;
    }

    public function getActiveImages()
    {
        $projectActiveImages_ids = collect([]);
        if($this->getNumberActiveAttachments()){
            foreach($this->getActiveAttachments()->get() as $attachment){
                if(str_contains($attachment->mime_type, 'image')){
                    $projectActiveImages_ids->push($attachment->id);
                }
            }
        }
        if($projectActiveImages_ids){
            $projectActiveImages = Attachment::whereIn('id', $projectActiveImages_ids);
            return $projectActiveImages;
        }
        return null;
    }

    public function getNumberActiveImages()
    {
        return $this->getActiveImages()->count();
    }


    public function closeAllActivities(){
        if($this->activities()){
            $projecActivities_ids = collect([]);
            foreach ($this->activities()->get() as $activity){
                $activity->last_status = $activity->status;
                $activity->status = 2;
                if($activity->save()){
                    $projecActivities_ids->push($activity->id);

                    //Adicionar ao Log -> Flag 22 -> concluir automáticamente actividade
                    $log = new Log;
                    $args = collect([]);
                    $args->put('project', $this);
                    $args->put('activity', $activity);
                    $log->store(22, $args);

                }else {
                    if($this->revertCloseAllActivities($projecActivities_ids)){
                        return false;
                    };
                }
            }
        }
        return true;
    }

    public function revertCloseAllActivities($projecActivities_ids = null){
        $flag_revert_da_edicao = false;
        if($projecActivities_ids == null){
            $flag_revert_da_edicao = true;
        }

        DB::beginTransaction();
        try{
            if(!$projecActivities_ids){
                $projecActivities_ids = $this->getProjectActivitiesIds();
            }
            if($projecActivities_ids){
                $count = $projecActivities_ids->count();
                while ($count > 0){
                    $activity_id = $projecActivities_ids->pop();
                    $activity = Activity::find($activity_id);
                    if($activity->last_status != null || $activity->last_status == 0){
                        $activity->status = $activity->last_status;
                        $activity->last_status = null;
                        $activity->save();

                        //Adicionar ao Log -> Flag 23/24 -> reverte estado automático da actividade
                        $log = new Log;
                        $args = collect([]);
                        $args->put('project', $this);
                        $args->put('activity', $activity);
                        if($flag_revert_da_edicao){
                            $log->store(24, $args); //da edição de estado concluído para pendente
                        }else{
                            $log->store(23, $args); //de erro no fecho automático
                        }

                    }
                    $count --;
                }
            }
        }catch(\Exception $e){
            DB::rollback();
            return false;
        }
        DB::commit();
        return true;
    }

    public function getProjectActivitiesIds(){
        if($this->activities()->count()){
            $projecActivities_ids = collect([]);
            foreach ($this->activities()->get() as $activity){
                $projecActivities_ids->push($activity->id);
            }
            return $projecActivities_ids;
        }
        return null;
    }

    public function getProjectLogs()
    {
        //return $this->logs()->withTrashed()->orderBy('id', 'desc')->paginate(10, ['*'], 'project_logs');
        //para datatables
        return $this->logs()->withTrashed()->orderBy('id', 'desc')->get();
    }

    public function isSubmitted(){
        return $this->submitted;
    }

    public function submittedToStr(){
        if($this->submitted ){
            return "Submetida";
        }else{
            return "Não submetida";
        }
    }

    public function getFoto(){
        if($this->project_photo){
            $path = $this->project_photo;
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            return $base64;
        } /*else {
            //sem upload da photo (sem profile_photo)
            $path = storage_path('app')."/public/photos/project_photoNotAvailable.jpg";
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            return $base64;
        }*/
        return "";

    }

    public function canBePostedInFacebook(){
        if($this->isClosed() || $this->isRunning()){
            return true;
        } else {
            return false;
        }
    }

}
