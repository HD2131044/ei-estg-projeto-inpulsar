<?php

namespace App;

use App\Notifications\ActivationLink;

class ActivationService
{

    protected $activationRepo;

    public function __construct(ActivationRepository $activationRepo)
    {
        $this->activationRepo = $activationRepo;
    }

    public function sendActivationMail($user, $random_password)
    {

        if (!$user->isActivated()) { //paranoia check -> apenas se utilizador não está ainda com conta activada

            $token = $this->activationRepo->createActivation($user);

            $link = route('user.activate', $token);

            $user->notify(new ActivationLink($user, $link, $random_password));

            return true;
        }

        return false;

    }

    public function activateUser($token)
    {
        $activation = $this->activationRepo->getActivationByToken($token);

        if ($activation === null) {
            return null;
        }

        $user = User::find($activation->user_id);

        $user->activated = true;

        $user->save();

        $this->activationRepo->deleteActivation($token);

        return $user;

    }

}

