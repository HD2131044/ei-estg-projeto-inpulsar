<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        //ModelNotFoundException
        if ($exception instanceof ModelNotFoundException)
        {
            //dashboard users -> manipular objectos do tipo user => retorna a home
            if($request->user){
                $message = ['error.message' => 'Utilizador já tinha sido removido da aplicação.'];
                return redirect()->route('home')->with($message);
            }elseif($request->attachment){ //dashboard utilizadores/managers -> manipular objectos do tipo attachments
                $message = ['error.message' => 'Anexo já tinha sido removido da aplicação.'];
                return redirect()->back()->with($message);
            }elseif($request->post){ //dashboard utilizadores/managers -> manipular objectos do tipo posts
                $message = ['error.message' => 'Registo já tinha sido removido da aplicação.'];
                if(str_contains($request->session()->get('_previous')['url'], 'activities')){//retorna a actvity.show
                    $activity = $request->activity;
                    $project = $request->project;
                    return redirect()->route('activity.show', compact(['project', 'activity']))->with($message);
                }else{//retorna a project.show (neste caso só managers)
                    $project = $request->project;
                    return redirect()->route('project.show', compact('project'))->with($message);
                }
            }elseif($request->activity){ //dashboard utilizadores/managers -> manipular objectos do tipo activities
                $message = ['error.message' => 'Actividade já tinha sido removida da aplicação.'];
                if(Auth::user()->isManager()){
                    $project = $request->project;
                    return redirect()->route('project.show', compact('project'))->with($message);
                }else{
                    return redirect()->route('home')->with($message);
                }
            }else{ //dashboard utilizadores/managers -> manipular objectos do tipo projects
                $message = ['error.message' => 'Projecto já tinha sido removido da aplicação.'];
                return redirect()->route('home')->with($message);
            }
        }

        //Outras excepções
        //if (!($exception instanceof AuthenticationException) && !($exception instanceof ValidationException)) {
            /*return response()->view('errors.custom', [], 500);*/
            //return response()->view('errors.custom');
        //}

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
