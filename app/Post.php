<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Um post pretence a um user. [One To Many - Inverse]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Um post pretence a uma activity. [One To Many - Inverse]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    /**
     * Um post pode ter vários attachments. [One To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    /**
     * Um post pode ter vários logs. [One To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    //----------------------------------------------------------------

    public function postOwnerName()
    {
        $owner = User::withTrashed()->find($this->user_id);
        if($owner){
            return $owner->name;
        }else{
            return "Utilizador apagado";
        }
    }

    public function isActive(){
        return $this->active;
    }

    public function belongsToUser(User $user){
        return $this->user_id == $user->id;
    }

    public function getAttachments()
    {
        return $this->attachments()->paginate(10, ['*'], 'attachments');
    }

    public function getNumberAttachments()
    {
        return $this->attachments()->count();
    }

    public function getActiveAttachments()
    {
        $projectActiveAttachments_ids = collect([]);
        if($this->attachments()->count()){
            foreach($this->attachments()->get() as $attachment){
                if($attachment->isActive()){
                    $projectActiveAttachments_ids->push($attachment->id);
                }
            }
        }
        if($projectActiveAttachments_ids){
            $projectActiveAttachments = Attachment::whereIn('id', $projectActiveAttachments_ids);
            return $projectActiveAttachments;
        }
        return null;
    }

    public function getNumberActiveAttachments()
    {
        $count = 0;
        if($this->getNumberAttachments()){
            foreach($this->attachments()->get() as $attachment){
                if($attachment->isActive()){
                    $count ++;
                }
            }
        }
        return $count;
    }

    public function getActiveImages()
    {
        $postActiveImages_ids = collect([]);
        if($this->getNumberActiveAttachments()){
            foreach($this->getActiveAttachments()->get() as $attachment){
                if(str_contains($attachment->mime_type, 'image')){
                    $postActiveImages_ids->push($attachment->id);
                }
            }
        }
        if($postActiveImages_ids){
            $postActiveImages = Attachment::whereIn('id', $postActiveImages_ids);
            return $postActiveImages;
        }
        return null;
    }

    public function getNumberActiveImages()
    {
        return $this->getActiveImages()->count();
    }

    public function activeToStr()
    {
        switch ($this->active)
        {
            case 0:
                return 'Inactivo';
            case 1:
                return 'Activo';
        }
        return '';
    }

    public function getPostLogs()
    {
        //return $this->logs()->withTrashed()->orderBy('id', 'desc')->paginate(10, ['*'], 'post_logs');
        //para datatables
        return $this->logs()->withTrashed()->orderBy('id', 'desc')->get();
    }

    public function canBePostedInFacebook(){
        if($this->isActive()){
            if($this->activity->isClosed() || $this->activity->isRunning()){
                if($this->activity->project->isClosed() || $this->activity->project->isRunning()){
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
