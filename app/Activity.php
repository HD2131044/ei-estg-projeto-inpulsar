<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Uma activity pretence a um project. [One To Many - Inverse]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Uma activity pode ter vários attachments. [One To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    /**
     * Uma activity pode ter vários logs. [One To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    /**
     * Uma activity pode ter vários posts. [One To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * Users associados à activity. [Many To Many]
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_activity');
    }

    //----------------------------------------------------------------

    public function isPending()
    {
        return $this->status == 0;
    }

    public function isRunning()
    {
        return $this->status == 1;
    }

    public function isClosed()
    {
        return $this->status == 2;
    }

    public function activityCreatorName()
    {
        $creator = User::withTrashed()->find($this->creator_id);
        if($creator){
            return $creator->name;
        }else{
            return "Utilizador apagado";
        }
    }

    public function getAllUsers(){
        return $this->users()
            ->orderBy('role', 'asc')
            ->paginate(10, ['*'], 'users');
    }

    public function getManagers(){
        return $this->users()->get()->where('role', '<', '3');
    }

    public function getManagersCount(){
        return $this->users()->where('role', '<', '3')->count();
    }

    public function getUsers(){
        return $this->users()->get()->where('role', '=', '3');
    }

    public function getUsersCount(){
        return $this->users()->where('role', '=', '3')->count();
    }

    public function containsUser(User $user){
        return $this->users()->get()->contains($user);
    }

    public function getUsersTextToTable(){
        if($this->getUsersCount() > 1){
            return 'Tem '.$this->getUsersCount().' técnicos associados';
        } elseif($this->getUsersCount() == 1){
            $user = $this->getUsers()->first();
            return $user->name;
        } else {
            return "Sem técnico(s) associado(s)";
        }
    }

    public function statusToStr()
    {
        switch ($this->status)
        {
            case 0:
                return 'Pendente';
            case 1:
                return 'A Decorrer';
            case 2:
                return 'Concluída';
        }
        return '';
    }

    public function getPosts()
    {
        return $this->posts()->paginate(10, ['*'], 'posts');
    }

    public function getNumberPosts()
    {
        return $this->posts()->count();
    }

    public function getNumberActivePosts()
    {
        return $this->posts()->where('active', '=', '1')->count();
    }

    public function getAttachments()
    {
        return $this->attachments()->paginate(10, ['*'], 'attachments');
    }

    public function getNumberAttachments()
    {
        return $this->attachments()->count();
    }

    public function getActiveAttachments()
    {
        $projectActiveAttachments_ids = collect([]);
        if($this->attachments()->count()){
            foreach($this->attachments()->get() as $attachment){
                if($attachment->isActive()){
                    $projectActiveAttachments_ids->push($attachment->id);
                }
            }
        }
        if($projectActiveAttachments_ids){
            $projectActiveAttachments = Attachment::whereIn('id', $projectActiveAttachments_ids);
            return $projectActiveAttachments;
        }
        return null;
    }

    public function getNumberActiveAttachments()
    {
        $count = 0;
        if($this->getNumberAttachments()){
            foreach($this->attachments()->get() as $attachment){
                if($attachment->isActive()){
                    $count ++;
                }
            }
        }
        return $count;
    }

    public function getActiveImages()
    {
        $activityActiveImages_ids = collect([]);
        if($this->getNumberActiveAttachments()){
            foreach($this->getActiveAttachments()->get() as $attachment){
                if(str_contains($attachment->mime_type, 'image')){
                    $activityActiveImages_ids->push($attachment->id);
                }
            }
        }
        if($activityActiveImages_ids){
            $activityActiveImages = Attachment::whereIn('id', $activityActiveImages_ids);
            return $activityActiveImages;
        }
        return null;
    }

    public function getNumberActiveImages()
    {
        return $this->getActiveImages()->count();
    }

    public function getNumberAttachmentsFromActivityAndActivePosts(){
        $count = 0;
        if($this->getNumberAttachments()){
            foreach($this->attachments()->get() as $attachment){
                if($attachment->isActive()){
                    $count ++;
                }
            }
        }
        return $count;
    }

    public function getActivityLogs()
    {
        //return $this->logs()->withTrashed()->orderBy('id', 'desc')->paginate(10, ['*'], 'activity_logs');
        //para datatables
        return $this->logs()->withTrashed()->orderBy('id', 'desc')->get();
    }

    public function getFoto(){
        if($this->activity_photo){
            $path = $this->activity_photo;
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            return $base64;
        } /*else {
            //sem upload da photo (sem profile_photo)
            $path = storage_path('app')."/public/photos/project_photoNotAvailable.jpg";
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            return $base64;
        }*/
        return "";

    }

    public function canBePostedInFacebook(){
        if($this->isClosed() || $this->isRunning()){
            if($this->project->isClosed() || $this->project->isRunning()){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
