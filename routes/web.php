<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'web'], function () {

    Route::get('/', function () {
        if(session()->has('error.message')){
            $message = session()->get('error.message');
            return redirect()->route('login')->with(['error.message' => $message]);
        }
        if(session()->has('success.message')){
            $message = session()->get('success.message');
            return redirect()->route('login')->with(['success.message' => $message]);
        }
        return redirect()->route('login');
    });

    Auth::routes();

    Route::get('/logout', 'Auth\LoginController@logout');

    Route::get('user/activation/{token}', [
        'uses' => 'Auth\LoginController@activateUser',
        'as' =>  'user.activate',
    ]);

    // Rotas utilizadores autenticados e não bloqueados
    Route::group(['middleware'=> ['auth', 'blocked']], function() {

        Route::get('/home', [
            'uses' => 'HomeController@getData',
            'as' => 'home',
        ]);

        Route::get('/show-profile', [
            'uses' => 'UsersController@showProfile',
            'as' => 'user.show-profile',
        ]);

        Route::get('/edit-profile', [
            'uses' => 'UsersController@editProfile',
            'as' => 'user.edit-profile',
        ]);

        Route::patch('/edit-profile', [
            'uses' => 'UsersController@updateProfile',
            'as' => 'user.update-profile',
        ]);

        // Rotas utilizadores do tipo administrador
        Route::group(['middleware' => 'admin'], function () {

            Route::get('/users/create', 'UsersController@create');

            Route::post('/users', 'UsersController@store');

            Route::get('/users/{user}', [
                'uses' => 'UsersController@show',
                'as' => 'user.show',
            ]);

            Route::get('/users/{user}/logs', 'LogsController@showUserLog');

            // Rotas utilizadores do tipo administrador que apenas podem alterar dados de utilizadores não "Administradores"
            Route::group(['middleware' => 'user-can-edit-users'], function () {

                Route::get('/users/{user}/edit', 'UsersController@edit');

                Route::patch('/users/{user}', 'UsersController@update');

                Route::delete('/users/{user}', 'UsersController@destroy');

                Route::patch('/users/{user}/block', [
                    'uses' => 'UsersController@changeUserToBlockedState',
                    'as' => 'admin.blockUser',
                ]);

                Route::patch('/users/{user}/unblock', [
                    'uses' => 'UsersController@changeUserToUnblockedState',
                    'as' => 'admin.unblockUser',
                ]);

                Route::post('/users/{user}/reset-password', [
                    'uses' => 'UsersController@adminPasswordReset',
                    'as' => 'admin.resetPassword',
                ]);

                Route::post('/users/{user}/activation-email', [
                    'uses' => 'UsersController@adminActivationEmail',
                    'as' => 'admin.activationEmail',
                ]);

            }); //user-can-edit-users

        }); //admin

        // Rotas utilizadores do tipo diretor - ANULADO MANAGER DO PROJECTO PODE FAZER O MESMO QUE O DIRETOR
/*        Route::group(['middleware' => 'director'], function () {

            Route::get('/projects/create', 'ProjectsController@create');

            Route::post('/projects', 'ProjectsController@store');

            Route::get('/projects/fetchallmanagers/', 'ProjectsController@getManagers');

            Route::get('/projects/{project}/edit', 'ProjectsController@edit');

            Route::patch('/projects/{project}', 'ProjectsController@update');

            Route::delete('/projects/{project}', 'ProjectsController@destroy');

        }); //diretor*/

        // Rotas utilizadores do tipo diretor e manager que pertence ao projecto
        Route::group(['middleware' => 'manager-belongs-to-project'], function () {

            Route::get('/projects/create', 'ProjectsController@create');

            Route::post('/projects', 'ProjectsController@store');

            Route::get('/projects/fetchallmanagers/', 'ProjectsController@getManagers');

            Route::get('/projects/{project}/edit', 'ProjectsController@edit');

            Route::patch('/projects/{project}', 'ProjectsController@update');

            Route::delete('/projects/{project}', 'ProjectsController@destroy');

            Route::get('/projects/{project}', [
                'uses' => 'ProjectsController@show',
                'as' => 'project.show',
            ]);

            Route::patch('projects/{project}/pending', [
                'uses' => 'ProjectsController@changeProjectToPendingState',
                'as' => 'director.pendingProject',
            ]);

            Route::patch('projects/{project}/running', [
                'uses' => 'ProjectsController@changeProjectToRunningState',
                'as' => 'director.runningProject',
            ]);

            Route::patch('projects/{project}/close', [
                'uses' => 'ProjectsController@changeProjectToClosedState',
                'as' => 'director.closeProject',
            ]);

            Route::patch('projects/{project}/cancel', [
                'uses' => 'ProjectsController@changeProjectToCancelledState',
                'as' => 'director.cancelProject',
            ]);

            Route::patch('projects/{project}/submit', [
                'uses' => 'ProjectsController@submitProject',
                'as' => 'director.submitProject',
            ]);

            Route::get('/projects/{project}/activities/create', 'ActivitiesController@create');

            Route::post('/projects/{project}/activities', 'ActivitiesController@store');

            Route::get('/projects/{project}/activities/{activity}/edit', 'ActivitiesController@edit');

            Route::patch('/projects/{project}/activities/{activity}', 'ActivitiesController@update');

            Route::delete('/projects/{project}/activities/{activity}', 'ActivitiesController@destroy');

            Route::patch('/projects/{project}/activities/{activity}/pending', [
                'uses' => 'ActivitiesController@changeActivityToPendingState',
                'as' => 'manager.pendingActivity',
            ]);

            Route::patch('/projects/{project}/activities/{activity}/running', [
                'uses' => 'ActivitiesController@changeActivityToRunningState',
                'as' => 'manager.runningActivity',
            ]);

            Route::patch('/projects/{project}/activities/{activity}/closed', [
                'uses' => 'ActivitiesController@changeActivityToClosedState',
                'as' => 'manager.closeActivity',
            ]);

            Route::patch('/projects/{project}/activities/{activity}/posts/{post}/activate', [
                'uses' => 'PostsController@changePostToActiveState',
                'as' => 'manager.activatePost',
            ]);

            Route::patch('/projects/{project}/activities/{activity}/posts/{post}/deactivate', [
                'uses' => 'PostsController@changePostToInactiveState',
                'as' => 'manager.deactivatePost',
            ]);

            Route::get('/projects/{project}/logs', 'LogsController@showProjectLog');

            Route::get('/projects/{project}/activities/{activity}/logs', 'LogsController@showActivityLog');

            Route::get('/projects/{project}/activities/{activity}/logs', 'LogsController@showActivityLog');

            //DATATABLES!!!

            Route::get('/projects/fetchmanagers/{project}', 'ProjectsController@fetchManagers');

            Route::get('/projects/fetchallmanagers/{project}', 'ProjectsController@fetchAllManagers');

            Route::get('/projects/fetchtabledata/{project}', 'ProjectsController@fetchTableData');

            Route::get('/projects/{project}/activities/fetchallusers/', 'ActivitiesController@getUsers');

            Route::get('/projects/{project}/activities/{activity}/fetchtabledata/', 'ActivitiesController@fetchTableData');

        }); //manager-belongs-to-project

        // Rotas utilizadores do tipo diretor, manager e utilizador que pertence à actividade
        Route::group(['middleware' => 'user-belongs-to-activity'], function () {

            Route::get('/projects/{project}/activities/{activity}', [
                'uses' => 'ActivitiesController@show',
                'as' => 'activity.show',
            ]);

            Route::get('/projects/{project}/activities/{activity}/posts/create', 'PostsController@create');

            Route::post('/projects/{project}/activities/{activity}/posts', 'PostsController@store');

            Route::get('/projects/{project}/activities/{activity}/show-project-info', 'ProjectsController@showProjectInfo');

        }); //user-belongs-to-activity

        // Rotas utilizadores do tipo diretor, manager e utilizador que pertence à actividade e se o post está activo
        Route::group(['middleware' => 'user-belongs-to-activity-and-post-is-active'], function () {

            Route::get('/projects/{project}/activities/{activity}/posts/{post}', [
                'uses' => 'PostsController@show',
                'as' => 'post.show',
            ]);

        }); //user-belongs-to-activity-and-post-is-active

        //Rotas utilizadores do tipo diretor, manager e utilizador é dono do post e se o post está activo
        Route::group(['middleware' => 'user-owns-post-and-post-is-active'], function () {

            Route::get('/projects/{project}/activities/{activity}/posts/{post}/edit', 'PostsController@edit');

            Route::patch('/projects/{project}/activities/{activity}/posts/{post}', 'PostsController@update');

            Route::delete('/projects/{project}/activities/{activity}/posts/{post}', 'PostsController@destroy');

            Route::get('/projects/{project}/activities/{activity}/posts/{post}/logs', 'LogsController@showPostLog');

            Route::get('/projects/{project}/activities/{activity}/posts/{post}/fetchtabledata/', 'PostsController@fetchTableData');

        }); //user-owns-post-and-post-is-active

        // Rotas utilizadores do tipo diretor, manager e utilizador que pertence à actividade e se o post está activo
        Route::group(['middleware' => 'user-can-download-attachment-and-post-is-active'], function () {

            Route::get('/projects/{project}/attachments/{attachment}/download/', [
                'uses' => 'AttachmentsController@download',
                'as' => 'attachment.download',
            ]);

            Route::get('/projects/{project}/attachments/{attachment}/show/', [
                'uses' => 'AttachmentsController@show',
                'as' => 'attachment.show',
            ]);

            //Route::post('/projects/{project}/attachments/{{attachment}}/getImagePreview', 'AttachmentsController@getImagePreview');

        }); //user-can-download-attachment-and-post-is-active

        //Rotas utilizadores do tipo diretor, manager e utilizador é dono do anexo e verifica se pertence a um post que está activo
        Route::group(['middleware'=> 'user-owns-attachment-and-post-is-active'], function() {

            Route::delete('/projects/{project}/attachments/{attachment}', 'AttachmentsController@destroy');

        }); //user-owns-attachment-and-post-is-active

        //Rotas utilizadores do tipo diretor, manager e utilizador que podem fazer download de todos os anexos activos
        Route::group(['middleware'=> 'user-can-download-all-attachments'], function() {

            Route::get('/projects/{project}/download-all/', [
                'uses' => 'AttachmentsController@downloadAllProjectAttachments',
                'as' => 'attachment.project.download-all',
            ]);

            Route::get('/projects/{project}/activities/{activity}/download-all/', [
                'uses' => 'AttachmentsController@downloadAllActivityAttachments',
                'as' => 'attachment.activity.download-all',
            ]);

            Route::get('/projects/{project}/activities/{activity}/posts/{post}/download-all/', [
                'uses' => 'AttachmentsController@downloadAllPostAttachments',
                'as' => 'attachment.post.download-all',
            ]);

        }); //user-can-download-all-attachments

        Route::post('/attachments/getImagePreview', 'AttachmentsController@getImagePreview');

        Route::post('/registerfblog', 'LogsController@publishFacebookLog');


    }); //auth e blocked

}); //web



