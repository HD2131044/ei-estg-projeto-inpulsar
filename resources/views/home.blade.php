@extends('layouts.app')

@section('content')

    @if (Auth::user()->isAdmin())
        @include('admin.dashboard-admin')
    @elseif(Auth::user()->isDirector())
        @include('director.dashboard-director')
    @elseif(Auth::user()->isManager())
        @include('manager.dashboard-manager')
    @else
        @include('user.dashboard-user')
    @endif


@endsection
