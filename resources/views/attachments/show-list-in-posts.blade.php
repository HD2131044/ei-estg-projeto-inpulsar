<div class="row">
    <div class="col-sm-6"> <h4 class="panel-title">Anexos associados</h4> </div>
    @if($post->getNumberActiveAttachments() > 0)
        <div class="col-sm-3 pull-right">
            <form action="/projects/{{$project->id}}/activities/{{$activity->id}}/posts/{{$post->id}}/download-all/" method="get" class="inline">
                <button id="download-all-post{{$post->id}}" type="submit" class="btn btn-block btn-primary">
                    @if(Auth::user()->isManager())
                        Descarregar todos os anexos activos
                    @else
                        Descarregar todos os anexos
                    @endif
                </button>
            </form>
        </div>
    @endif
</div>

<div class="table-responsive">

    @if($post->getNumberAttachments() > 0)
        <table class="table table-hover">

            <thead>
            <tr>
                <th>Id</th>
                <th>Upload feito por</th>
                <th>Pertence a</th>
                <th>Nome ficheiro</th>
                <th>Data de upload</th>
                <th>Tamanho</th>
                <th>Tipo de ficheiro</th>
                @if(Auth::user()->isManager())
                    <th>Estado do Anexo</th>
                @endif
                @if(!str_contains(URL::current(), 'edit'))
                    <th>Opções</th>
                @endif
            </tr>
            </thead>
            <tbody>

            @foreach ($post->getAttachments() as $attachment)

                <tr>
                    <td>{{ $attachment->id }}</td>
                    <td>{{ $attachment->attachmentOwnerName()}}</td>
                    <td>{{ $attachment->uploadedWith()}}</td>
                    <td>{{ $attachment->original_filename }}</td>
                    <td>{{ $attachment->date }}</td>
                    <td>{{ $attachment->size }}</td>
                    <td>{{ $attachment->mime_type }}</td>
                    @if(Auth::user()->isManager())
                        <td>{{ $attachment->activeToStr() }}</td>
                    @endif
                    @if(!str_contains(URL::current(), 'edit'))
                        <td>
                            <div style="display: inline-block">
                                <span style="display: inline-block">
                                    <form action="/projects/{{$attachment->project_id}}/attachments/{{$attachment->id}}/download/" method="get" class="inline">
                                        <button id="download{{$attachment->id}}" type="submit" class="glyphicon glyphicon-download-alt" style="padding: 0; border: none; background: none;" title="Descarregar"></button>
                                    </form>
                                </span>
                                @if(str_contains($attachment->mime_type, 'image'))
                                    <span style="display: inline-block">
                                            <button id="preview{{$attachment->id}}" type="button" class="glyphicon glyphicon-search" onclick="previewImage({{$attachment->id}})" style="padding: 0; border: none; background: none;" title="Pré-Visualizar"></button>
                                        </span>
                                @endif
                                @if(Auth::user()->isManager() || ($attachment->belongsToUser(Auth::user()) && !$attachment->isClosed()))
                                    <span style="display: inline-block">
                                        <div>
                                            <button type="button" id="apagar{{$attachment->id}}" class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#modal-delete-{{$attachment->id}}" style="padding: 0; border: none; background: none;" title="Apagar"></button>
                                            <div class="modal fade" tabindex="-1" id="modal-delete-{{$attachment->id}}" role="dialog">
                                                <div class="modal-dialog modal-lg">
                                                    {{$action_url = "/projects/$attachment->project_id/attachments/$attachment->id"}}
                                                    @include('modals.delete', array('action_url' => $action_url))
                                                </div>
                                            </div>
                                        </div>
                                    </span>
                                @endif
                            </div>
                        </td>
                    @endif
                </tr>

            @endforeach
        </table>
        <div class="row" align="center">
            {{$post->getAttachments()->appends(Request::except('page'))->links()}}
        </div>
    @else
        <h4>Não foram encontrados anexos</h4>
    @endif

</div>

@include('modals.image-preview')

<script>
    var previewImage = function (imageId) {
        var imageModal = null;
        if(imageId){
            $.ajax({
                type: 'post',
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                url: "/attachments/getImagePreview",
                data: {'imageId' : imageId},
                success: function (result) {
                    imageModal = $('#imagePreviewModal');
                    imageModal.find('#imageName').html(result.fileName);
                    imageModal.find('#imagePreview').attr('src', 'data:image/png;base64,' + result.image);
                    imageModal.modal().toggle();
                },
                error: function (result) {
                    console.log(result);
                }
            });
        }
    }
</script>