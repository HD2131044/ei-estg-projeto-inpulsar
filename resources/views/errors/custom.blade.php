@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">

            <div class="panel-heading">

                <ol class="breadcrumb">
                    <li><a href ="{{ route('home') }}">Home</a></li>
                    <li class="active">Página de erro</li>
                </ol>

            </div>

            <div class="panel-body">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                {{--<div class="well" style="background: #ff0000; text-align: left; color: #000000">
                    <div>
                        <h1>Aconteceu um erro na aplicação da intranet da InPulsar!!!</h1>
                    </div>
                </div>--}}

                    <div class="row">
                        <div class="jumbotron" style="margin-left: 10px; margin-right: 10px">
                            <div class="text-center"><i class="fa fa-5x fa-frown-o" style="color:#d9534f;"></i></div>
                            <h2 class="text-center">{{ $title }}</h2>
                            <br>
                            <p class="text-center">Aconteceu um erro na aplicação da intranet da InPulsar!!!</p>
                            <br>
                            <p class="text-center"><a class="btn btn-primary" href="{{ route('home') }}"><i class="fa fa-home"></i>  Voltar à página inicial</a></p>
                        </div>
                    </div>

                {{--<div>
                    <a class="btn btn-block btn-default " href="{{ route('home') }}">Voltar à página home</a>
                </div>--}}

            </div>
        </div>
    </div>

@endsection
