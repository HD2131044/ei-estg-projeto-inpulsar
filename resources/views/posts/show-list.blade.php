<h4 class="panel-title">Registos associados</h4>

<div class="table-responsive">

    @if((Auth::user()->isManager() && $activity->getNumberPosts() > 0) || $activity->getNumberActivePosts() > 0)
        <table class="table table-hover">

            <thead>
            <tr>
                <th>Id</th>
                <th>Criado Por</th>
                <th>Título</th>
                <th>Data</th>
                <th>Localização</th>
                <th>Nº Participantes</th>
                <th>Nº Anexos</th>
                @if(Auth::user()->isManager())
                    <th>Estado do Registo</th>
                @endif
                <th>Opções</th>
            </tr>
            </thead>
            <tbody>

            @foreach ($activity->getPosts() as $post)

                @if(Auth::user()->isManager() || $post->isActive())

                    <tr>
                        <td>{{ $post->id }}</td>
                        <td>{{ $post->postOwnerName()}}</td>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->date }}</td>
                        <td>{{ $post->location }}</td>
                        <td>{{ $post->number_participants }}</td>
                        <td>{{ $post->getNumberAttachments() }}</td>
                        @if(Auth::user()->isManager())
                            <td>{{ $post->activeToStr() }}</td>
                        @endif
                        <td>

                            <div>
                                @include('posts.options-list')

                                @include('posts.dropdown')
                            </div>
                        </td>
                    </tr>

                @endif

            @endforeach
        </table>
        <div class="row" align="center">
            {{$activity->getPosts()->appends(Request::except('page'))->links()}}
        </div>
    @else
        <h4>Não foram encontrados registos</h4>
    @endif

</div>