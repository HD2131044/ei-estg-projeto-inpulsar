@if(Auth::user()->isManager() || Auth::user()->isPostOwner($post))
    @if($post->getPostLogs()->count())
        <div class="col-md-3">
            <form action="/projects/{{$project->id}}/activities/{{$activity->id}}/posts/{{$post->id}}/logs" method="get" class="inline">
                <button type="submit" class="btn btn-block btn-default">Visualisar Log</button>
            </form>
        </div>
    @endif
    <div class="col-md-3">
        <form action="/projects/{{$project->id}}/activities/{{$activity->id}}/posts/{{$post->id}}/edit" method="get" class="inline">
            <button type="submit" class="btn btn-block btn-default">Editar</button>
        </form>
    </div>
    <div class="col-md-3">
        <button type="button" class="btn btn-block btn-danger" data-toggle="modal" data-target="#modal-delete-{{$post->id}}">Apagar</button>
        <div class="modal fade" tabindex="-1" id="modal-delete-{{$post->id}}" role="dialog">
            <div class="modal-dialog modal-lg">
                {{$action_url = "/projects/$project->id/activities/$activity->id/posts/$post->id"}}
                @include('modals.delete', compact('action_url'))
            </div>
        </div>
    </div>
@endif

@if(Auth::user()->isManager())
    @if($post->isActive())
        <div class="col-md-3">
            <form action="{{ route('manager.deactivatePost', ['project' => $project, 'activity' => $activity, 'post' => $post]) }}" method="post" class="inline">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-danger">Desactivar</button>
                </div>
            </form>
        </div>
    @else
        <div class="col-md-3">
            <form action="{{ route('manager.activatePost', ['project' => $project, 'activity' => $activity, 'post' => $post]) }}" method="post" class="inline">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-success">Activar</button>
                </div>
            </form>
        </div>
    @endif
@endif

<div class="col-md-3">
    @if(str_contains(URL::previous(), 'activities'))
        <a class="btn btn-block btn-default " href="/projects/{{$project->id}}/activities/{{$activity->id}}">Voltar</a>
    @else
        <a class="btn btn-block btn-default " href="/projects/{{$project->id}}">Voltar</a>
    @endif
</div>