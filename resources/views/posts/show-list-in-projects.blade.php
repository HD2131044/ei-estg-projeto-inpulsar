<h4 class="panel-title">Registos associados</h4>

<div class="table-responsive">

    @if($project->getNumberPosts() > 0)
        <table class="table table-hover">

            <thead>
            <tr>
                <th>Id</th>
                <th>Criado Por</th>
                <th>Título</th>
                <th>Data</th>
                <th>Localização</th>
                <th>Nº Participantes</th>
                <th>Nº Anexos</th>
                <th>Estado do Registo</th>
                <th>Opções</th>
            </tr>
            </thead>
            <tbody>

            @foreach ($project->getPosts() as $post)

                <tr>
                    <td>{{ $post->id }}</td>
                    <td>{{ $post->postOwnerName()}}</td>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->date }}</td>
                    <td>{{ $post->location }}</td>
                    <td>{{ $post->number_participants }}</td>
                    <td>{{ $post->getNumberAttachments() }}</td>
                    <td>{{ $post->activeToStr() }}</td>
                    <td>

                        <div>
                            @include('posts.options-list-in-projects')

                            @include('posts.dropdown')
                        </div>
                    </td>
                </tr>

            @endforeach
        </table>
        <div class="row" align="center">
            {{$project->getPosts()->appends(Request::except('page'))->links()}}
        </div>
    @else
        <h4>Não foram encontrados registos</h4>
    @endif

</div>