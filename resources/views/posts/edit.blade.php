@extends('layouts.app')

@section('content')
    @if (count(Session::get('errors')) > 0)
        @include('partials.errors-ajax')
    @endif

    <style>
        .file-drop{
            background-color: white; outline: 3px dashed lightslategrey; box-shadow: gray; width: 100%; height: 100px
        }
        .file-drop-hover{
            background-color: lightpink;
            outline-color: darkred;
        }
    </style>

    <script>
        var currentUser = {{\Auth::user()->id}}
    </script>

    <div class="container" style="width: 85%">
        <div class="panel panel-default">
            <div class="panel-heading">

                <ol class="breadcrumb">
                    <li><a href ="{{ route('home') }}">Home</a></li>
                    @if(Auth::user()->isManager())
                        <li>Projecto: <a href="/projects/{{$project->id}}">{{$project->name}}</a></li>
                    @else
                        <li>Projecto: <a href="/projects/{{$project->id}}/activities/{{$activity->id}}/show-project-info">{{$project->name}}</a></li>
                    @endif
                    <li>Actividade: <a href="/projects/{{$project->id}}/activities/{{$activity->id}}">{{$activity->name}}</a></li>
                    <li>Registo: <a href="/projects/{{$project->id}}/activities/{{$activity->id}}/posts/{{$post->id}}">{{$post->title}}</a></li>
                    <li class="active">Editar detalhes do registo</li>
                </ol>

                <h4 class="panel-title">Editar Detalhes do Registo</h4>
            </div>

            <div class="panel-body">
                <form id="postEdit" action="/projects/{{$project->id}}/activities/{{$activity->id}}/posts/{{$post->id}}" method="post" class="form-group" enctype="multipart/form-data">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="required" for="inputTitle">Título</label>
                        <input
                                type="text" class="form-control"
                                name="título" id="inputTitle"
                                value="{{$post->title}}"/>
                    </div>

                    <div class="form-group">
                        <label for="inputText">Texto</label>
                        <input
                                type="text" class="form-control"
                                name="texto" id="inputText"
                                value="{{$post->text}}"/>
                    </div>

                    <div class="form-group">
                        <label class="required" for="inputDate">Data</label>
                        <input
                                type="date" class="form-control"
                                name="data" id="inputDate"
                                value="{{$post->date}}"/>
                    </div>

                    <div class="form-group">
                        <label for="inputLocation">Localização</label>
                        <input
                                type="text" class="form-control"
                                name="localização" id="inputLocation"
                                value="{{$post->location}}"/>
                    </div>

                    <div class="form-group">
                        <label for="inputPostUrl">Url do registo</label>
                        <input
                                type="text" class="form-control"
                                name="url_registo" id="inputPostUrl"
                                value="{{$post->post_url}}"/>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label class="required" for="inputParticipants">Número de participantes</label>
                        <input
                                type="number" min="0" class="form-control"
                                name="número_de_participantes" id="inputParticipants"
                                value="{{$post->number_participants}}"/>
                    </div>

                    <hr>

                    <div class="form-group col-md-12">
                        <label for="inputAttachments">Adicionar anexos:</label>
                        <div id="fileInput" class="file-drop" style="text-align: center">
                            Clique ou largue aqui os seus ficheiros!
                        </div>
                    </div>

                    <hr>

                    <input type="file" id="inputFile" value="" class="hidden" multiple>
                    <h4 class="panel-title">Anexos associados</h4>

                    <div class="table-responsive">
                        <div class="col-sm-12">
                            <table id="postAttachments" class="display" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Criado a</th>
                                    <th>Inserido por</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="ok">Guardar</button>
                        <a class="btn btn-default" href="/projects/{{$project->id}}/activities/{{$activity->id}}/posts/{{$post->id}}">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>

        var files = [];
        var filesFromServer = [];
        var filesToDelete = [];
        var filesToAdd = [];

        $.get('/projects/{{$project->id}}/activities/{{$activity->id}}/posts/{{$post->id}}/fetchtabledata', function (data) {
            if(data['attachments'].length >0){
                files = data['attachments'];
                filesFromServer = files;
            }

            var postAttachments = $('#postAttachments').DataTable({
                "paging": false,
                "searching": false,
                "responsive": true,
                "iDisplayLength": 10,
                "data": files,
                "columns": [
                    { "data": function(data){if(data.filename){return data.original_filename}else{return data.name}}},
                    { "data": function(data){if(data.created_at){return data.created_at}else{return data.lastModified}}},
                    { "data": function(data){if(data.user_id){return data.user_id}else{return currentUser}}},
                    { "render": function (a, b, c, d) {
                        return "<a id='removeFile' class='btn btn-xs btn-danger'>Remover</a>";
                    }}
                ]
            });

            var fileInput = $('#fileInput');

            fileInput.on('dragenter', function (e) {
                e.preventDefault();
                e.stopPropagation();
                console.log('HEEEYYYYY');
            });
            fileInput.on('dragover', function (e) {
                e.preventDefault();
                e.stopPropagation();
                fileInput.addClass('file-drop-hover');
            });
            fileInput.on('dragleave', function (e) {
                e.preventDefault();
                e.stopPropagation();
                fileInput.removeClass('file-drop-hover');
            });
            fileInput.on('dragend', function (e) {
                e.preventDefault();
                e.stopPropagation();
                fileInput.removeClass('file-drop-hover');
            });
            fileInput.on('drop', function (e) {
                fileInput.removeClass('file-drop-hover');
                if(e.originalEvent.dataTransfer.files.length){
                    e.preventDefault();
                    e.stopPropagation();

                    $.each(e.originalEvent.dataTransfer.files, function(index, file){
                        if(_.findIndex(files, function(obj){if(_.has(obj, 'media_path')){
                                return obj.original_filename == file.name;
                            }
                                return obj.name == file.name;
                            }) == -1)
                        {
                            files.push(file);
                            filesToAdd.push(file);
                        }
                    });
                    postAttachments.clear();
                    $.each(files, function (index, fileToUpload) {
                        postAttachments.row.add(fileToUpload);
                    });
                    postAttachments.draw();
                }
            });

            fileInput.on('click', function () {
                $('#inputFile').click();
            });

            $('#inputFile').on('change', function (e) {
                e.preventDefault();
                var inputFiles = $(this).prop('files');
                if(inputFiles.length){

                    $.each(inputFiles, function(index, file){
                        if(_.findIndex(files, function(obj){if(_.has(obj, 'media_path')){
                                return obj.original_filename == file.name;
                            }
                                return obj.name == file.name;
                            }) == -1)
                        {
                            files.push(file);
                            filesToAdd.push(file);
                        }
                    });
                    postAttachments.clear();
                    $.each(files, function (index, fileToUpload) {
                        postAttachments.row.add(fileToUpload);
                    });
                    postAttachments.draw();
                }
            });

            postAttachments.on('click', '#removeFile', function () {
                var row = postAttachments.row($(this).parents('tr'));
                var file = row.data();
                if(_.has(file, 'media_path')){
                    if(_.findIndex(filesToDelete, function(obj){return obj.filename == file.filename;}) == -1){
                        filesToDelete.push(file);
                    }
                }
                _.remove(files, function (obj){
                    return obj == file;
                });
                postAttachments.clear();
                $.each(files, function (index, fileToUpload) {
                    postAttachments.row.add(fileToUpload);
                });
                postAttachments.draw();
            });
        });

        $('#postEdit').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData (this);
            $.each(filesToAdd, function (index, file) {
                formData.append('anexos['+ file.name +']', file);
            });
            $.each(filesToDelete, function (index, fileRemove) {
                formData.append('anexosARemover['+ fileRemove.filename +']', JSON.stringify(fileRemove));
            });
            $.ajax({
                type: 'post',
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                processData: false,
                contentType: false,
                url: "/projects/{{$project->id}}/activities/{{$activity->id}}/posts/{{$post->id}}",
                data: formData,
                success: function (result) {
                    window.location.replace('/projects/{{$project->id}}/activities/{{$activity->id}}/posts/{{$post->id}}');
                },
                error: function () {
                    window.location.reload();
                }
            });
        });
    </script>

@endsection
