@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">

            <div class="panel-heading">

                <ol class="breadcrumb">
                    <li><a href ="{{ route('home') }}">Home</a></li>
                    @if(Auth::user()->isManager())
                        <li>Projecto: <a href="/projects/{{$project->id}}">{{$project->name}}</a></li>
                    @else
                        <li>Projecto: <a href="/projects/{{$project->id}}/activities/{{$activity->id}}/show-project-info">{{$project->name}}</a></li>
                    @endif
                    <li>Actividade: <a href="/projects/{{$project->id}}/activities/{{$activity->id}}">{{$activity->name}}</a></li>
                    <li class="active">Registo: {{$post->title}}</li>
                </ol>

                <h4 class="panel-title">Detalhes do Registo</h4>
                <div class="col-sm-3 pull-right">

                    @if(Auth::user()->isPublisher() && $post->canBePostedInFacebook())
                        <button type="button" class="btn btn-block btn-success" value="facebook" data-toggle="modal" data-target="#shareModal">
                            Publicar no Facebook</button>
                    @endif

                </div>
            </div>

            <div class="panel-body">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Criado por: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$post->postOwnerName() }}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Título: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$post->title }}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Texto: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$post->text}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Data da publicação: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$post->date}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Localização: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$post->location}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Url do registo: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4><a href="{{$post->post_url}}">{{$post->post_url}}</a></h4>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Número de participantes: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$post->number_participants}}</h4>
                    </div>
                </div>

                <hr>

                @include('attachments.show-list-in-posts')

                <hr>

                @include('gallery.gallery')

                <hr>

                @if(Auth::user()->isManager())
                    <div class="row">
                        <div class="col-md-2">
                            <h4><span class="label label-info">Estado do registo: </span></h4>
                        </div>
                        <div class="col-md-4 pull-left">
                            <h4>{{$post->activeToStr()}}</h4>
                        </div>
                    </div>
                @endif

                <hr>

                <div class="row">

                    @include('posts.options-show')

                </div>

            </div>
        </div>
    </div>

    @include('modals.image-preview')

    @if(Auth::user()->isPublisher() && $post->canBePostedInFacebook())

        @include('modals.share')

        <script>
            $('#shareModal').on('show.bs.modal', function () {
                $('#fbText').val('{{$post->text}}');
            });
        </script>

    @endif

@endsection
