@if(Auth::user()->isManager())
    <div class="btn-group">
        <button type="button" class="btn btn-xs btn-block btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Acções <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" style="right: 4px;left: auto;">
            @if($post->isActive())
                <li>
                    <a href="projects/{{$project->id}}/activities/{{$post->activity->id}}/posts/{{$post->id}}/deactivate"
                       onclick="event.preventDefault();
                               document.getElementById('deactivate-form-{{$post->id}}').submit();">
                            Desactivar
                    </a>

                    <form id="deactivate-form-{{$post->id}}" action="{{ route('manager.deactivatePost', ['project' => $project, 'activity' => $post->activity, 'post' => $post]) }}" method="post">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}
                    </form>
                </li>
            @else
                <li>
                    <a href="projects/{{$project->id}}/activities/{{$post->activity->id}}/posts/{{$post->id}}/activate"
                       onclick="event.preventDefault();
                               document.getElementById('deactivate-form-{{$post->id}}').submit();">
                            Activar
                    </a>

                    <form id="deactivate-form-{{$post->id}}" action="{{ route('manager.activatePost', ['project' => $project, 'activity' => $post->activity, 'post' => $post]) }}" method="post">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}
                    </form>
                </li>
            @endif

        </ul>
    </div>
@endif