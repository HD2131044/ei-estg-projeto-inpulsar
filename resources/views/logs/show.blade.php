@extends('layouts.app')

@section('content')

    <style>
        .dataTable > thead > tr > th[class*="sort"]:after{
            content: "" !important;
        }

        table.dataTable thead > tr > th.sorting_asc,
        table.dataTable thead > tr > th.sorting_desc,
        table.dataTable thead > tr > th.sorting,
        table.dataTable thead > tr > td.sorting_asc,
        table.dataTable thead > tr > td.sorting_desc,
        table.dataTable thead > tr > td.sorting {
            padding-right: inherit;
        }
    </style>

    <div class="container">
        <div class="panel panel-default">
            @if(str_contains(URL::current(), 'users'))
                <!-- Título do utilizador -->
                <div class="panel-heading">

                    <ol class="breadcrumb">
                        <li><a href ="{{ route('home') }}">Home</a></li>
                        @if(Auth::user() == $user)
                            <li><a href="{{ route('user.show-profile') }}">Meu perfil</a></li>
                        @else
                            <li>Utilizador: <a href="/users/{{$user->id}}">{{$user->name}}</a></li>
                        @endif
                        <li class="active">Log do utilizador</li>
                    </ol>

                    <h4 class="panel-title">Log do Utilizador</h4>
                </div>
            @elseif(str_contains(URL::current(), 'posts'))
                <!-- Título de post -->
                    <div class="panel-heading">

                        <ol class="breadcrumb">
                            <li><a href ="{{ route('home') }}">Home</a></li>
                            @if(Auth::user()->isManager())
                                <li>Projecto: <a href="/projects/{{$project->id}}">{{$project->name}}</a></li>
                            @else
                                <li>Projecto: <a href="/projects/{{$project->id}}/activities/{{$activity->id}}/show-project-info">{{$project->name}}</a></li>
                            @endif
                            <li>Actividade: <a href="/projects/{{$project->id}}/activities/{{$activity->id}}">{{$activity->name}}</a></li>
                            <li>Registo: <a href="/projects/{{$project->id}}/activities/{{$activity->id}}/posts/{{$post->id}}">{{$post->title}}</a></li>
                            <li class="active">Log do registo</li>
                        </ol>

                        <h4 class="panel-title">Log do Registo</h4>
                    </div>
            @elseif(str_contains(URL::current(), 'activities'))
                <!-- Título de actividade -->
                <div class="panel-heading">

                    <ol class="breadcrumb">
                        <li><a href ="{{ route('home') }}">Home</a></li>
                        @if(Auth::user()->isManager())
                            <li>Projecto: <a href="/projects/{{$project->id}}">{{$project->name}}</a></li>
                        @else
                            <li>Projecto: <a href="/projects/{{$project->id}}/activities/{{$activity->id}}/show-project-info">{{$project->name}}</a></li>
                        @endif
                        <li>Activity: <a href="/projects/{{$project->id}}/activities/{{$activity->id}}">{{$activity->name}}</a></li>
                        <li class="active">Log da actividade</li>
                    </ol>

                    <h4 class="panel-title">Log da Actividade</h4>
                </div>
            @else
                <!-- Título de projecto -->
                <div class="panel-heading">

                    <ol class="breadcrumb">
                        <li><a href ="{{ route('home') }}">Home</a></li>
                        <li>Projecto: <a href="/projects/{{$project->id}}">{{$project->name}}</a></li>
                        <li class="active">Log do projecto</li>
                    </ol>

                    <h4 class="panel-title">Log do Projecto</h4>
                </div>
            @endif

            <div class="panel-body">

                <div class="table-responsive">
                    @if($logs->count() > 0)
                        <table id="logTable" class="table table-hover">

                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Data</th>
                                <th>Nome do criador</th>
                                <th>Acção</th>
                                <th>Texto</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($logs as $log)

                                <tr>
                                    <td>{{ $log->id }}</td>
                                    <td>{{ $log->date }}</td>
                                    <td>{{ $log->logCreatorName()}}</td>
                                    <td>{{ $log->action }}</td>
                                    <td>{{ $log->entry_text }}</td>
                                </tr>

                            @endforeach
                        </table>

                        <div class="row" align="center">
                            {{--{{$logs->appends(Request::except('page'))->links()}}--}}
                        </div>
                    @else
                        <h4>Não foram encontrados logs</h4>
                    @endif

                </div>

                <hr>

                <div class="row">
                    @if(str_contains(URL::current(), 'users'))
                        <!-- Voltar do utilizador -->
                        <div class="col-md-3">
                            <form action="/users/{{$user->id}}" method="get" class="inline">
                                <button type="submit" class="btn btn-block btn-default">Voltar</button>
                            </form>
                        </div>
                    @elseif(str_contains(URL::current(), 'posts'))
                        <!-- Voltar de post -->
                            <div class="col-md-3">
                                <form action="/projects/{{$project->id}}/activities/{{$activity->id}}/posts/{{$post->id}}" method="get" class="inline">
                                    <button type="submit" class="btn btn-block btn-default">Voltar</button>
                                </form>
                            </div>
                    @elseif(str_contains(URL::current(), 'activities'))
                        <!-- Voltar de actividade -->
                        <div class="col-md-3">
                            <form action="/projects/{{$project->id}}/activities/{{$activity->id}}" method="get" class="inline">
                                <button type="submit" class="btn btn-block btn-default">Voltar</button>
                            </form>
                        </div>
                    @else
                        <!-- Voltar de projecto -->
                        <div class="col-md-3">
                            <form action="/projects/{{$project->id}}" method="get" class="inline">
                                <button type="submit" class="btn btn-block btn-default">Voltar</button>
                            </form>
                        </div>
                    @endif

                </div>

            </div>
        </div>
    </div>

    <script>
        $('#logTable').DataTable({
            responsive: true,
            paging: true,
            ordering: true,
            iDisplayLength: 10
        });
    </script>

@endsection