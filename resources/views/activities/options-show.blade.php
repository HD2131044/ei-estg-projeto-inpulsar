@if(Auth::user()->isManager())
    @if($activity->getActivityLogs()->count())
        <div class="col-md-3">
            <form action="/projects/{{$project->id}}/activities/{{$activity->id}}/logs" method="get" class="inline">
                <button type="submit" class="btn btn-block btn-default">Visualisar Log</button>
            </form>
        </div>
    @endif

    <div class="col-md-3">
        <form action="/projects/{{$project->id}}/activities/{{$activity->id}}/edit" method="get" class="inline">
            <button type="submit" class="btn btn-block btn-default">Editar</button>
        </form>
    </div>
    <div class="col-md-3">
        <button type="button" class="btn btn-block btn-danger" data-toggle="modal" data-target="#modal-delete-{{$activity->id}}">Apagar</button>
        <div class="modal fade" tabindex="-1" id="modal-delete-{{$activity->id}}" role="dialog">
            <div class="modal-dialog modal-lg">
                {{$action_url = "/projects/$project->id/activities/$activity->id"}}
                @include('modals.delete', compact('action_url'))
            </div>
        </div>
    </div>

    @if(!$activity->isClosed())
        @if($activity->isPending())
            <div class="col-md-3">
                <button type="button" class="btn btn-block btn-warning" data-toggle="modal" data-target="#modal-running-{{$activity->id}}">Decorrer</button>
                <div class="modal fade" tabindex="-1" id="modal-running-{{$activity->id}}"> role="dialog">
                    <div class="modal-dialog modal-lg">
                        {{$action_button = "decorrer"}}
                        @include('modals.state-activity', compact('action_button'))
                    </div>
                </div>
            </div>
        @elseif($activity->isRunning())
            <div class="col-md-3">
                <button type="button" class="btn btn-block btn-warning" data-toggle="modal" data-target="#modal-close-{{$activity->id}}">Concluir</button>
                <div class="modal fade" tabindex="-1" id="modal-close-{{$activity->id}}"> role="dialog">
                    <div class="modal-dialog modal-lg">
                        {{$action_button = "concluir"}}
                        @include('modals.state-activity', compact('action_button'))
                    </div>
                </div>
            </div>
        @endif
    @endif
@endif

<div class="col-md-3">
    @if(Auth::user()->isManager())
        <a class="btn btn-block btn-default " href="/projects/{{$project->id}}">Voltar</a>
    @else
        <a class="btn btn-block btn-default " href="{{ route('home') }}">Voltar</a>
    @endif
</div>
