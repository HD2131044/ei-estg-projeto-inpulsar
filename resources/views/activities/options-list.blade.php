<div class="btn-group">
    <form action="/projects/{{$activity->project_id}}/activities/{{$activity->id}}" method="get" class="inline">
        <button id="detalhes{{$activity->id}}" type="submit" class="btn btn-xs btn-block btn-default">Detalhes</button>
    </form>
</div>
@if(!Auth::user()->isManager())
    <div>
        <form action="/projects/{{$activity->project_id}}/activities/{{$activity->id}}/show-project-info" method="get" class="inline">
            <button id="info-projecto{{$activity->id}}" type="submit" class="btn btn-xs btn-block btn-default">Info Projecto</button>
        </form>
    </div>
@endif

