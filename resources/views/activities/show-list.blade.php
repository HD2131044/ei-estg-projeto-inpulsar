<h4 class="panel-title">Actividades associadas</h4>

<div class="table-responsive">
    @if($project->getNumberActivities() > 0)
        <table class="table table-hover">

            <thead>
            <tr>
                <th>Id</th>
                <th>Criada Por</th>
                <th>Nome</th>
                <th>Data de Ínicio</th>
                <th>Data de Fim</th>
                <th>Estado da Actividade</th>
                <th>Técnico(s)</th>
                <th>Nº Utilizadores</th>
                <th>Nº Registos</th>
                <th>Nº Anexos</th>
                <th>Opções</th>
            </tr>
            </thead>
            <tbody>

            @foreach ($project->getActivities() as $activity)

                <tr>
                    <td>{{ $activity->id }}</td>
                    <td>{{ $activity->activityCreatorName()}}</td>
                    <td>{{ $activity->name }}</td>
                    <td>{{ $activity->start_date }}</td>
                    <td>{{ $activity->end_date }}</td>
                    <td>{{ $activity->statusToStr() }}</td>
                    <td>{{ $activity->getUsersTextToTable() }}</td>
                    <td>{{ $activity->users()->count() }}</td>
                    <td>{{ $activity->getNumberPosts() }}</td>
                    <td>{{ $activity->getNumberAttachments() }}</td>
                    <td>

                        <div>
                            @include('activities.options-list')

                            @include('activities.dropdown')
                        </div>
                    </td>
                </tr>

            @endforeach
        </table>
        <div class="row" align="center">
            {{$project->getActivities()->appends(Request::except('page'))->links()}}
        </div>
    @else
        <h4>Não foram encontradas actividades</h4>
    @endif

</div>
