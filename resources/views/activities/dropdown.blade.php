@if(Auth::user()->isManager())
    @if(!$activity->isClosed())
        <div class="btn-group">
            <button id="toggleState{{$activity->id}}" type="button" class="btn btn-xs btn-block btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Estados <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu" style="right: 4px;left: auto;">
                @if($activity->isPending())
                    <li>
                        <a id="btn-running-{{$activity->id}}" style="cursor: pointer;">Decorrer</a>
                    </li>
                @elseif($activity->isRunning())
                    <li>
                        <a id="btn-close-{{$activity->id}}" style="cursor: pointer;">Concluir</a>
                    </li>
                @endif
            </ul>
        </div>
    @endif
@endif

<!-- Modal -->
<div class="modal fade" tabindex="-1" id="modal-running-{{$activity->id}}"> role="dialog">
    <div class="modal-dialog modal-lg">
        {{$action_button = "decorrer"}}
        @include('modals.state-activity', compact('action_button'))
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#btn-running-{{$activity->id}}").click(function(){
            $("#modal-running-{{$activity->id}}").modal();
        });
    });
</script>

<div class="modal fade" tabindex="-1" id="modal-close-{{$activity->id}}"> role="dialog">
    <div class="modal-dialog modal-lg">
        {{$action_button = "concluir"}}
        @include('modals.state-activity', compact('action_button'))
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#btn-close-{{$activity->id}}").click(function(){
            $("#modal-close-{{$activity->id}}").modal();
        });
    });
</script>
