@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">

            <div class="panel-heading">

                <ol class="breadcrumb">
                    <li><a href ="{{ route('home') }}">Home</a></li>
                    @if(Auth::user()->isManager())
                        <li>Projecto: <a href="/projects/{{$project->id}}">{{$project->name}}</a></li>
                    @else
                        <li>Projecto: <a href="/projects/{{$project->id}}/activities/{{$activity->id}}/show-project-info">{{$project->name}}</a></li>
                    @endif
                    <li class="active">Actividade: {{$activity->name}}</li>
                </ol>

                <h4 class="panel-title">Detalhes da Actividade</h4>
                    <div class="col-sm-3 pull-right">
                        @if(!$activity->isClosed() && !$project->isClosed() && !$project->isCancelled())
                            <form action="/projects/{{$project->id}}/activities/{{$activity->id}}/posts/create" method="get" class="form-inline">
                                <button type="submit" class="btn btn-block btn-primary">Adicionar Registo</button>
                            </form>
                        @endif

                        @if(Auth::user()->isPublisher() && $activity->canBePostedInFacebook())
                            <button type="button" class="btn btn-block btn-success" value="facebook" data-toggle="modal" data-target="#shareModal">
                                Publicar no Facebook</button>
                        @endif
                    </div>
            </div>

            <div class="panel-body">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="form-group" id="fotoContainer">
                    <h4><span class="label label-info">Foto: </span></h4>
                    <img id="activityPhoto" src="{{$activity->getFoto()}}" class="img-thumbnail" alt="Sem foto" style="width: 200px; height: 200px">
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Criado por: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$activity->activityCreatorName() }}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Nome: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$activity->name }}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Descrição: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$activity->description}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Data de Início: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$activity->start_date}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Data de Fim: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        @if($activity->end_date)
                            <h4>{{$activity->end_date}}</h4>
                        @else
                            <h4>Por definir</h4>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Localização: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$activity->location}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Url da actividade: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4><a href="{{$activity->activity_url}}">{{$activity->activity_url}}</a></h4>
                    </div>
                </div>

                <hr>

                <h4 class="panel-title">Utilizadores associados</h4>

                <div class="table-responsive">
                    @if($activity->users()->count() > 0)
                        <table class="table table-hover">

                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nome</th>
                                <th>Tipo de Perfil</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($activity->getAllUsers() as $user)

                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name}}</td>
                                    <td>{{ $user->roleToStr() }}</td>
                                </tr>

                            @endforeach
                        </table>

                        <div class="row" align="center">
                            {{$activity->getAllUsers()->appends(Request::except('page'))->links()}}
                        </div>
                    @else
                        <h4>Não foram encontrados utilizadores</h4>
                    @endif

                </div>

                <hr>

                @include('posts.show-list')

                <hr>

                @include('attachments.show-list-in-activities')

                <hr>

                @include('gallery.gallery')

                <hr>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Estado da actividade: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$activity->statusToStr()}}</h4>
                    </div>
                </div>

                <hr>

                <div class="row">

                    @include('activities.options-show')

                </div>

            </div>
        </div>
    </div>

    @include('modals.image-preview')

    @if(Auth::user()->isPublisher() && $activity->canBePostedInFacebook())

        @include('modals.share')

        <script>
            $('#shareModal').on('show.bs.modal', function () {
                $('#fbText').val('{{$activity->description}}');
            });
        </script>

    @endif
@endsection