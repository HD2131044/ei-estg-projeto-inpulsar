@if(Auth::user()->isAdmin() && $user->getUserLogs()->count())
    <div class="col-md-3">
        <form action="/users/{{$user->id}}/logs" method="get" class="inline">
            <button type="submit" class="btn btn-block btn-default">Visualisar Log</button>
        </form>
    </div>
@endif
@if(Auth::user() == $user)
    <div class="col-md-3">
        <form action="{{ route('user.edit-profile') }}" method="get" class="inline">
            <button type="submit" class="btn btn-block btn-default">Editar os  meus dados</button>
        </form>
    </div>
@else
    @if(!$user->isAdmin())
        @if(Auth::user()->isAdmin())

            <div class="col-md-3">
                <form action="/users/{{ $user->id }}/edit" method="get" class="inline">
                    <button type="submit" class="btn btn-block btn-default">Editar</button>
                </form>
            </div>

            <div class="col-md-3">
                <button type="button" class="btn btn-block btn-danger" data-toggle="modal" data-target="#modal-delete-{{$user->id}}">Apagar</button>
                <div class="modal fade" tabindex="-1" id="modal-delete-{{$user->id}}" role="dialog">
                    <div class="modal-dialog modal-lg">
                        {{$action_url = "/users/$user->id"}}
                        @include('modals.delete', compact('action_url'))
                    </div>
                </div>
            </div>

            @if($user->isBlocked())
                <div class="col-md-3">
                    <form action="{{ route('admin.unblockUser', ['user' => $user]) }}" method="post" class="inline">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-success">Desbloquear</button>
                        </div>
                    </form>
                </div>
            @else
                <div class="col-md-3">
                    <form action="{{ route('admin.blockUser', ['user' => $user]) }}" method="post" class="inline">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-danger">Bloquear</button>
                        </div>
                    </form>
                </div>
            @endif

            <div class="col-md-3">
                <form action="{{ route('admin.resetPassword', ['user' => $user]) }}" method="post" class="inline">
                    {{ csrf_field() }}
                    @if($user->isBlocked() || !$user->isActivated())
                        <button type="submit" class="btn btn-block btn-warning " disabled>Reinicializar Password</button>
                    @else
                        <button type="submit" class="btn btn-block btn-warning ">Reinicializar Password</button>
                    @endif
                </form>
            </div>
            <div class="col-md-3">
                <form action="{{ route('admin.activationEmail', ['user' => $user]) }}" method="post" class="inline">
                    {{ csrf_field() }}
                    @if($user->isActivated() || $user->isBlocked())
                        <button type="submit" class="btn btn-block btn-warning " disabled>Reenviar link de activação</button>
                    @else
                        <button type="submit" class="btn btn-block btn-warning ">Reenviar link de activação</button>
                    @endif
                </form>
            </div>
        @endif
    @endif
@endif

<div class="col-md-3">
    <a class="btn btn-block btn-default " href="{{ route('home') }}">Voltar</a>
</div>