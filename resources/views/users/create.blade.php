@extends('layouts.app')

@section('content')
    @if (count($errors) > 0)
        @include('partials.errors')
    @endif

    <div class="container" style="width: 85%">
        <div class="panel panel-default">
            <div class="panel-heading">

                <ol class="breadcrumb">
                    <li><a href ="{{ route('home') }}">Home</a></li>
                    <li class="active">Criar utilizador</li>
                </ol>

                <h4 class="panel-title">Criar Utilizador</h4>
            </div>

            <div class="panel-body">
                <form action="/users" method="post" class="form-group" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group" id="fotoContainer">
                        <label for="profilePhoto" style="display: block">Foto: </label>
                        <img id="profilePhoto" src="" class="img-thumbnail" alt="Clique aqui para escolher foto" style="width: 200px; height: 200px">
                    </div>
                    <input type="file" name="foto" id="foto" class="hidden"/>

                    <div class="form-group">
                        <label class="required" for="inputName">Nome</label>
                        <input
                                type="text" class="form-control"
                                name="nome" id="inputName"
                                value="{{old('nome')}}"/>
                    </div>

                    <div class="form-group">
                        <label class="required" for="inputEmail">Email</label>
                        <input
                                type="email" class="form-control"
                                name="email" id="inputEmail"
                                value="{{old('email')}}"/>
                    </div>

                    <div class="form-group">
                        <label class="required" for="inputBirthDate">Data de Nascimento</label>
                        <input
                                type="date" class="form-control"
                                name="data_de_nascimento" id="inputBirthDate"
                                value="{{old('data_de_nascimento')}}"/>
                    </div>

                    <div class="form-group">
                        <label class="required" for="inputGender">Sexo</label>
                        <select name="sexo" id="inputGender" class="form-control">
                            @if(!old('sexo'))
                                <option disabled selected> -- escolha uma opção -- </option>
                                <option value="M">Masculino</option>
                                <option value="F">Feminino</option>
                            @else
                                @if(old('sexo') == 'M')
                                    <option selected value="M">Masculino</option>
                                    <option value="F">Feminino</option>
                                @else
                                    <option value="M">Masculino</option>
                                    <option selected value="F">Feminino</option>
                                @endif
                            @endif

                        </select>
                    </div>

                    <div class="form-group">
                        <label class="required" for="inputAddress">Morada</label>
                        <textarea
                                rows="3" class="form-control"
                                name="morada" id="inputAddress" style="resize: none;">{{old('morada')}}</textarea >
                    </div>

                    <div class="form-group">
                        <label for="inputProfileUrl">URL Pessoal</label>
                        <input
                                type="text" class="form-control"
                                name="url_pessoal" id="inputProfileUrl"
                                value="{{old('url_pessoal')}}"/>
                    </div>

                    <div class="form-group">
                        <label for="inputDescription">Descrição</label>
                        <textarea
                                rows="3" class="form-control"
                                name="descrição" id="inputDescription" style="resize: none;">{{old('descrição')}}</textarea >
                    </div>

                    <hr>

                    <div class="form-group">
                        <label class="required" for="inputRole">Tipo de Conta</label>
                        <select name="tipo_de_conta" id="inputRole" class="form-control">
                            @if(!old('tipo_de_conta'))
                                <option disabled selected> -- escolha uma opção -- </option>
                                <option value="1">Diretor</option>
                                <option value="2">Coordenador</option>
                                <option value="3">Técnico do projecto</option>
                            @else
                                @if(old('tipo_de_conta') == 1)
                                    <option selected value="1">Diretor</option>
                                    <option value="2">Coordenador</option>
                                    <option value="3">Técnico do projecto</option>
                                @elseif(old('tipo_de_conta') == 2))
                                    <option value="1">Diretor</option>
                                    <option selected value="2">Coordenador</option>
                                    <option value="3">Técnico do projecto</option>
                                @else
                                    <option value="1">Diretor</option>
                                    <option value="2">Coordenador</option>
                                    <option selected value="3">Técnico do projecto</option>
                                @endif
                            @endif
                        </select>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label class="required" for="inputPublisher">Publicar no Facebook</label>
                        <select name="pode_publicar_no_facebook" id="inputPublisher" class="form-control" required>
                            @if(!old('pode_publicar_no_facebook'))
                                <option disabled selected> -- escolha uma opção -- </option>
                                <option value="1">Sim</option>
                                <option value="0">Não</option>
                            @else
                                @if(old('pode_publicar_no_facebook') == '1')
                                    <option selected value="1">Sim</option>
                                    <option value="0">Não</option>
                                @else
                                    <option value="1">Sim</option>
                                    <option selected value="0">Não</option>
                                @endif
                            @endif

                        </select>
                    </div>

                    <hr>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="ok">Guardar</button>
                        <a class="btn btn-default" href="{{ route('home') }}">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        var inputFoto = $('#foto');
        $('#fotoContainer').on('click', function () {
            inputFoto.click();
        });

        inputFoto.on('change', function () {
            var imageType = /image.*/;
            if (this.files && this.files[0] && this.files[0].type.match(imageType)) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#profilePhoto').attr('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }
        });
    </script>
@endsection