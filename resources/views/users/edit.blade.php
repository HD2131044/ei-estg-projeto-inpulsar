@extends('layouts.app')

@section('content')
    @if (count($errors) > 0)
        @include('partials.errors')
    @endif

    <div class="container" style="width: 85%">
        <div class="panel panel-default">
            <div class="panel-heading">

                <ol class="breadcrumb">
                    <li><a href ="{{ route('home') }}">Home</a></li>
                    @if(Auth::user() == $user)
                        <li><a href="{{ route('user.show-profile') }}">Meu perfil</a></li>
                        <li class="active">Editar o meu perfil</li>
                    @else
                        <li>Utilizador: <a href="/users/{{$user->id}}">{{$user->name}}</a></li>
                        <li class="active">Editar detalhes do utilizador</li>
                    @endif
                </ol>

                <h4 class="panel-title">Editar Detalhes do Utilizador</h4>
            </div>

            <div class="panel-body">

                <form
                    @if(Auth::user() == $user)
                        action="{{ route('user.update-profile') }}" method="post" class="form-group" enctype="multipart/form-data">
                    @else
                        action="/users/{{ $user->id }}" method="post" class="form-group" enctype="multipart/form-data">
                    @endif
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}

                    <div class="form-group" id="fotoContainer">
                        <label for="profilePhoto" style="display: block">Foto: </label>
                        <img id="profilePhoto" src="{{$user->getFoto()}}" class="img-thumbnail" alt="Clique aqui para escolher foto" style="width: 200px; height: 200px">
                    </div>
                    <input type="file" name="foto" id="foto" class="hidden"/>

                    <div class="form-group">
                        <label class="required" for="inputName">Nome: </label>
                        <input
                                type="text" class="form-control"
                                name="nome" id="inputName"
                                value="{{$user->name}}"/>
                    </div>

                    <div class="form-group">
                        <label class="required"for="inputEmail">Email</label>
                        <input
                                type="email" class="form-control"
                                name="email" id="inputEmail"
                                value="{{$user->email}}"/>
                    </div>

                    <div class="form-group">
                        <label class="required" for="inputBirthDate">Data de Nascimento</label>
                        <input
                                type="date" class="form-control"
                                name="data_de_nascimento" id="inputBirthDate"
                                value="{{$user->birth_date}}"/>
                    </div>

                    <div class="form-group">
                        <label class="required" for="inputGender">Sexo</label>
                        <select name="sexo" id="inputGender" class="form-control">

                            @if($user->gender == 'M')
                                <option selected value="M">Masculino</option>
                                <option value="F">Feminino</option>
                            @else
                                <option value="M">Masculino</option>
                                <option selected value="F">Feminino</option>
                            @endif

                        </select>
                    </div>

                    <div class="form-group">
                        <label class="required" for="inputAddress">Morada</label>
                        <textarea
                                rows="3" class="form-control"
                                name="morada" id="inputAddress" style="resize: none;">{{$user->address}}</textarea >
                    </div>

                    <div class="form-group">
                        <label for="inputProfileUrl">URL Pessoal</label>
                        <input
                                type="text" class="form-control"
                                name="url_pessoal" id="inputProfileUrl"
                                value="{{$user->profile_url}}"/>
                    </div>

                    <div class="form-group">
                        <label for="inputDescription">Descrição</label>
                        <textarea
                                rows="3" class="form-control"
                                name="descrição" id="inputDescription"
                                value="{{$user->description}}" style="resize: none;"></textarea >
                    </div>

                    <hr>

                    @if(Auth::user() == $user)
                        <input type="hidden" class="form-control" name="tipo_de_conta" id="inputRole" value="{{$user->role}}"/>
                        <input type="hidden" class="form-control" name="pode_publicar_no_facebook" id="inputRole" value="{{$user->publisher}}"/>
                    @else
                        <div class="form-group">
                            <label class="required" for="inputRole">Tipo de Conta</label>
                            <select name="tipo_de_conta" id="inputRole" class="form-control">
                                @if($user->isAdmin())
                                    <option type="hidden" selected value="0">Gestor de conta</option>
                                @elseif($user->isDirector())
                                    <option selected value="1">Diretor</option>
                                    <option value="2">Coordenador</option>
                                    <option value="3">Técnico do projecto</option>
                                @elseif($user->isManager())
                                    <option value="1">Diretor</option>
                                    <option selected value="2">Coordenador</option>
                                    <option value="3">Técnico do projecto</option>
                                @else
                                    <option value="1">Diretor</option>
                                    <option value="2">Coordenador</option>
                                    <option selected value="3">Técnico do projecto</option>
                                @endif
                            </select>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label class="required" for="inputPublisher">Publicar no Facebook</label>
                            <select name="pode_publicar_no_facebook" id="inputPublisher" class="form-control">

                                @if($user->publisher == 1)
                                    <option selected value="1">Sim</option>
                                    <option value="0">Não</option>
                                @else
                                    <option value="1">Sim</option>
                                    <option selected value="0">Não</option>
                                @endif

                            </select>
                        </div>

                        <hr>

                    @endif

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="ok">Guardar</button>
                        @if(Route::is('user.edit-profile'))
                            <a class="btn btn-default" href="{{ route('user.show-profile') }}">Voltar</a>
                        @else
                            <a class="btn btn-default" href="/users/{{$user->id}}">Voltar</a>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        var inputFoto = $('#foto');
        $('#fotoContainer').on('click', function () {
           inputFoto.click();
        });
        
        inputFoto.on('change', function () {
            var imageType = /image.*/;
            if (this.files && this.files[0] && this.files[0].type.match(imageType)) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#profilePhoto').attr('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }
        });
    </script>
@endsection
