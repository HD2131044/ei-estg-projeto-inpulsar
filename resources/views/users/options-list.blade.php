@if(Auth::user() == $user)
    <form action="{{ route('user.show-profile') }}" method="get" class="inline">
        <button id="detalhes{{$user->id}}" type="submit" class="btn btn-xs btn-block btn-default">Detalhes</button>
    </form>
@else
    <form action="/users/{{$user->id}}" method="get" class="inline">
        <button id="detalhes{{$user->id}}" type="submit" class="btn btn-xs btn-block btn-default">Detalhes</button>
    </form>
@endif