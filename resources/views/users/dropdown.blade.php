@if(!$user->isAdmin())
    <div class="btn-group">
        <button id="actions{{$user->id}}" type="button" class="btn btn-xs btn-block btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Acções <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" style="right: 4px;left: auto;">
            @if($user->isBlocked())
                <li>
                    <a id="unblock{{$user->id}}" href="/users/{{$user->id}}/unblock"
                       onclick="event.preventDefault();
                               document.getElementById('unblock-form-{{$user->id}}').submit();">
                        Desbloquear
                    </a>

                    <form id="unblock-form-{{$user->id}}" action="{{ route('admin.unblockUser', ['user' => $user]) }}" method="post">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}
                    </form>
                </li>
            @else
                <li>
                    <a id="block{{$user->id}}" href="/users/{{$user->id}}/block"
                       onclick="event.preventDefault();
                               document.getElementById('block-form-{{$user->id}}').submit();">
                        Bloquear
                    </a>

                    <form id="block-form-{{$user->id}}" action="{{ route('admin.blockUser', ['user' => $user]) }}" method="post">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}
                    </form>
                </li>
            @endif
        </ul>
    </div>
@endif
