@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">

            <div class="panel-heading">

                <ol class="breadcrumb">
                    <li><a href ="{{ route('home') }}">Home</a></li>
                    @if(Auth::user() == $user)
                        <li class="active">Meu perfil</li>
                    @else
                        <li class="active">Utilizador: {{$user->name}}</li>
                    @endif
                </ol>

                @if(Auth::user() == $user)
                    <h4 class="panel-title">Perfil do Utilizador</h4>
                @else
                    <h4 class="panel-title">Detalhes do Utilizador</h4>
                @endif
            </div>

            <div class="panel-body">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Foto: </span></h4>
                    </div>
                    <div>
                        <img src="{{$user->getFoto()}}" class="img-thumbnail" alt="Sem foto" style="width: 200px; height: 200px">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Nome: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$user->name }}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Email: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$user->email}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Idade: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$user->getAge()}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Sexo: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$user->getGender()}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Morada: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$user->address}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Url pessoal: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4><a href="{{$user->profile_url}}">{{$user->profile_url}}</a> </h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Descrição: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$user->description}}</h4>
                    </div>
                </div>

                @if(Auth::user() != $user)

                    <hr>

                    <div class="row">
                        <div class="col-md-2">
                            <h4><span class="label label-info">Perfil: </span></h4>
                        </div>
                        <div class="col-md-4 pull-left">
                            <h4>{{$user->roleToStr()}}</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <h4><span class="label label-info">Publicar no Facebook: </span></h4>
                        </div>
                        <div class="col-md-4 pull-left">
                            <h4>{{$user->canPublish()}}</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <h4><span class="label label-info">Conta: </span></h4>
                        </div>
                        <div class="col-md-4 pull-left">
                            <h4>{{$user->activatedToStr()}}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <h4><span class="label label-info">Bloqueado: </span></h4>
                        </div>
                        <div class="col-md-4 pull-left">
                            <h4>{{$user->blockedToStr()}}</h4>
                        </div>
                    </div>

                @endif

                <hr>

                <div class="row">

                    @include('users.options-show')

                </div>
            </div>
        </div>
    </div>
@endsection
