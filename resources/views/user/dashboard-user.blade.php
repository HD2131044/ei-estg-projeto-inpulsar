<style>
    .dataTable > thead > tr > th[class*="sort"]:after{
        content: "" !important;
    }

    table.dataTable thead > tr > th.sorting_asc,
    table.dataTable thead > tr > th.sorting_desc,
    table.dataTable thead > tr > th.sorting,
    table.dataTable thead > tr > td.sorting_asc,
    table.dataTable thead > tr > td.sorting_desc,
    table.dataTable thead > tr > td.sorting {
        padding-right: inherit;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="panel panel-default">
                <div class="panel-heading">

                    <ol class="breadcrumb">
                        <li class="active">Home</li>
                    </ol>

                    <div class="row">
                        <div class="col-sm-6"> <h3>{{ $title }}</h3> </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="table-responsive">
                        @if(count($arrayData) > 0)
                            <table id="dashUserTable" class="table table-hover">

                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Criada Por</th>
                                    <th>Nome</th>
                                    <th>Data de Início</th>
                                    <th>Data de Fim</th>
                                    <th>Estado da Actividade</th>
                                    {{--<th>Pertence ao Projecto</th>--}}
                                    <th>Nº Utilizadores</th>
                                    <th>Nº Registos</th>
                                    <th>Nº Anexos</th>
                                    <th>Opções</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach ($arrayData as $activity)

                                        <tr>
                                            <td>{{ $activity->id }}</td>
                                            <td>{{ $activity->activityCreatorName()}}</td>
                                            <td>{{ $activity->name }}</td>
                                            <td>{{ $activity->start_date }}</td>
                                            <td>{{ $activity->end_date }}</td>
                                            <td>{{ $activity->statusToStr() }}</td>
                                           {{-- <td><a href ="/projects/{{$activity->project_id}}/activities/{{$activity->id}}/show-project-info">nome</a></td>--}}
                                            <td>{{ $activity->users()->count() }}</td>
                                            <td>{{ $activity->getNumberActivePosts() }}</td>
                                            <td>{{ $activity->getNumberAttachmentsFromActivityAndActivePosts() }}</td>
                                            <td>
                                                <div>

                                                    @include('activities.options-list')

                                                </div>
                                            </td>
                                        </tr>

                                @endforeach
                            </table>
                            <div class="row" align="center">
                                {{--{{$arrayData->appends(Request::except('page'))->links()}}--}}
                            </div>
                        @else
                            <h3>Não foram encontradas actividades</h3>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#dashUserTable').DataTable({
        responsive: true,
        paging: true,
        ordering: true,
        iDisplayLength: 10
    });
</script>