<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- Classe CSS pessoal -->
    <link href="{{ asset('css/personal.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select.bootstrap.min.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

    <!-- Scripts -->
    <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.select.min.js') }}"></script>

</head>
<body>
<div id="app">

    @include('layouts.nav')

    <div style="padding-top: 80px;">
        <div>
            @if(Session::has('error.message'))
                @include('partials.error')
            @elseif(Session::has('success.message'))
                @include('partials.success')
            @endif
        </div>
        @yield('content')
    </div>
</div>
</body>
<footer>
    <div class="footer @if(request()->is('login')) navbar-fixed-bottom @endif ">
        <div class="panel-footer" style="text-align: center;">&copy;2017 InPulsar Intranet.
            Criado por <a href="https://www.linkedin.com/in/carlos-manuel-marques-dos-santos-8625b1136/">Carlos Santos</a> e
            <a href="https://www.linkedin.com/in/hugoaguadias/">Hugo Dias</a>,
            <a href="https://www.ipleiria.pt/estg/dei/">DEI-ESTG-IPLeiria</a>. All rights reserved.</div>
    </div>
</footer>
</html>
