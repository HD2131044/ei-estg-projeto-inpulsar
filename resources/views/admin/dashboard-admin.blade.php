<style>
    .dataTable > thead > tr > th[class*="sort"]:after{
        content: "" !important;
    }

    table.dataTable thead > tr > th.sorting_asc,
    table.dataTable thead > tr > th.sorting_desc,
    table.dataTable thead > tr > th.sorting,
    table.dataTable thead > tr > td.sorting_asc,
    table.dataTable thead > tr > td.sorting_desc,
    table.dataTable thead > tr > td.sorting {
        padding-right: inherit;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="panel panel-default">
                <div class="panel-heading">

                    <ol class="breadcrumb">
                        <li class="active">Home</li>
                    </ol>

                    <div class="row">
                        <div class="col-sm-6"> <h3>{{ $title }}</h3> </div>
                        <div class="col-sm-3 pull-right">
                            <form action="/users/create" method="get" class="form-inline">
                                <button type="submit" class="btn btn-block btn-primary">Adicionar Utilizador</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="table-responsive">
                        @if(count($arrayData) > 0)
                            <table id="dashAdminTable" class="table table-hover">

                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nome</th>
                                    <th>Idade</th>
                                    <th>Género</th>
                                    <th>Email</th>
                                    <th>Tipo de Perfil</th>
                                    <th>Publicar no Facebook</th>
                                    <th>Estado da Conta</th>
                                    <th>Bloqueado</th>
                                    <th>Opções</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach ($arrayData as $user)

                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->getAge() }}</td>
                                            <td>{{ $user->getGender() }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->roleToStr() }}</td>
                                            <td>{{ $user->canPublish() }}</td>
                                            <td>{{ $user->activatedToStr() }}</td>
                                            <td>{{ $user->blockedToStr() }}</td>
                                            <td>

                                                <div>

                                                    @include('users.options-list')

                                                    @include('users.dropdown')

                                                </div>
                                            </td>
                                        </tr>

                                @endforeach
                            </table>
                            <div class="row" align="center">
                                {{--{{$arrayData->appends(Request::except('page'))->links()}}--}}
                            </div>
                        @else
                            <h3>Não foram encontrados utilizadores</h3>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#dashAdminTable').DataTable({
        responsive: true,
        paging: true,
        ordering: true,
        iDisplayLength: 10
    });
</script>
