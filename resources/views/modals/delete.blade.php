<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        @if(str_contains($action_url, 'users'))
            <h4 class="modal-title">Apagar utilizador {{$user->name}}?</h4>
        @elseif(str_contains($action_url, 'attachments'))
            <h4 class="modal-title">Apagar anexo {{$attachment->original_filename}}?</h4>
        @elseif(str_contains($action_url, 'posts'))
            <h4 class="modal-title">Apagar registo {{$post->title}}?</h4>
        @elseif(str_contains($action_url, 'activities'))
            <h4 class="modal-title">Apagar actividade {{$activity->name}}?</h4>
        @else
            <h4 class="modal-title">Apagar projecto {{$project->name}}?</h4>
        @endif
    </div>
    <div class="modal-body">
        @if(str_contains($action_url, 'users'))
            <p>Confirme caso pretenda apagar os dados do utilizador.</p>
        @elseif(str_contains($action_url, 'attachments'))
            <p>Confirme caso pretenda apagar o anexo.</p>
        @elseif(str_contains($action_url, 'posts'))
            <p>Confirme caso pretenda apagar os dados do registo. Todos os anexos associados serão também apagados.</p>
        @elseif(str_contains($action_url, 'activities'))
            <p>Confirme caso pretenda apagar os dados da actividade. Todos os registos e anexos associados serão também apagados.</p>
        @else
            <p>Confirme caso pretenda apagar os dados do projecto. Todos as actividades, registos e anexos associados serão também apagados.</p>
        @endif
    </div>
    <div class="modal-footer">
        <div class="form-group">
            @if(str_contains($action_url, 'users'))
                <form action="/users/{{ $user->id }}" method="POST" class="inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-block btn-danger">Confirmar Apagar</button>
                </form>
            @elseif(str_contains($action_url, 'attachments'))
                <form action="/projects/{{$attachment->project_id}}/attachments/{{$attachment->id}}" method="post" class="inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-block btn-danger">Confirmar Apagar</button>
                </form>
            @elseif(str_contains($action_url, 'posts'))
                <form action="/projects/{{$project->id}}/activities/{{$activity->id}}/posts/{{$post->id}}" method="POST" class="inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-block btn-danger">Confirmar Apagar</button>
                </form>
            @elseif(str_contains($action_url, 'activities'))
                <form action="/projects/{{$project->id}}/activities/{{$activity->id}}" method="POST" class="inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-block btn-danger">Confirmar Apagar</button>
                </form>
            @else(str_contains(URL::current(), 'projects'))
                <form action="/projects/{{ $project->id }}" method="POST" class="inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-block btn-danger">Confirmar Apagar</button>
                </form>
            @endif
        </div>
        <div class="form-group">
            <button type="button" class="btn btn-block btn-default" data-dismiss="modal">Cancelar</button>
        </div>
    </div>
</div>