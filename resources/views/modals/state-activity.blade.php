<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pretende alterar o estado da actividade {{$activity->name}} para estado
            @if($action_button == 'decorrer')
                "A Decorrer"
            @elseif($action_button == 'concluir')
                "Concluída"
            @endif
            ?
        </h4>
    </div>
    <div class="modal-body">
        <p>Confirme caso pretenda alterar o estado da actividade.</p>
        @if($action_button == 'decorrer')
            <p>Implica que a actividade foi aprovada.</p>
        @elseif($action_button == 'concluir')
            <p>Implica que a actividade foi concluída.</p>
        @endif
    </div>
    <div class="modal-footer">
        <div class="form-group">
            @if($action_button == 'decorrer')
                <form action="{{ route('manager.runningActivity', ['project'=> $project, 'activity' => $activity]) }}" method="post" class="inline">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <button id="decorrer{{$activity->id}}" type="submit" class="btn btn-block btn-warning">Decorrer</button>
                </form>
            @elseif($action_button == 'concluir')
                <form action="{{ route('manager.closeActivity', ['project'=> $project, 'activity' => $activity]) }}" method="post" class="inline">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <button id="concluir{{$activity->id}}" type="submit" class="btn btn-block btn-danger">Concluir</button>
                </form>
            @endif
        </div>
        <div class="form-group">
            <button type="button" class="btn btn-block btn-default" data-dismiss="modal">Cancelar</button>
        </div>
    </div>
</div>