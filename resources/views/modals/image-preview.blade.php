<div class="modal fade" id="imagePreviewModal" tabindex="-1" role="dialog" aria-labelledby="imagePreviewModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title" id="imageName"></h4>
            </div>
            <div class="modal-body">
                <img id="imagePreview" class="img-responsive" src="" style="max-width: 400px; max-height: 400px; align-content: center">
            </div>
        </div>
    </div>
</div>