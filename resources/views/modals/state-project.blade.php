<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        @if($action_button == 'submeter' || $action_button == 'resubmeter')
            <h4 class="modal-title">Pretende {{$action_button}} o projecto {{$project->name}}?</h4>
        @else
            <h4 class="modal-title">Pretende alterar o estado do projecto {{$project->name}} para estado
                @if($action_button == 'decorrer')
                    "A Decorrer"
                @elseif($action_button == 'cancelar')
                    "Cancelado"
                @elseif($action_button == 'concluir')
                    "Concluído"
                @endif
                ?
            </h4>
        @endif
    </div>
    <div class="modal-body">
        @if($action_button == 'submeter')
            <p>Confirme caso pretenda {{$action_button}} o projecto.</p>
        @elseif($action_button == 'decorrer')
            <p>Confirme caso pretenda alterar o estado do projecto.</p>
            <p>Implica que o projecto foi aprovado.</p>
        @elseif($action_button == 'cancelar')
            <p>Confirme caso pretenda alterar o estado do projecto.</p>
            <p>Implica que o projecto foi reprovado.</p>
        @elseif($action_button == 'concluir')
            <p>Confirme caso pretenda alterar o estado do projecto.</p>
            <p>Implica que o projecto foi concluído.</p>
            <p><strong>Todas as actividades associadas ao projecto vão passar ao estado de "Concluída", de forma automática.</strong></p>
        @elseif($action_button == 'resubmeter')
            <p>Confirme caso pretenda {{$action_button}} o projecto.</p>
            <p><strong>O projeto vai voltar ao estado "Pendente" e com estado de submissão nulo.</strong></p>
        @endif
    </div>
    <div class="modal-footer">
        <div class="form-group">
            @if($action_button == 'submeter')
                <form action="{{ route('director.submitProject', ['project' => $project]) }}" method="post" class="inline">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <button id="submeter{{$project->id}}" type="submit" class="btn btn-block btn-warning">Submeter Projecto</button>
                </form>
            @elseif($action_button == 'decorrer')
                <form action="{{ route('director.runningProject', ['project' => $project]) }}" method="post" class="inline">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <button id="decorrer{{$project->id}}" type="submit" class="btn btn-block btn-warning">Decorrer</button>
                </form>
            @elseif($action_button == 'cancelar')
                <form action="{{ route('director.cancelProject', ['project' => $project]) }}" method="post" class="inline">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <button id="cancelar{{$project->id}}" type="submit" class="btn btn-block btn-warning">Cancelar</button>
                </form>
            @elseif($action_button == 'concluir')
                <form action="{{ route('director.closeProject', ['project' => $project]) }}" method="post" class="inline">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <button id="concluir{{$project->id}}" type="submit" class="btn btn-block btn-warning">Concluir</button>
                </form>
            @elseif($action_button == 'resubmeter')
                <form action="{{ route('director.pendingProject', ['project' => $project]) }}" method="post" class="inline">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <button id="resubmeter{{$project->id}}" type="submit" class="btn btn-block btn-warning">Resubmeter Projecto</button>
                </form>
            @endif
        </div>
        <div class="form-group">
            <button type="button" class="btn btn-block btn-default" data-dismiss="modal">Cancelar</button>
        </div>
    </div>
</div>