<div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                @if(str_contains(URL::current(), 'posts'))
                    <h4 class="modal-title">Registo não publicado no Facebook: {{$post->title}}</h4>
                @elseif(str_contains(URL::current(), 'activities'))
                    <h4 class="modal-title">Actividade não publicada no Facebook: {{$activity->name}}</h4>
                @elseif(str_contains(URL::current(), 'projects'))
                    <h4 class="modal-title">Projecto não publicado no Facebook: {{$project->name}}</h4>
                @else
                    <h4 class="modal-title">Não publicado no Facebook</h4>
                @endif
            </div>
            <div class="modal-body">
                <h5 id="alertModalMessage"></h5>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button type="button" class="btn btn-block btn-default" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
</div>