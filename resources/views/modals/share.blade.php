<style>
    .portfolio .thumbnail {
        position: relative;
    }

    .portfolio .thumbnail:hover {
        cursor: pointer;
    }

    .portfolio .caption {
        bottom: 0;
        position: absolute;
    }

    .portfolio .btn {
        opacity: 0.75;
    }
</style>

<input id="appId" type="hidden" value="{{env('FB_APP_ID')}}">
<input id="token" type="hidden" value="{{env('FB_ACCESS_TOKEN')}}">
<input id="page" type="hidden" value="{{env('FB_PAGE_ID')}}">

<div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="shareModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                @if(str_contains(URL::current(), 'posts'))
                    <h4 class="modal-title">Registo a publicar no Facebook: {{$post->title}}</h4>
                @elseif(str_contains(URL::current(), 'activities'))
                    <h4 class="modal-title">Actividade a publicar no Facebook: {{$activity->name}}</h4>
                @elseif(str_contains(URL::current(), 'projects'))
                    <h4 class="modal-title">Projecto a publicar no Facebook: {{$project->name}}</h4>
                @else
                    <h4 class="modal-title">Publicar no Facebook</h4>
                @endif
            </div>
            <div class="modal-body">
                <form id="fbForm" class="form form-group" enctype="multipart/form-data">
                    <div class="well form-group">
                        <label class="control-label" for="fbText">Mensagem: </label>
                        <textarea id="fbText" class="form-control" name="fbText" style="resize: none; height: 160px"></textarea>

                        <hr>

                        <h4 class="panel-title">Escolha as imagens a partilhar</h4>

                        <div style="overflow: scroll; overflow-x: hidden; height: 200px">
                            <div class="panel-body row portfolio" style="padding-top: 60px">
                                @if($galleryImages->count() > 0)
                                    <?php $i = 0?>
                                    @foreach($galleryImages as $image)
                                        @if($image->isActive())
                                            <div class="col-sm-6 col-md-3">
                                                <div id="imageContainer" onclick="toggleCheckOnClickImage(this)"
                                                     class="thumbnail" style="border: solid #bfbfbf 1px">
                                                    <input type="checkbox" name="image[]"
                                                           value="data:image/png;base64,{{base64_encode(file_get_contents($image['media_path'] . '/' . $image['filename']))}}">
                                                    <img class="img-responsive"
                                                         src="data:image/png;base64,{{base64_encode(file_get_contents($image['media_path'] . '/' . $image['filename']))}}"
                                                         style="max-width: 80px; height: 80px">
                                                </div>
                                            </div>
                                            <?php $i++?>
                                        @endif
                                    @endforeach
                                @else
                                    <h4>Não foram encontradas imagens</h4>
                                @endif
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button type="button" class="btn btn-block btn-success" onclick="facebookShare()" data-dismiss="modal">
                        Publicar no Facebook
                    </button>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-block btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>

@include('modals.success-modal')
@include('modals.error-modal')

<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: $('#appId').val(),
            xfbml: true,
            version: 'v2.9'
        });

        FB.AppEvents.logPageView();
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    var facebookShare = function () {
        if($('#fbText').val().trim().length > 0){
            postImageToFacebook($('#token').val());
        } else {
            alert('A mensagem não pode estar vazia.');
        }
    };

    var toggleCheckOnClickImage = function (elem) {
        var checkbox = $(elem).find('input');
        if (checkbox.attr('checked')) {
            checkbox.attr('checked', false);
        } else {
            checkbox.attr('checked', true);
        }
    };

    var postToFeed = function (images) {
        var body = $('#fbText').val();
        FB.api('/'+$('#page').val()+'/feed', 'post', {access_token: $('#token').val(), message: body, attached_media: images}, function (response) {
            if (!response || response.error) {
                var errorModal = $('#errorModal');
                errorModal.find('#alertModalMessage').html('Erro ao publicar no Facebook.');
                errorModal.modal().toggle();
                console.log(response);
            } else {
                var successModal = $('#successModal');
                successModal.find('#alertModalMessage').html('Publicação efectuada com sucesso.');
                successModal.modal().toggle();
                $('#fbText').val('');
                $('[name="image[]"]').attr('checked', false);

                var url = "{{url()->current()}}";

                /*ENVIA LOG*/
                $.ajax({
                    type: "post",
                    url: "/registerfblog",
                    headers: {
                        'X-CSRF-Token': "{{csrf_token()}}"
                    },
                    data: {'url' : url},
                    success: function (data) {

                    },
                    error: function (response) {
                        console.log("error " + response);
                    }
                });
            }
        });
    };

    var postImageToFacebook = function (authToken) {
        var images = $('#fbForm [name="image[]"]').serializeArray();
        var imagesBase64 = [];
        var publishedImagesIds = [];
        $.each(images, function (index, value) {
            imagesBase64.push(value.value);
        });
        var i=0;
        if (images.length > 0) {
            $.each(images, function (index, value) {
                var imageData = value.value;
                try {
                    blob = dataURItoBlob(imageData);
                }
                catch (e) {
                    console.log(e);
                }
                var fd = new FormData();
                fd.append("access_token", authToken);
                fd.append("source", blob);
                fd.append("published", false);
                try {
                    $.ajax({
                        url: "https://graph.facebook.com/"+$('#page').val()+"/photos?access_token=" + authToken,
                        type: "POST",
                        data: fd,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (data) {
                            console.log("success " + data);
                            publishedImagesIds.push({'media_fbid' : data.id});
                            i++;
                            if(i == images.length){
                                postToFeed(publishedImagesIds);
                            }
                        },
                        error: function (shr, status, data) {
                            console.log("error " + data + " Status " + shr.status);
                        },
                        complete: function () {
                            console.log("Posted to facebook");
                        }
                    });
                }
                catch (e) {
                    console.log(e);
                }
            });
        } else {
            postToFeed([]);
        }
    };

    function dataURItoBlob(dataURI) {
        var byteString = atob(dataURI.split(',')[1]);
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ab], {type: 'image/png'});
    }
</script>
