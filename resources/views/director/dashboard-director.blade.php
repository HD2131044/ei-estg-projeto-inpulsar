<style>
    .dataTable > thead > tr > th[class*="sort"]:after{
        content: "" !important;
    }

    table.dataTable thead > tr > th.sorting_asc,
    table.dataTable thead > tr > th.sorting_desc,
    table.dataTable thead > tr > th.sorting,
    table.dataTable thead > tr > td.sorting_asc,
    table.dataTable thead > tr > td.sorting_desc,
    table.dataTable thead > tr > td.sorting {
        padding-right: inherit;
    }
</style>

<div class="container">
    <div class="row">
        <div class="col">
            <div class="panel panel-default">
                <div class="panel-heading">

                    <ol class="breadcrumb">
                        <li class="active">Home</li>
                    </ol>

                    <div class="row">
                        <div class="col-sm-6"> <h3>{{ $title }}</h3> </div>
                        <div class="col-sm-3 pull-right">
                            <form action="/projects/create" method="get" class="form-inline">
                                <button type="submit" class="btn btn-block btn-primary">Adicionar Projecto</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="table-responsive">
                        @if(count($arrayData) > 0)
                            <table id="dashDirectorTable" class="table table-hover">

                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Criado Por</th>
                                    <th>Nome</th>
                                    <th>Data de Início</th>
                                    <th>Data de Fim</th>
                                    <th>Estado do Projecto</th>
                                    <th>Candidatura</th>
                                    <th>Coordenador(es)</th>
                                    <th>Nº Utilizadores</th>
                                    <th>Nº Actividades</th>
                                    <th>Nº Registos</th>
                                    <th>Nº Anexos</th>
                                    <th>Opções</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach ($arrayData as $project)

                                    <tr>
                                        <td>{{ $project->id }}</td>
                                        <td>{{ $project->projectCreatorName()}}</td>
                                        <td>{{ $project->name }}</td>
                                        <td>{{ $project->start_date }}</td>
                                        <td>{{ $project->end_date }}</td>
                                        <td>{{ $project->statusToStr() }}</td>
                                        <td>{{ $project->submittedToStr() }}</td>
                                        <td>{{ $project->getManagersTextToTable() }}</td>
                                        <td>{{ $project->users()->count() }}</td>
                                        <td>{{ $project->getNumberActivities() }}</td>
                                        <td>{{ $project->getNumberPosts() }}</td>
                                        <td>{{ $project->getNumberAttachments() }}</td>
                                        <td>

                                            <div>
                                                @include('projects.options-list')

                                                @include('projects.dropdown')
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach
                            </table>
                            <div class="row" align="center">
                                {{--{{$arrayData->appends(Request::except('page'))->links()}}--}}
                            </div>
                        @else
                            <h3>Não foram encontrados projectos</h3>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#dashDirectorTable').DataTable({
        responsive: true,
        paging: true,
        ordering: true,
        iDisplayLength: 10
    });
</script>