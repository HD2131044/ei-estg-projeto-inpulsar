<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <ul>
        @foreach (Session::get('errors') as $key => $value)
            <li>{{$value[0]}}</li>
        @endforeach
    </ul>
</div>