@if(Auth::user()->isManager())
    @if($project->getProjectLogs()->count())
        <div class="col-md-3">
            <form action="/projects/{{$project->id}}/logs" method="get" class="inline">
                <button type="submit" class="btn btn-block btn-default">Visualisar Log</button>
            </form>
        </div>
    @endif

    <div class="col-md-3">
        <form action="/projects/{{ $project->id }}/edit" method="get" class="inline">
            <button type="submit" class="btn btn-block btn-default">Editar</button>
        </form>
    </div>

    <div class="col-md-3">
        <button type="button" class="btn btn-block btn-danger" data-toggle="modal" data-target="#modal-delete-{{$project->id}}">Apagar</button>
        <div class="modal fade" tabindex="-1" id="modal-delete-{{$project->id}}"> role="dialog">
            <div class="modal-dialog modal-lg">
                {{$action_url = "/projects/$project->id"}}
                @include('modals.delete', compact('action_url'))
            </div>
        </div>
    </div>

    <!-- Estados -->
    @if(!$project->isClosed())
        @if($project->isPending() && !$project->isSubmitted())
            <div class="col-md-3">
                <button type="button" class="btn btn-block btn-warning" data-toggle="modal" data-target="#modal-submit-{{$project->id}}">Submeter</button>
                <div class="modal fade" tabindex="-1" id="modal-submit-{{$project->id}}"> role="dialog">
                    <div class="modal-dialog modal-lg">
                        {{$action_button = "submeter"}}
                        @include('modals.state-project', compact('action_button'))
                    </div>
                </div>
            </div>
        @elseif($project->isPending() && $project->isSubmitted())
            <div class="col-md-3">
                <button type="button" class="btn btn-block btn-warning" data-toggle="modal" data-target="#modal-running-{{$project->id}}">Decorrer</button>
                <div class="modal fade" tabindex="-1" id="modal-running-{{$project->id}}"> role="dialog">
                    <div class="modal-dialog modal-lg">
                        {{$action_button = "decorrer"}}
                        @include('modals.state-project', compact('action_button'))
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-block btn-warning" data-toggle="modal" data-target="#modal-cancel-{{$project->id}}">Cancelar</button>
                <div class="modal fade" tabindex="-1" id="modal-cancel-{{$project->id}}"> role="dialog">
                    <div class="modal-dialog modal-lg">
                        {{$action_button = "cancelar"}}
                        @include('modals.state-project', compact('action_button'))
                    </div>
                </div>
            </div>
        @elseif($project->isRunning())
            <div class="col-md-3">
                <button type="button" class="btn btn-block btn-warning" data-toggle="modal" data-target="#modal-close-{{$project->id}}">Concluir</button>
                <div class="modal fade" tabindex="-1" id="modal-close-{{$project->id}}"> role="dialog">
                    <div class="modal-dialog modal-lg">
                        {{$action_button = "concluir"}}
                        @include('modals.state-project', compact('action_button'))
                    </div>
                </div>
            </div>
        @elseif($project->isCancelled())
            <div class="col-md-3">
                <button type="button" class="btn btn-block btn-warning" data-toggle="modal" data-target="#modal-pending-{{$project->id}}">Resubmeter</button>
                <div class="modal fade" tabindex="-1" id="modal-pending-{{$project->id}}"> role="dialog">
                    <div class="modal-dialog modal-lg">
                        {{$action_button = "resubmeter"}}
                        @include('modals.state-project', compact('action_button'))
                    </div>
                </div>
            </div>
        @endif
    @endif
@endif

<div class="col-md-3">
    <a class="btn btn-block btn-default" href="{{ route('home') }}">Voltar</a>
</div>

