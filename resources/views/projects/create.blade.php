@extends('layouts.app')

@section('content')
    @if (count(Session::get('errors')) > 0)
        @include('partials.errors-ajax')
    @endif

    <style>
        .file-drop{
            background-color: white; outline: 3px dashed lightslategrey; box-shadow: gray; width: 100%; height: 100px
        }
        .file-drop-hover{
            background-color: lightpink;
            outline-color: darkred;
        }
    </style>

    <script>
        var currentUser = {{\Auth::user()->id}}
    </script>

    <div class="container" style="width: 85%">
        <div class="panel panel-default">
            <div class="panel-heading">

                <ol class="breadcrumb">
                    <li><a href ="{{ route('home') }}">Home</a></li>
                    <li class="active">Criar projecto</li>
                </ol>

                <h4 class="panel-title">Criar Projecto</h4>
            </div>

            <div class="panel-body">
                <form id="projectForm" class="form-group" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group" id="fotoContainer">
                        <label for="profilePhoto" style="display: block">Foto: </label>
                        <img id="profilePhoto" src="" class="img-thumbnail" alt="Clique aqui para escolher foto" style="width: 200px; height: 200px">
                    </div>
                    <input type="file" name="foto" id="foto" class="hidden"/>

                    <div class="form-group">
                        <label class="required" for="inputName">Nome</label>
                        <input
                                type="text" class="form-control"
                                name="nome" id="inputName"
                                value="{{old('nome')}}"/>
                    </div>

                    <div class="form-group">
                        <label for="inputDescription">Descrição</label>
                        <input
                                type="text" class="form-control"
                                name="descrição" id="inputDescription"
                                value="{{old('descrição')}}"/>
                    </div>

                    <div class="form-group">
                        <label class="required" for="inputStartDate">Data de Início</label>
                        <input
                                type="date" class="form-control"
                                name="data_de_início" id="inputStartDate"
                                value="{{old('data_de_início')}}"/>
                    </div>

                    <div class="form-group">
                        <label for="inputEndDate">Data de Fim</label>
                        <input
                                type="date" class="form-control"
                                name="data_de_fim" id="inputEndDate"
                                value="{{old('data_de_fim')}}"/>
                    </div>

                    <div class="form-group">
                        <label for="inputLocation">Localização</label>
                        <input
                                type="text" class="form-control"
                                name="localização" id="inputLocation"
                                value="{{old('localização')}}"/>
                    </div>

                    <div class="form-group">
                        <label for="inputProjectUrl">Url do projecto</label>
                        <input
                                type="text" class="form-control"
                                name="url_projecto" id="inputProjectUrl"
                                value="{{old('url_projecto')}}"/>
                    </div>

                    <hr>

                        <div class="form-group">
                            <label class="required" for="inputStatus">Estado do projecto</label>
                            <select name="estado_do_projecto" id="inputStatus" class="form-control">
                                @if(!old('estado_do_projecto'))
                                    <option value="0" selected>Pendente</option>
                                    <option value="1">A Decorrer</option>
                                    <option value="2">Concluído</option>
                                    <option value="3">Cancelado</option>
                                @else
                                    @if(old('estado_do_projecto') == 0)
                                        <option selected value="0">Pendente</option>
                                        <option value="1">A Decorrer</option>
                                        <option value="2">Concluído</option>
                                        <option value="3">Cancelado</option>
                                    @elseif(old('estado_do_projecto') == 1)
                                        <option value="0">Pendente</option>
                                        <option selected value="1">A Decorrer</option>
                                        <option value="2">Concluído</option>
                                        <option value="3">Cancelado</option>
                                    @elseif(old('estado_do_projecto') == 2)
                                        <option value="0">Pendente</option>
                                        <option value="1">A Decorrer</option>
                                        <option selected value="2">Concluído</option>
                                        <option value="3">Cancelado</option>
                                    @elseif(old('estado_do_projecto') == 3)
                                        <option value="0">Pendente</option>
                                        <option value="1">A Decorrer</option>
                                        <option value="2">Concluído</option>
                                        <option selected value="3">Cancelado</option>
                                    @endif
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="required" for="inputSubmitted">Estado da candidatura</label>
                            <select name="estado_da_candidatura" id="inputSubmitted" class="form-control">
                                @if(!old('estado_da_candidatura'))
                                    <option selected value="0">Não submetida</option>
                                    <option value="1">Submetida</option>
                                @else
                                    @if(old('estado_da_candidatura') == 0)
                                        <option selected value="0">Não submetida</option>
                                        <option value="1">Submetida</option>
                                    @else
                                        <option value="0">Não submetida</option>
                                        <option selected value="1">Submetida</option>
                                    @endif
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="required" for="inputTimesSubmitted">Número de candidaturas já efetuadas</label>
                            <input
                                    type="number" min="0" class="form-control"
                                    name="vezes_que_a_candidatura_foi_efectuada" id="inputTimesSubmitted"
                                    @if(old('vezes_que_a_candidatura_foi_efectuada'))
                                        value="{{old('vezes_que_a_candidatura_foi_efectuada')}}"/>
                                    @else
                                        value='0'/>
                                    @endif
                        </div>

                    <hr>

                        <div class="row">
                            <div class="col-sm-12">
                                <label for="inputManagers">Seleccionar Coordenadores:</label>
                                <table id="allManagers" class="display" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nome</th>
                                        <th>Tipo de Perfil</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>

                        </div>

                    <hr>

                    <input type="file" id="inputFile" value="" class="hidden" multiple>
                    <div class="form-group col-md-12">
                        <label for="inputAttachments">Adicionar anexos:</label>
                        <div id="fileInput" class="file-drop" style="text-align: center">
                            Clique ou largue aqui os seus ficheiros!
                        </div>
                    </div>

                    <hr>

                    <h4 class="panel-title">Anexos</h4>

                    <div class="table-responsive">
                        <div class="col-sm-12">
                            <table id="projectAttachments" class="display" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Criado a</th>
                                    <th>Inserido por</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="ok">Guardar</button>
                        <a class="btn btn-default" href="{{ route('home') }}">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var files = [];
        $(document).ready(function() {

            var tableAllManagers = $('#allManagers').DataTable( {
                "scrollY": '50vh',
                "scrollCollapse": true,
                "responsive": true,
                "iDisplayLength": 10,
                "select": {
                    "style": "multi"
                },
                "paging": false,
                "searching": false,
                "ajax": '/projects/fetchallmanagers/',
                "columns": [
                    { "data": "id" },
                    { "data": "name" },
                    { "data": function (data) {
                        switch (data.role)
                            {
                                case 1:
                                    return 'Diretor';
                                case 2:
                                    return 'Coordenador';
                                case 3:
                                    return 'Técnico do projecto';
                            }
                    } }
                ],
                "columnDefs": [ {
                    "orderable": false,
                    "className": 'select-checkbox',
                    "targets":   0
                } ]
            });

            var projectAttachments = $('#projectAttachments').DataTable({
                "paging": false,
                "searching": false,
                "data": files,
                "columns": [
                    { "data": function(data){if(data.filename){return data.original_filename}else{return data.name}}},
                    { "data": function(data){if(data.created_at){return data.created_at}else{return data.lastModified}}},
                    { "data": function(data){if(data.user_id){return data.user_id}else{return currentUser}}},
                    { "render": function (a, b, c, d) {
                        return "<a id='removeFile' class='btn btn-xs btn-danger'>Remover</a>";
                    }}
                ]
            });

            var fileInput = $('#fileInput');

            fileInput.on('dragenter', function (e) {
                e.preventDefault();
                e.stopPropagation();
            });
            fileInput.on('dragover', function (e) {
                e.preventDefault();
                e.stopPropagation();
                fileInput.addClass('file-drop-hover');
            });
            fileInput.on('dragleave', function (e) {
                e.preventDefault();
                e.stopPropagation();
                fileInput.removeClass('file-drop-hover');
            });
            fileInput.on('dragend', function (e) {
                e.preventDefault();
                e.stopPropagation();
                fileInput.removeClass('file-drop-hover');
            });
            fileInput.on('drop', function (e) {
                fileInput.removeClass('file-drop-hover');
                if(e.originalEvent.dataTransfer.files.length){
                    e.preventDefault();
                    e.stopPropagation();

                    $.each(e.originalEvent.dataTransfer.files, function(index, file){
                        if(_.findIndex(files, function(obj){ return obj.name == file.name; }) == -1)
                        {
                            files.push(file);
                        }
                    });
                    projectAttachments.clear();
                    $.each(files, function (index, fileToUpload) {
                        projectAttachments.row.add(fileToUpload);
                    });
                    projectAttachments.draw();
                }
            });

            fileInput.on('click', function () {
                $('#inputFile').click();
            });

            $('#inputFile').on('change', function (e) {
                e.preventDefault();
                var inputFiles = $(this).prop('files');
                if(inputFiles.length){

                    $.each(inputFiles, function(index, file){
                        if(_.findIndex(files, function(obj){ return obj.name == file.name; }) == -1)
                        {
                            files.push(file);
                        }
                    });
                    projectAttachments.clear();
                    $.each(files, function (index, fileToUpload) {
                        projectAttachments.row.add(fileToUpload);
                    });
                    projectAttachments.draw();
                }
            });

            projectAttachments.on('click', '#removeFile', function () {
                var row = projectAttachments.row($(this).parents('tr'));
                var file = row.data();
                _.remove(files, function (obj){
                    return obj == file;
                });
                projectAttachments.clear();
                $.each(files, function (index, fileToUpload) {
                    projectAttachments.row.add(fileToUpload);
                });
                projectAttachments.draw();
            });

            $('#projectForm').on('submit', function (e) {
                var formData = new FormData(this);
                formData.append('managers', JSON.stringify(tableAllManagers.rows({ selected: true }).data().toArray()));
                $.each(files, function (index, file) {
                   formData.append('anexos[' + file.name + ']', file);
                });
                e.preventDefault();
                    $.ajax({
                        type: 'post',
                        headers: {
                            'X-CSRF-Token': "{{csrf_token()}}"
                        },
                        processData: false,
                        contentType: false,
                        url: '/projects',
                        data: formData,
                        success: function (result) {
                            window.location.replace('/home');
                        },
                        error: function () {
                            window.location.reload();
                        }
                });
            });
        });

        var inputFoto = $('#foto');
        $('#fotoContainer').on('click', function () {
            inputFoto.click();
        });

        inputFoto.on('change', function () {
            var imageType = /image.*/;
            if (this.files && this.files[0] && this.files[0].type.match(imageType)) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#profilePhoto').attr('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }
        });
    </script>
@endsection
