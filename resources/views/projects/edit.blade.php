@extends('layouts.app')

@section('content')
    @if (count(Session::get('errors')) > 0)
        @include('partials.errors-ajax')
    @endif

    <style>
        .file-drop{
            background-color: white; outline: 3px dashed lightslategrey; box-shadow: gray; width: 100%; height: 100px
        }
        .file-drop-hover{
            background-color: lightpink;
            outline-color: darkred;
        }
    </style>

    <script>
        var currentUser = {{\Auth::user()->id}}
    </script>
    <div class="container" style="width: 85%">
        <div class="panel panel-default">
            <div class="panel-heading">

                <ol class="breadcrumb">
                    <li><a href ="{{ route('home') }}">Home</a></li>
                    <li>Projecto: <a href="/projects/{{$project->id}}">{{$project->name}}</a></li>
                    <li class="active">Editar detalhes do projecto</li>
                </ol>

                <h4 class="panel-title">Editar Detalhes do Projecto</h4>
            </div>

            <div class="panel-body">
                <form id="formProject" action="/projects/{{$project->id}}" method="post"  class="form-group" enctype="multipart/form-data">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}

                    <div class="form-group" id="fotoContainer">
                        <label for="profilePhoto" style="display: block">Foto: </label>
                        <img id="profilePhoto" src="{{$project->getFoto()}}" class="img-thumbnail" alt="Clique aqui para escolher foto" style="width: 200px; height: 200px">
                    </div>
                    <input type="file" name="foto" id="foto" class="hidden"/>

                    <div class="form-group">
                        <label class="required" for="inputName">Nome</label>
                        <input
                                type="text" class="form-control"
                                name="nome" id="inputName"
                                value="{{$project->name}}"/>
                    </div>

                    <div class="form-group">
                        <label for="inputDescription">Descrição</label>
                        <input
                                type="text" class="form-control"
                                name="descrição" id="inputDescription"
                                value="{{$project->description}}"/>
                    </div>

                    <div class="form-group">
                        <label class="required" for="inputStartDate">Data de Início</label>
                        <input
                                type="date" class="form-control"
                                name="data_de_início" id="inputStartDate"
                                value="{{$project->start_date}}"/>
                    </div>

                    <div class="form-group">
                        <label for="inputEndDate">Data de Fim</label>
                        <input
                                type="date" class="form-control"
                                name="data_de_fim" id="inputEndDate"
                                value="{{$project->end_date}}"/>
                    </div>

                    <div class="form-group">
                        <label for="inputLocation">Localização</label>
                        <input
                                type="text" class="form-control"
                                name="localização" id="inputLocation"
                                value="{{$project->location}}"/>
                    </div>

                    <div class="form-group">
                        <label for="inputProjectUrl">Url do projecto</label>
                        <input
                                type="text" class="form-control"
                                name="url_projecto" id="inputProjectUrl"
                                value="{{$project->project_url}}"/>
                    </div>

                    <hr>

                        @if(Auth::user()->isManager())

                            <div class="form-group">
                                <label class="required" for="inputStatus">Estado do projecto</label>
                                <select name="estado_do_projecto" id="inputStatus" class="form-control">
                                    @if($project->status == 0)
                                        <option selected value="0">Pendente</option>
                                        <option value="1">A Decorrer</option>
                                        <option value="2">Concluído</option>
                                        <option value="3">Cancelado</option>
                                    @elseif($project->status == 1)
                                        <option value="0">Pendente</option>
                                        <option selected value="1">A Decorrer</option>
                                        <option value="2">Concluído</option>
                                        <option value="3">Cancelado</option>
                                    @elseif($project->status == 2)
                                        <option value="0">Pendente</option>
                                        <option value="1">A Decorrer</option>
                                        <option selected value="2">Concluído</option>
                                        <option value="3">Cancelado</option>
                                    @elseif($project->status == 3)
                                        <option value="0">Pendente</option>
                                        <option value="1">A Decorrer</option>
                                        <option value="2">Concluído</option>
                                        <option selected value="3">Cancelado</option>
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="required" for="inputSubmitted">Estado da candidatura</label>
                                <select name="estado_da_candidatura" id="inputSubmitted" class="form-control">
                                    @if($project->submitted == 0)
                                        <option selected value="0">Não submetida</option>
                                        <option value="1">Submetida</option>
                                    @elseif($project->submitted == 1)
                                        <option value="0">Não submetida</option>
                                        <option selected value="1">Submetida</option>
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="required" for="inputTimesSubmitted">Número de candidaturas já efetuadas</label>
                                <input
                                        type="number" min="0" class="form-control"
                                        name="vezes_que_a_candidatura_foi_efectuada" id="inputTimesSubmitted"
                                        value="{{$project->times_submitted}}"/>
                            </div>

                        @endif

                    <hr>

                        <div class="row">
                            <h4 class="col-md-12">Coordenador(es)</h4>
                                <div class="col-sm-6">
                                    <table id="projectManagers" class="display" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nome</th>
                                            <th>Tipo de Perfil</th>
                                            <th>Ações</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            <div class="col-sm-6">
                                <table id="allManagers" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nome</th>
                                            <th>Tipo de Perfil</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                    <hr>

                    <div class="form-group col-md-12">
                        <label for="inputAttachments">Adicionar anexos:</label>
                        <div id="fileInput" class="file-drop" style="text-align: center">
                            Clique ou largue aqui os seus ficheiros!
                        </div>
                    </div>

                    <hr>

                    <input type="file" id="inputFile" value="" class="hidden" multiple>
                    <h4 class="panel-title">Anexos associados</h4>
                    <div class="table-responsive">
                        <div class="col-sm-12">
                            <table id="projectAttachments" class="display" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Criado a</th>
                                    <th>Inserido por</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="ok">Guardar</button>
                        <a class="btn btn-default" href="/projects/{{$project->id}}">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var files = [];
        var filesFromServer = [];
        var filesToDelete = [];
        var filesToAdd = [];

        $(document).ready(function() {

            $.get('/projects/fetchtabledata/{{$project->id}}', function (data) {

                if(data['attachments'].length >0){
                    files = data['attachments'];
                    filesFromServer = files;
                }

                var tableProjectManagers = $('#projectManagers').DataTable( {
                    "scrollY": '30vh',
                    "scrollCollapse": true,
                    "paging": false,
                    "searching": false,
                    "data": data['managers'],
                    "columns": [
                        { "data": "id" },
                        { "data": "name" },
                        { "data": function (data) {
                            switch (data.role)
                            {
                                case 1:
                                    return 'Diretor';
                                case 2:
                                    return 'Coordenador';
                                case 3:
                                    return 'Técnico do projecto';
                            }
                        } },
                        { "data": "columnDefs"}
                    ],
                    "columnDefs": [ {
                        "targets": -1,
                        "data": null,
                        "render": function ( data, type, full, meta ) {
                            return "<a id='removeManager' class='btn btn-sm btn-danger'>Remover</a><input class='form-group' name='managers["+ full['id'] +"]' type='hidden' value="+full['id']+">";
                        }
                    }]
                });
                var tableAllManagers = $('#allManagers').DataTable( {
                    "scrollY": '30vh',
                    "scrollCollapse": true,
                    "paging": false,
                    "searching": false,
                    "data": data['allManagers'],
                    "columns": [
                        { "data": "id" },
                        { "data": "name" },
                        { "data": function (data) {
                            switch (data.role)
                            {
                                case 1:
                                    return 'Diretor';
                                case 2:
                                    return 'Coordenador';
                                case 3:
                                    return 'Técnico do projecto';
                            }
                        } },
                        { "data": "columnDefs"}
                    ],
                    "columnDefs": [ {
                        "targets": -1,
                        "data": null,
                        "defaultContent": "<a id='addManager' class='btn btn-sm btn-success'>Adicionar</a>"
                    } ]
                });
                var projectAttachments = $('#projectAttachments').DataTable({
                    "paging": false,
                    "searching": false,
                    "responsive": true,
                    "iDisplayLength": 10,
                    "data": files,
                    "columns": [
                        { "data": function(data){if(data.filename){return data.original_filename}else{return data.name}}},
                        { "data": function(data){if(data.created_at){return data.created_at}else{return data.lastModified}}},
                        { "data": function(data){if(data.user_id){return data.user_id}else{return currentUser}}},
                        { "render": function (a, b, c, d) {
                            return "<a id='removeFile' class='btn btn-xs btn-danger'>Remover</a>";
                        }}
                    ]
                });

                $('#projectManagers tbody').on( 'click', '#removeManager', function () {
                    var data = tableProjectManagers.row( $(this).parents('tr') ).data();
                    tableProjectManagers.row($(this).parents('tr')).remove().draw();
                    tableAllManagers.row.add( data ).draw();

                });

                $('#allManagers tbody').on( 'click', '#addManager', function () {
                    var data = tableAllManagers.row( $(this).parents('tr') ).data();
                    tableAllManagers.row($(this).parents('tr')).remove().draw();
                    tableProjectManagers.row.add( data ).draw();
                });

                var fileInput = $('#fileInput');

                fileInput.on('dragenter', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                });
                fileInput.on('dragover', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    fileInput.addClass('file-drop-hover');
                });
                fileInput.on('dragleave', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    fileInput.removeClass('file-drop-hover');
                });
                fileInput.on('dragend', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    fileInput.removeClass('file-drop-hover');
                });
                fileInput.on('drop', function (e) {
                    fileInput.removeClass('file-drop-hover');
                    if(e.originalEvent.dataTransfer.files.length){
                        e.preventDefault();
                        e.stopPropagation();

                        $.each(e.originalEvent.dataTransfer.files, function(index, file){
                            if(_.findIndex(files, function(obj){if(_.has(obj, 'media_path')){
                                            return obj.original_filename == file.name;
                                            }
                                            return obj.name == file.name;
                                            }) == -1)
                            {
                                files.push(file);
                                filesToAdd.push(file);
                            }
                        });
                        projectAttachments.clear();
                        $.each(files, function (index, fileToUpload) {
                            projectAttachments.row.add(fileToUpload);
                        });
                        projectAttachments.draw();
                    }
                });

                fileInput.on('click', function () {
                    $('#inputFile').click();
                });

                $('#inputFile').on('change', function (e) {
                    e.preventDefault();
                    var inputFiles = $(this).prop('files');
                    if(inputFiles.length){

                        $.each(inputFiles, function(index, file){
                            if(_.findIndex(files, function(obj){if(_.has(obj, 'media_path')){
                                    return obj.original_filename == file.name;
                                }
                                    return obj.name == file.name;
                                }) == -1)
                            {
                                files.push(file);
                                filesToAdd.push(file);
                            }
                        });
                        projectAttachments.clear();
                        $.each(files, function (index, fileToUpload) {
                            projectAttachments.row.add(fileToUpload);
                        });
                        projectAttachments.draw();
                    }
                });

                projectAttachments.on('click', '#removeFile', function () {
                   var row = projectAttachments.row($(this).parents('tr'));
                   var file = row.data();
                   if(_.has(file, 'media_path')){
                       if(_.findIndex(filesToDelete, function(obj){return obj.filename == file.filename;}) == -1){
                           filesToDelete.push(file);
                       }
                   }
                   _.remove(files, function (obj){
                       return obj == file;
                   });
                    projectAttachments.clear();
                    $.each(files, function (index, fileToUpload) {
                        projectAttachments.row.add(fileToUpload);
                    });
                    projectAttachments.draw();
                });

                $('#formProject').on('submit', function (e) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    $.each(filesToAdd, function (index, file) {
                        formData.append('anexos['+ file.name +']', file);
                    });
                    $.each(filesToDelete, function (index, fileRemove) {
                        formData.append('anexosARemover['+ fileRemove.filename +']', JSON.stringify(fileRemove));
                    });
                    $.ajax({
                       type: "post",
                        headers: {
                            'X-CSRF-Token': "{{csrf_token()}}"
                        },
                        url: "/projects/{{$project->id}}",
                        processData: false,
                        contentType: false,
                        data: formData,
                        success: function(data, status){
                            window.location.replace('/projects/{{$project->id}}');
                        },
                        error: function () {
                            window.location.reload();
                        }
                    });
                });

            });
        });

        var inputFoto = $('#foto');
        $('#fotoContainer').on('click', function () {
            inputFoto.click();
        });

        inputFoto.on('change', function () {
            var imageType = /image.*/;
            if (this.files && this.files[0] && this.files[0].type.match(imageType)) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#profilePhoto').attr('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }
        });
    </script>
@endsection