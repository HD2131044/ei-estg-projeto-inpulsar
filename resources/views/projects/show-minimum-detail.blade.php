@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">

            <div class="panel-heading">

                <ol class="breadcrumb">
                    <li><a href ="{{ route('home') }}">Home</a></li>
                    <li class="active">Projecto: {{$project->name}}</li>
                </ol>

                <h4 class="panel-title">Detalhes do Projecto</h4>
            </div>

            <div class="panel-body">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                {{--                <div class="row">
                                    <div class="col-md-1">
                                        <h4><span class="label label-info">Foto: </span></h4>
                                    </div>
                                </div>--}}

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Criado por: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$project->projectCreatorName() }}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Nome: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$project->name }}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Descrição: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$project->description}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Data de Início: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$project->start_date}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Data de Fim: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        @if($project->end_date)
                            <h4>{{$project->end_date}}</h4>
                        @else
                            <h4>Por definir</h4>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Localização: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$project->location}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Url do projecto: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4><a href="{{$project->project_url}}">{{$project->project_url}}</a></h4>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Estado do projecto: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$project->statusToStr()}}</h4>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-md-3">
                        <a class="btn btn-block btn-default " href="{{ URL::previous() }}">Voltar</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection