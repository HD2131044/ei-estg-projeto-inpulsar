@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">

            <div class="panel-heading">

                <ol class="breadcrumb">
                    <li><a href ="{{ route('home') }}">Home</a></li>
                    <li class="active">Projecto: {{$project->name}}</li>
                </ol>

                <h3 class="panel-title">Detalhes do Projecto</h3>
                    <div class="col-sm-3 pull-right">
                        @if(!$project->isClosed() && !$project->isCancelled())
                            <form action="/projects/{{$project->id}}/activities/create" method="get" class="form-inline">
                                <button type="submit" class="btn btn-block btn-primary">Adicionar Actividade</button>
                            </form>
                        @endif

                        @if(Auth::user()->isPublisher() && $project->canBePostedInFacebook())
                            <button type="button" class="btn btn-block btn-success" value="facebook" data-toggle="modal" data-target="#shareModal">
                                Publicar no Facebook</button>
                        @endif
                    </div>
            </div>

            <div class="panel-body">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="form-group" id="fotoContainer">
                    <h4><span class="label label-info">Foto: </span></h4>
                    <img id="projectPhoto" src="{{$project->getFoto()}}" class="img-thumbnail" alt="Sem foto" style="width: 200px; height: 200px">
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Criado por: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$project->projectCreatorName() }}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Nome: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$project->name }}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Descrição: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$project->description}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Data de Início: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$project->start_date}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Data de Fim: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        @if($project->end_date)
                            <h4>{{$project->end_date}}</h4>
                        @else
                            <h4>Por definir</h4>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Localização: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$project->location}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Url do projecto: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4><a href="{{$project->project_url}}">{{$project->project_url}}</a></h4>
                    </div>
                </div>

                <hr>

                <h4 class="panel-title">Utilizadores associados</h4>

                <div class="table-responsive">
                    @if($project->users()->count() > 0)
                        <table class="table table-hover">

                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nome</th>
                                <th>Tipo de Perfil</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($project->getAllUsers() as $user)

                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name}}</td>
                                    <td>{{ $user->roleToStr() }}</td>
                                </tr>

                            @endforeach
                        </table>

                        <div class="row" align="center">
                            {{$project->getAllUsers()->appends(Request::except('page'))->links()}}
                        </div>
                    @else
                        <h4>Não foram encontrados utilizadores</h4>
                    @endif

                </div>

                <hr>

                @include('activities.show-list')

                <hr>

                @include('posts.show-list-in-projects')

                <hr>

                @include('attachments.show-list-in-projects')

                <hr>

                @include('gallery.gallery')

                <hr>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Estado do projecto: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$project->statusToStr()}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4><span class="label label-info">Estado da candidatura: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4>{{$project->submittedToStr()}}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <h4><span class="label label-info">Número de candidaturas já efetuadas: </span></h4>
                    </div>
                    <div class="col-md-4 pull-left">
                        <h4> {{$project->times_submitted}} </h4>
                    </div>
                </div>

                <hr>

                <div class="row">

                    @include('projects.options-show')

                </div>

            </div>
        </div>
    </div>

    @include('modals.image-preview')

    @if(Auth::user()->isPublisher() && $project->canBePostedInFacebook())

        @include('modals.share')

        <script>
            $('#shareModal').on('show.bs.modal', function () {
                $('#fbText').val('{{$project->description}}');
            });
        </script>

    @endif
@endsection