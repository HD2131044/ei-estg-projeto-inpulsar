@if(Auth::user()->isManager())
    @if(!$project->isClosed())
        <div class="btn-group">
            <button id="toggleState{{$project->id}}" type="button" class="btn btn-xs btn-block btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Estados <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu" style="right: 4px;left: auto;">
                @if($project->isPending() && !$project->isSubmitted())
                    <li>
                        <a id="btn-submit-{{$project->id}}" style="cursor: pointer;">Submeter</a>
                    </li>
                @elseif($project->isPending() && $project->isSubmitted())
                    <li>
                        <a id="btn-running-{{$project->id}}" style="cursor: pointer;">Decorrer</a>
                    </li>
                    <li>
                        <a id="btn-cancel-{{$project->id}}" style="cursor: pointer;">Cancelar</a>
                    </li>
                @elseif($project->isRunning())
                    <li>
                        <a id="btn-close-{{$project->id}}" style="cursor: pointer;">Concluir</a>
                    </li>
                @elseif($project->isCancelled())
                    <li>
                        <a id="btn-pending-{{$project->id}}" style="cursor: pointer;">Resubmeter</a>
                    </li>
                @endif
            </ul>
        </div>
    @endif
@endif

<!-- Modal -->
<div class="modal fade" tabindex="-1" id="modal-submit-{{$project->id}}"> role="dialog">
    <div class="modal-dialog modal-lg">
        {{$action_button = "submeter"}}
        @include('modals.state-project', compact('action_button'))
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#btn-submit-{{$project->id}}").click(function(){
            $("#modal-submit-{{$project->id}}").modal();
        });
    });
</script>

<div class="modal fade" tabindex="-1" id="modal-running-{{$project->id}}"> role="dialog">
    <div class="modal-dialog modal-lg">
        {{$action_button = "decorrer"}}
        @include('modals.state-project', compact('action_button'))
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#btn-running-{{$project->id}}").click(function(){
            $("#modal-running-{{$project->id}}").modal();
        });
    });
</script>

<div class="modal fade" tabindex="-1" id="modal-cancel-{{$project->id}}"> role="dialog">
    <div class="modal-dialog modal-lg">
        {{$action_button = "cancelar"}}
        @include('modals.state-project', compact('action_button'))
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#btn-cancel-{{$project->id}}").click(function(){
            $("#modal-cancel-{{$project->id}}").modal();
        });
    });
</script>


<div class="modal fade" tabindex="-1" id="modal-close-{{$project->id}}"> role="dialog">
    <div class="modal-dialog modal-lg">
        {{$action_button = "concluir"}}
        @include('modals.state-project', compact('action_button'))
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#btn-close-{{$project->id}}").click(function(){
            $("#modal-close-{{$project->id}}").modal();
        });
    });
</script>


<div class="modal fade" tabindex="-1" id="modal-pending-{{$project->id}}"> role="dialog">
    <div class="modal-dialog modal-lg">
        {{$action_button = "resubmeter"}}
        @include('modals.state-project', compact('action_button'))
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#btn-pending-{{$project->id}}").click(function(){
            $("#modal-pending-{{$project->id}}").modal();
        });
    });
</script>
