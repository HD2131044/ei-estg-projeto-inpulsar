<style>
    .portfolio .thumbnail {
        position: relative;
    }

    .portfolio .thumbnail:hover {
        cursor: pointer;
    }

    .portfolio .caption {
        bottom: 0;
        position: absolute;
    }

    .portfolio .btn {
        opacity: 0.75;
    }
</style>

<div class="well">
    @if(Auth::user()->isManager())
        <h4 class="panel-title">Galeria das imagens (activas)</h4>
    @else
        <h4 class="panel-title">Galeria das imagens</h4>
    @endif
    <div class="panel-body row portfolio" style="padding-top: 60px">
        @if($galleryImages->count() > 0)
            @foreach($galleryImages as $image)
                @if($image->isActive())
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail" onclick="previewGalleryImage(this, '{{$image['original_filename']}}')" style="border: solid #bfbfbf 1px">
                            <img class="img-responsive" src="data:image/png;base64,{{base64_encode(file_get_contents($image['media_path'] . '/' . $image['filename']))}}" style="max-width: 194px; height: 194px">
                        </div>
                    </div>
                @endif
            @endforeach
        @else
            <h4>Não foram encontradas imagens</h4>
        @endif
    </div>
</div>

<script>
    var previewGalleryImage = function (elem, filename) {
        imageModal = $('#imagePreviewModal');
        imageModal.find('#imageName').html(filename);
        imageModal.find('#imagePreview').attr('src', $(elem).find('img').attr('src'));
        imageModal.modal().toggle();
    }
</script>
