<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Activity;
use App\Attachment;
use App\Log;
use App\Post;
use App\Project;
use App\User;

$factory->define(User::class, function (Faker\Generator $faker) {
    static $password;

    //nome por género
    $genero = $faker->randomElement($array = array('M', 'F'));
    $sobrenome = $faker->lastName;
    if ($genero == 'M'){
        $nome = $faker->firstNameMale." ".$sobrenome;
    } else {
        $nome = $faker->firstNameFemale." ".$sobrenome;
    }

    //profile photo
    $fakeProfilePhotoDirectory = storage_path('app'). "/public/faker-attachments/photo_profile";
    $destinationProfilePhotoPath = storage_path('app').'/users';
    $file_data = $faker->file($sourceDir = $fakeProfilePhotoDirectory, $targetDir = $destinationProfilePhotoPath);
    $profile_photo = $file_data;

    return [
        'name' => $nome,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        //birth_date para ser maior de idade
        'birth_date' => $faker->dateTimeBetween($startDate = '-60 years', $endDate = '-18 years'),
        'gender' => $genero,
        'address' => $faker->address,
        'profile_photo' =>  $faker->randomElement($array = array('', $profile_photo)),
        'profile_url' => $faker->randomElement($array = array('', $faker->url)),
        'description' => $faker->randomElement($array = array('', $faker->text($maxNbChars = 200))),
        'admin' => 0,
        'role' => $faker->numberBetween(1, 3),
        'activated'=> 0
    ];
});

$factory->define(Project::class, function (Faker\Generator $faker) {

    //considerando que um diretor é que é criador de um projeto
    //considerando as nossas seed para Diretores => id's = [3,...,6]
    $creatorId = $faker->numberBetween(3, 6);

    //datas de inicio e de fim
    $initial_Date = $faker->dateTimeBetween($startDate = '-1 years', $endDate = '1 years');
    $finish_Date = $faker->dateTimeInInterval($startDate = '0 years', $interval = '+ 1 years');
    while ($initial_Date >= $finish_Date)
    {
        $finish_Date = $faker->dateTimeInInterval($startDate = '0 years', $interval = '+ 1 years');
    }

    return [
        'creator_id' => $creatorId,
        'name' => $faker->sentence($nbWords = 5, $variableNbWords = true),
        'description' => $faker->randomElement($array = array('', $faker->text($maxNbChars = 200))),
        'start_date' => $initial_Date,
        'end_date' => $faker->randomElement($array = array(null, $finish_Date)),
        'location' => $faker->randomElement($array = array('', $faker->address)),
        'project_url' => $faker->randomElement($array = array('', $faker->url)),
        'status' => $faker->numberBetween(0, 3)
    ];
});

$factory->define(Activity::class, function (Faker\Generator $faker) {

    //considerando que um gestor é que é criador de uma actividade
    //considerando as nossas seed para Gestores => id's = [7,..,10]
    $creatorId = $faker->numberBetween(7, 10);

    //o attachement pertence sempre a um project
    //considerando as nossas seed para projects => id's = [1,...,15]
    $projectId = $faker->numberBetween(1, 15);

    return [
        'creator_id' => $creatorId,
        'project_id' => $projectId,
        'name' => $faker->sentence($nbWords = 5, $variableNbWords = true),
        'description' => $faker->randomElement($array = array('', $faker->text($maxNbChars = 200))),
        'start_date' => $faker->dateTimeBetween($startDate = '0 years', $endDate = '1 years'),
        'end_date' => null,
        'location' => $faker->randomElement($array = array('', $faker->address)),
        'activity_url' => $faker->randomElement($array = array('', $faker->url)),
        'status' => $faker->numberBetween(0, 2)
    ];
});


$factory->define(Post::class, function (Faker\Generator $faker) {

    //considerando qualquer utilizador, que não administrador, é criador de um post
    //considerando as nossas seed para users não admnistradores => id's = [3,...,15]
    $ownerId = $faker->numberBetween(3, 15);

    //um post pertence sempre a uma activity
    //considerando as nossas seed para activities => id's = [1,...,15]
    $activityId = $faker->numberBetween(1, 15);

    return [
        'user_id' => $ownerId,
        'activity_id' => $activityId,
        'title' => $faker->sentence($nbWords = 5, $variableNbWords = true),
        'text' => $faker->randomElement($array = array('', $faker->realText($maxNbChars = 200, $indexSize = 2))),
        'date' => $faker->dateTimeBetween($startDate = '0 years', $endDate = '1 years'),
        'location' => $faker->randomElement($array = array('', $faker->address)),
        'post_url' => $faker->randomElement($array = array('', $faker->url)),
        'number_participants' =>$faker->numberBetween(0,20)
    ];
});

$factory->define(Attachment::class, function (Faker\Generator $faker) {

    //considerando qualquer utilizador, que não administrador, é criador de um attchment
    //considerando as nossas seed para users não admnistradores => id's = [3,...,15]
    $ownerId = $faker->numberBetween(3, 15);

    //o attachement pertence sempre a um project
    //considerando as nossas seed para projects => id's = [1,...,15]
    $projectId = $faker->numberBetween(1, 15);

    //o attachement pode pertencer a uma activivity
    //considerando as nossas seed para activities => id's = [1,...,15]
    $activityId = $faker->numberBetween(1, 15);

    //o attachement pode pertencer a um post
    //considerando as nossas seed para posts => id's = [1,...,15]
    $postId = $faker->numberBetween(1, 15);

    //preparar ficheiros
    $fakeDirectory = storage_path('app'). "/public/faker-attachments";
    $destinationPath = storage_path('app').'/project'.$projectId;
    $file_data = $faker->file($sourceDir = $fakeDirectory, $targetDir = $destinationPath, false);
    $originalFileName = $file_data;
    $splitName = explode('.', $originalFileName, 2);
    $extension = $splitName[1];
    //$file_name = $faker->word.'.'.$extension;
    $file_name = $originalFileName; //na factory teve de repetir-se o nome

    if($extension == 'docx'){
        $mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    }elseif($extension == 'xlsx'){
        $mimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    }elseif($extension == 'pdf'){
        $mimeType = 'application/pdf';
    }elseif($extension == 'txt'){
        $mimeType = 'text/plain';
    }elseif($extension == 'jpg' || $extension == 'png'){
        $mimeType = 'image/png';
    }elseif($extension == 'jpeg'){
        $mimeType = 'image/jpeg';
    }elseif($extension == 'mp4'){
        $mimeType = 'video/mp4';
    }

    return [
        'user_id' => $ownerId ,
        'project_id' => $projectId,
        'activity_id' => $faker->randomElement($array = array(null, $activityId)),
        'post_id' => $faker->randomElement($array = array(null, $postId)),
        'filename' => $file_name,
        'original_filename' => $originalFileName,
        'size' => $faker->numberBetween($min = 8, $max = 1890423),
        'mime_type' => $mimeType,
        'media_path' => $destinationPath,
        'date' => $faker->dateTimeBetween($startDate = '0 years', $endDate = '1 years')
    ];
});

$factory->define(Log::class, function (Faker\Generator $faker) {

    //considerando qualquer utilizador, que não administrador, é criador de um log
    //considerando as nossas seed para users não admnistradores => id's = [3,...,15]
    $ownerId = $faker->numberBetween(3, 15);

    //considerando qualquer utilizador, que não administrador, é criador de um log
    //considerar uma série de logs que pertencem a posts
    //considerando as nossas seed para projects, activities e posts => id's = [1,...,15]
    $projectId = $faker->numberBetween(1, 15);
    $activityId = $faker->numberBetween(1, 15);
    $postId = $faker->numberBetween(1, 15);

    return [
        'entry_by' => $ownerId,
        'project_id' => $projectId,
        'activity_id' => $activityId,
        'post_id' => $postId,
        'action' => $faker->randomElement($array = array('criação', 'edição', 'eliminação', 'mudança de estado')),
        'entry_text' => $faker->firstNameMale." ".$faker->lastName.' '.$faker->randomElement($array = array('criação', 'edição', 'eliminação', 'mudança de estado')).', referente a qualquer coisa XPTO, em '.$faker->dateTimeBetween($startDate = '0 years', $endDate = '1 years')->format('Y-m-d H:i:s').'.',
        'date' => $faker->dateTimeBetween($startDate = '0 years', $endDate = '1 years')
    ];
});
