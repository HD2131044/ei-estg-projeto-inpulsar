<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->date('birth_date');
            $table->string('gender',1);
            $table->string('address',100);
            $table->string('profile_photo')->nullable();
            $table->string('profile_url')->nullable();
            $table->string('description', 200)->nullable();
            $table->boolean('admin')->default(false);
            $table->tinyInteger('role');
            $table->boolean('publisher')->default(false);
            $table->boolean('blocked')->default(false);
            $table->boolean('activated')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
