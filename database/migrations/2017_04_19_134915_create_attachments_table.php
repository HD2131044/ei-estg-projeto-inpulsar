<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->integer('activity_id')->unsigned()->nullable();
            $table->integer('post_id')->unsigned()->nullable();
            $table->string('filename');
            $table->string('original_filename');
            $table->integer('size');
            $table->string('mime_type');
            $table->string('media_path');
            $table->date('date');
            $table->timestamps();
            $table->softDeletes();
            //chaves estrangeiras
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); //Softdelete dos users evita a eliminação de anexos de users
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('activity_id')->references('id')->on('activities')->onDelete('cascade');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
