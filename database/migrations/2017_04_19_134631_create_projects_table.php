<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creator_id')->unsigned();
            $table->string('name');
            $table->string('description', 200)->nullable();
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->string('location',100)->nullable();
            $table->string('project_photo', 255)->nullable();
            $table->string('project_url')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->boolean('submitted')->default(false);
            $table->integer('times_submitted')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
