<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creator_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->string('name');
            $table->string('description', 200)->nullable();
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->string('location',100)->nullable();
            $table->string('activity_photo', 255)->nullable();
            $table->string('activity_url')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('last_status')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //chaves estrangeiras
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
