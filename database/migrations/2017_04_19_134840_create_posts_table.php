<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('activity_id')->unsigned();
            $table->string('title');
            $table->text('text')->nullable();
            $table->date('date');
            $table->string('location',100)->nullable();
            $table->string('post_url')->nullable();
            $table->integer('number_participants');
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->softDeletes();
            //chaves estrangeiras
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); //Softdelete dos users evita a eliminação de posts de users
            $table->foreign('activity_id')->references('id')->on('activities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
