<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Administradores = 2 => id's = [1,2]

        factory(User::class)->create([
            'name' => "Gestor Conta Um",
            'email' => "admin1@mail.pt",
            'password' => password_hash('admin123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'admin' => '1',
            'role' => '0',
            'activated' => '1'
        ]);

        factory(User::class)->create([
            'name' => "Gestor Conta Dois",
            'email' => "admin2@mail.pt",
            'password' => password_hash('admin123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'admin' => '1',
            'role' => '0',
            'activated' => '1'
        ]);

        //Diretores = 1+3 => id's = [3,4,5,6]

        factory(User::class)->create([
            'name' => "Diretor",
            'email' => "diretor@mail.pt",
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '1',
            'publisher' => '1',
            'activated' => '1'
        ]);

        factory(User::class, 3)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '1',
            'activated' => '1'
        ]);

        //Gestores = 1+3 => id's = [7,8,9,10]

        factory(User::class)->create([
            'name' => "Coordenador",
            'email' => "gestor@mail.pt",
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '2',
            'publisher' => '1',
            'activated' => '1'
        ]);

        factory(User::class, 3)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '2',
            'activated' => '1'
        ]);

        //Utilizadores = 1+3 => id's = [11,12,13,14,15]

        factory(User::class)->create([
            'name' => "Técnico Projecto",
            'email' => "utilizador@mail.pt",
            'password' => password_hash('secret', PASSWORD_DEFAULT),
            'gender' => 'M',
            'role' => '3',
            'activated' => '1'
        ]);

        factory(User::class, 4)->create([
            'password' => password_hash('123123123', PASSWORD_DEFAULT),
            'role' => '3',
            'activated' => '1'
        ]);

    }
}
