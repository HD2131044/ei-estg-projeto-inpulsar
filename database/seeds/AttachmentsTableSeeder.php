<?php

use App\Activity;
use App\Attachment;
use App\Post;
use App\Project;
use App\User;
use Illuminate\Database\Seeder;

class AttachmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Attachment::class, 5)
            ->create()
            ->each(function($attachment) {
                //considerando que um attachment está sempre associado a um project (SEM ESTAR ASSOCIADO A ACTIVIDADE NEM POST)
                //considerando as nossas seed para Projects => id's = [1,..,15]
                $projectData = Project::all()->random();
                $attachment->project_id = $projectData->id;
                $attachment->activity_id = null;
                $attachment->post_id = null;

                //o user que insere um attachment tem de pertencer à colecção project()->users() [Gestores] ou ser um director
                //o user pode ser qq director
                $user_director = User::all()->where('role', '=', '1')->random()->id;

                //o user poder ser um project manager se existirem
                if($projectData->users()->where('role', '<', '3')->count() > 1){
                    $user_manager = $projectData->users()->get()->where('role', '<', '3')->random()->id;
                    $users = [$user_director, $user_manager];
                }else{
                    $users = [$user_director];
                }
                $attachment->user_id = $users[array_rand($users)];
                $attachment->save();
            });

        factory(Attachment::class, 5)
            ->create()
            ->each(function($attachment) {
                //considerando que um attachment está sempre associado a um project em estado de candidatura não submetida (SEM ESTAR ASSOCIADO A ACTIVIDADE NEM POST)
                //considerando as nossas seed para Projects => id's = [1,..,15]
                $projectData = Project::all()->where('submitted', '=', false)->random();
                $attachment->project_id = $projectData->id;
                $attachment->activity_id = null;
                $attachment->post_id = null;

                //o user que insere um attachment tem de pertencer à colecção project()->users() [Gestores] ou ser um director
                //o user pode ser qq director
                $user_director = User::all()->where('role', '=', '1')->random()->id;

                //o user poder ser um project manager se existirem
                if($projectData->users()->where('role', '<', '3')->count() > 1){
                    $user_manager = $projectData->users()->get()->where('role', '<', '3')->random()->id;
                    $users = [$user_director, $user_manager];
                }else{
                    $users = [$user_director];
                }
                $attachment->user_id = $users[array_rand($users)];
                $attachment->save();
            });

        factory(Attachment::class, 5)
            ->create()
            ->each(function($attachment) {
                //considerando que um attachment está sempre associado a um project e a uma actividade (SEM ESTAR ASSOCIADO A POST)
                //considerando as nossas seed para Activities => id's = [1,..,15]
                $activityData = Activity::all()->random();
                while($activityData->id > 15){
                    $activityData = Activity::all()->random();
                }
                $attachment->project_id = $activityData->project_id;
                $attachment->activity_id = $activityData->id;
                $attachment->post_id = null;

                //o user que insere um attachment tem de pertencer à colecção project()->users() [Gestores], activity->users() [Utilizadores] ou ser um director
                //as nossas seeds para activities têm sempre dois utilizaddores associados logo a coleção project->users() nunca está vazia
                $projectData = Project::find($activityData->project_id);

                //o user pode ser qq director ou manager
                $user_director = User::all()->where('role', '=', '1')->random()->id;
                if($projectData->users()->where('role', '<', '3')->count()){
                    $user_manager = $projectData->users()->get()->where('role', '<', '3')->random()->id;
                    $users = [$user_director, $user_manager];
                }else{
                    $users = [$user_director];
                }
                $attachment->user_id = $users[array_rand($users)];
                $attachment->save();
            });

        factory(Attachment::class, 5)
            ->create()
            ->each(function($attachment) {
                //considerando que um attachment está sempre associado a um project, a uma actividade e a um post
                //considerando as nossas seed para Activities => id's = [1,..,15]
                $postData = Post::all()->random();
                $activityDataId = $postData->activity_id;
                while($postData->activity_id > 15){
                    $postData = Post::all()->random();
                    $activityDataId = $postData->activity_id;
                }
                $activityData = Activity::find($activityDataId);
                $attachment->project_id = $activityData->project_id;
                $attachment->activity_id = $activityDataId;
                $attachment->post_id = $postData->id;

                //o user que insere um attachment tem de pertencer à colecção project()->users() [Gestores], activity->users() [Utilizadores] ou ser um director
                //as nossas seeds para activities têm sempre dois utilizaddores associados logo a coleção project->users() nunca está vazia
                $projectData = Project::find($activityData->project_id);

                //o user pode ser qq director
                $user_director = User::all()->where('role', '=', '1')->random()->id;
                if($activityData->users()->where('role', '=', '3')->count()){
                    $user_from_activity = $activityData->users()->get()->where('role', '=', '3')->random()->id;
                    if($projectData->users()->where('role', '<', '3')->count()){
                        $user_manager = $projectData->users()->get()->where('role', '<', '3')->random()->id;
                        $users = [$user_from_activity, $user_director, $user_manager];
                    }else{
                        $users = [$user_from_activity, $user_director];
                    }
                }elseif($projectData->users()->where('role', '<', '3')->count()){
                    $user_manager = $projectData->users()->get()->where('role', '<', '3')->random()->id;
                    $users = [$user_director, $user_manager];
                }else{
                    $users = [$user_director];
                }

                $attachment->user_id = $users[array_rand($users)];
                $attachment->save();
            });

    }
}
