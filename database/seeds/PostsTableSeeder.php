<?php

use App\Activity;
use App\Post;
use App\Project;
use App\User;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Post::class, 15)
            ->create()
            ->each(function($post) {
                //considerando que um post está só associado a uma activity
                //considerando as nossas seed para Activities => id's = [1,..,15]
                $activityData = Activity::all()->random();
                while($activityData->id > 15){
                    $activityData = Activity::all()->random();
                }
                $post->activity_id = $activityData->id;

                //o user que faz post tem de pertencer à colecção activity()->users()
                //as nossas seeds para activities têm sempre dois utilizaddores associados logo a coleção project->users() nunca está vazia
                $projectData = Project::find($activityData->project_id);
                //o user pode ser qq director
                $user_director = User::all()->where('role', '=', '1')->random()->id;
                if($activityData->users()->where('role', '=', '3')->count()){
                    $user_from_activity = $activityData->users()->get()->where('role', '=', '3')->random()->id;
                    if($projectData->users()->where('role', '<', '3')->count()){
                        $user_manager = $projectData->users()->get()->where('role', '<', '3')->random()->id;
                        $users = [$user_from_activity, $user_director, $user_manager];
                    }else{
                        $users = [$user_from_activity, $user_director];
                    }
                }elseif($projectData->users()->where('role', '<', '3')->count()){
                    $user_manager = $projectData->users()->get()->where('role', '<', '3')->random()->id;
                    $users = [$user_director, $user_manager];
                }else{
                    $users = [$user_director];
                }
                $post->user_id = $users[array_rand($users)];
                $post->save();
            });
        
    }
}
