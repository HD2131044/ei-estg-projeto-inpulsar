<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(ActivitiesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(AttachmentsTableSeeder::class);
        $this->call(LogsTableSeeder::class);
    }
}
