<?php

use App\Activity;
use App\Log;
use App\Project;
use App\User;
use Illuminate\Database\Seeder;

class LogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Log::class, 5)
            ->create()
            ->each(function($log) {
                //considerando que um log está associado ao crud de um utilizador
                //os user do tipo administrador considerando as nossa seeds => id's = [1,2]
                $log->entry_by = User::all()->where('role', '=', '0')->random()->id;
                $user_ownerData = User::find($log->entry_by);

                ///considerando as nossas seed para restantes utilizadores => id's = [3,..,40]
                $log->user_id = User::all()->where('role', '>', '0')->random()->id;
                $userData = User::find($log->user_id);

                $log->project_id = null;
                $log->activity_id = null;
                $log->post_id = null;
                $log->attachment_id = null;

                //acções possíveis
                $array_actions = array('user_create', 'user_edit', 'user_block', 'user_unblock', 'user_reset_password', 'user_send_activation_mail');
                $log->action = 'faker_'.$array_actions[array_rand($array_actions)];
                $log->entry_text = $log->date->format('Y-m-d H:i:s').' - Faker Log - '.$user_ownerData->name.' criou um log de gestão da conta do utilizador '.$userData->name.' com uma acção na aplicação.';

                $log->save();
            });

        //para ter dados referente ao utilizador id = 3
        factory(Log::class, 15)
            ->create()
            ->each(function($log) {
                //considerando que um log está associado ao crud de um utilizador
                //os user do tipo administrador considerando as nossa seeds => id's = [1,2]
                $log->entry_by = User::all()->where('role', '=', '0')->random()->id;
                $user_ownerData = User::find($log->entry_by);

                ///utilizador id = 3
                $log->user_id = 3;
                $userData = User::find($log->user_id);

                $log->project_id = null;
                $log->activity_id = null;
                $log->post_id = null;
                $log->attachment_id = null;

                //acções possíveis
                $array_actions = array('user_create', 'user_edit', 'user_block', 'user_unblock', 'user_reset_password', 'user_send_activation_mail');
                $log->action = 'faker_'.$array_actions[array_rand($array_actions)];
                $log->entry_text = $log->date->format('Y-m-d H:i:s').' - Faker Log - '.$user_ownerData->name.' criou um log de gestão da conta do utilizador '.$userData->name.' com uma acção na aplicação.';

                $log->save();
            });

        //para ter dados referente ao projecto id = 1
        factory(Log::class, 5)
            ->create()
            ->each(function($log) {
                //considerando que um log está associado ao crud do projecto
                //os user do tipo não administrador considerando as nossa seeds => id's = [3,...,40]
                $project = Project::find(1);
                if($project->users()->count()){
                    $project->users()->get()->random()->id;
                }else{
                    $log->entry_by = User::all()->where('role', '=', '1')->random()->id;
                }
                $user_ownerData = User::find($log->entry_by);

                $log->user_id = null;
                $log->project_id = $project->id;
                $log->activity_id = null;
                $log->post_id = null;
                $log->attachment_id = null;

                //acções possíveis
                $array_actions = array('project_create', 'project_edit', 'project_cancel', 'project_running', 'project_close', 'project_pending');
                $log->action = 'faker_'.$array_actions[array_rand($array_actions)];
                $log->entry_text = $log->date->format('Y-m-d H:i:s').' - Faker Log - '.$user_ownerData->name.' criou um log do projecto '.$project->name.' com uma acção na aplicação.';

                $log->save();
            });

        //para ter dados referente à actvividade id = 1
        factory(Log::class, 5)
            ->create()
            ->each(function($log) {
                //considerando que um log está associado ao crud da actividade
                //os user do tipo não administrador considerando as nossa seeds => id's = [3,...,40]
                $activity = Activity::find(1);
                if($activity->users()->count()){
                    $activity->users()->get()->random()->id;
                }else{
                    $log->entry_by = User::all()->where('role', '=', '1')->random()->id;
                }
                $log->entry_by = User::all()->where('role', '>', '0')->random()->id;
                $user_ownerData = User::find($log->entry_by);

                $log->user_id = null;
                $log->project_id = $activity->project_id;
                $log->activity_id = $activity->id;
                $log->post_id = null;
                $log->attachment_id = null;

                //acções possíveis
                $array_actions = array('activity_create', 'activity_edit', 'activity_running', 'activity_close');
                $log->action = 'faker_'.$array_actions[array_rand($array_actions)];
                $log->entry_text = $log->date->format('Y-m-d H:i:s').' - Faker Log - '.$user_ownerData->name.' criou um log da actividade '.$activity->name.' com uma acção na aplicação.';

                $log->save();
            });
    }
}
