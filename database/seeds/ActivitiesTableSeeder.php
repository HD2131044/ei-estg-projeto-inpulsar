<?php

use App\Activity;
use App\Project;
use App\User;
use Illuminate\Database\Seeder;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Activity::class, 10)
            ->create()
            ->each(function($activity) {
                //considerando que uma activity está só associada a um project
                //considerando as nossas seed para Projects => id's = [1,..,15]
                $projectData = Project::all()->random();
                while($projectData->status > 2 || !$projectData->submitted){
                    $projectData = Project::all()->random();
                }
                $activity->project_id = $projectData->id;

                //se tem manager associado escolhe um manager da lista de user se não escolhe um Diretor random
                if($projectData->users()->where('role', '<', '3')->count() > 1){
                    $activity->creator_id = $projectData->users()->get()->where('role', '<', '3')->random()->id;
                }else{
                    $activity->creator_id = User::all()->where('role', '=', '1')->random()->id;
                }

                //verificar se o projecto já foi concluído e simular que guardou um last_status na actividade
                if($projectData->status == 2){
                    $activity->last_status = $activity->status;
                    $activity->status = 2;
                }

                $activity->save();

                //ANULADO -> activity->users() só tem users do tipo utilizador
                //activity pode ter vários users associados
                //associar o user do tipo Director/Manager que criou a activity
                //$activity->users()->attach($activity->creator_id);

                //activity->users() só tem users do tipo utilizador
                //considerando que todos os Gestores do project tb estão associados à activity
                if($projectData->users()->count()){
                    foreach ($projectData->users()->get()->where('role', '<', '3') as $user) {
                        if(!$activity->users()->find($user->id)){
                            $activity->users()->attach($user->id);
                        }
                    }
                }

                //considerando as nossas seed para Advanced + Simple users => id's = [11,..,15]
                //projecto com mais 2 participantes além dos gestores
                //associar utilizadores á activity
                $participant1Id = User::all()->where('role', '=', '3')->random()->id;
                $participant2Id = User::all()->where('role', '=', '3')->random()->id;
                while($participant2Id == $participant1Id){
                    $participant2Id = User::all()->where('role', '=', '3')->random()->id;
                }

                //pode sempre adicionar
                $activity->users()->attach($participant1Id);
                $activity->users()->attach($participant2Id);


                //associar utilizadores ao project
                if($projectData->users()->count() > 1){
                    if(!$projectData->users()->find($participant1Id)){
                        $projectData->users()->attach($participant1Id);
                    }
                    if(!$projectData->users()->find($participant2Id)){
                        $projectData->users()->attach($participant2Id);
                    }
                } else{
                    $projectData->users()->attach($participant1Id);
                    $projectData->users()->attach($participant2Id);
                }

            });

        factory(Activity::class, 5)
            ->create()
            ->each(function($activity) {
                //considerando que uma activity está só associada a um project
                //considerando as nossas seed para Projects => id's = [1,..,15]
                $projectData = Project::all()->random();
                while($projectData->status > 2 || !$projectData->submitted){
                    $projectData = Project::all()->random();
                }
                $activity->project_id = $projectData->id;

                //se tem manager associado escolhe um manager da lista de user se não escolhe um Diretor random
                if($projectData->users()->where('role', '<', '3')->count() > 1){
                    $activity->creator_id = $projectData->users()->get()->where('role', '<', '3')->random()->id;

                }else{
                    $activity->creator_id = User::all()->where('role', '=', '1')->random()->id;
                }

                //verificar se o projecto já foi concluído e simular que guardou um last_status na actividade
                if($projectData->status == 2){
                    $activity->last_status = $activity->status;
                    $activity->status = 2;
                }

                $activity->save();

                //ANULADO -> activity->users() só tem users do tipo utilizador
                //activity pode ter vários users associados
                //associar o user do tipo Director/Manager que criou a activity
                //$activity->users()->attach($activity->creator_id);

                //activity->users() só tem users do tipo utilizador
                //considerando que todos os Gestores do project tb estão associados à activity
                if($projectData->users()->count()){
                    foreach ($projectData->users()->get()->where('role', '<', '3') as $user) {
                        if(!$activity->users()->find($user->id)){
                            $activity->users()->attach($user->id);
                        }
                    }
                }

                //considerando as nossas seed para Advanced + Simple users => id's = [11,..,15]
                //projecto com 1 participantes além dos gestores
                //associar utilizadores á activity
                $participant1Id = User::all()->where('role', '=', '3')->random()->id;

                //pode sempre adicionar
                $activity->users()->attach($participant1Id);

                //associar utilizadores ao project
                if($projectData->users()->count() > 1){
                    if(!$projectData->users()->find($participant1Id)){
                        $projectData->users()->attach($participant1Id);
                    }
                } else{
                    $projectData->users()->attach($participant1Id);
                }

            });

        factory(Activity::class, 5)
            ->create()
            ->each(function($activity) {
                //ACTIVITIES SEM UTILIZADORES ASSOCIADOS E EM ESTADO PENDENTE
                //considerando que uma activity está só associada a um project
                //considerando as nossas seed para Projects => id's = [1,..,15]
                $projectData = Project::all()->random();
                while($projectData->status > 1 || !$projectData->submitted){
                    $projectData = Project::all()->random();
                }
                $activity->project_id = $projectData->id;

                //se tem manager associado escolhe um manager da lista de user se não escolhe um Diretor random
                if($projectData->users()->where('role', '<', '3')->count() > 1){
                    $activity->creator_id = $projectData->users()->get()->where('role', '<', '3')->random()->id;

                }else{
                    $activity->creator_id = User::all()->where('role', '=', '1')->random()->id;
                }

                //actividade pendente
                $activity->status = 0;

                //SEM SENTIDO PELO WHILE DA LINHA 90
                //verificar se o projecto já foi concluído e simular que guardou um last_status na actividade
                /*if($projectData->status == 2){
                    $activity->last_status = $activity->status;
                    $activity->status = 2;
                }*/

                $activity->save();

                //ANULADO -> activity->users() só tem users do tipo utilizador
                //activity pode ter vários users associados
                //associar o user do tipo Director/Manager que criou a activity
                //$activity->users()->attach($activity->creator_id);

                //activity->users() só tem users do tipo utilizador
                //considerando que todos os Gestores do project tb estão associados à activity
                if($projectData->users()->count()){
                    foreach ($projectData->users()->get()->where('role', '<', '3') as $user) {
                        if(!$activity->users()->find($user->id)){
                            $activity->users()->attach($user->id);
                        }
                    }
                }

            });

    }
}
