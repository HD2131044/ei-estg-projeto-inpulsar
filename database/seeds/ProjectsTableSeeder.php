<?php

use App\User;
use Illuminate\Database\Seeder;
use App\Project;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //projects sem Gestor
        factory(Project::class, 3)
            ->create()
            ->each(function($project) {
                //simulação do estado de submetido a aprovação
                $arrayTimesSubmitted = [1, 2, 3];
                if($project->status == 0){
                    $arrayTimesSubmitted_other = [0, 1, 2];
                    $arraySubmitted = [false, true];
                    $project->submitted = $arraySubmitted[array_rand($arraySubmitted)];
                    if($project->submitted){
                        $project->times_submitted = $arrayTimesSubmitted[array_rand($arrayTimesSubmitted)];
                    }else{
                        $project->times_submitted = $arrayTimesSubmitted_other[array_rand($arrayTimesSubmitted_other)];
                    }
                } elseif ($project->status > 0 && $project->status != 3){
                    $project->submitted = true;
                    $project->times_submitted = $arrayTimesSubmitted[array_rand($arrayTimesSubmitted)];
                } else { //estado cancelado
                    $project->submitted = true;
                    $project->times_submitted = $arrayTimesSubmitted[array_rand($arrayTimesSubmitted)];
                }
                $project->save();

            });

        factory(Project::class, 2)
            ->create()
            ->each(function($project) {
                //simulação do estado de submetido a aprovação
                $arrayTimesSubmitted = [1, 2, 3];
                if($project->status == 0){
                    $arrayTimesSubmitted_other = [0, 1, 2];
                    $arraySubmitted = [false, true];
                    $project->submitted = $arraySubmitted[array_rand($arraySubmitted)];
                    if($project->submitted){
                        $project->times_submitted = $arrayTimesSubmitted[array_rand($arrayTimesSubmitted)];
                    }else{
                        $project->times_submitted = $arrayTimesSubmitted_other[array_rand($arrayTimesSubmitted_other)];
                    }
                } elseif ($project->status > 0 && $project->status != 3){
                    $project->submitted = true;
                    $project->times_submitted = $arrayTimesSubmitted[array_rand($arrayTimesSubmitted)];
                } else { //estado cancelado
                    $project->submitted = true;
                    $project->times_submitted = $arrayTimesSubmitted[array_rand($arrayTimesSubmitted)];
                }
                $project->save();

                //o project pode ter vários users associados do tipo Gestor
                //considerando as nossas seed para Gestores => id's = [7,..,10]
                //projecto apenas com um Gestor do tipo diretor
                $project->users()->attach(User::get()->where('role', '=', 1)->random()->id);
            });

        factory(Project::class, 5)
            ->create()
            ->each(function($project) {
                //simulação do estado de submetido a aprovação

                $arrayTimesSubmitted = [1, 2, 3];
                if($project->status == 0){
                    $arrayTimesSubmitted_other = [0, 1, 2];
                    $arraySubmitted = [false, true];
                    $project->submitted = $arraySubmitted[array_rand($arraySubmitted)];
                    if($project->submitted){
                        $project->times_submitted = $arrayTimesSubmitted[array_rand($arrayTimesSubmitted)];
                    }else{
                        $project->times_submitted = $arrayTimesSubmitted_other[array_rand($arrayTimesSubmitted_other)];
                    }
                } elseif ($project->status > 0 && $project->status != 3){
                    $project->submitted = true;
                    $project->times_submitted = $arrayTimesSubmitted[array_rand($arrayTimesSubmitted)];
                } else { //estado cancelado
                    $project->submitted = true;
                    $project->times_submitted = $arrayTimesSubmitted[array_rand($arrayTimesSubmitted)];
                }
                $project->save();

                //projecto apenas com um Gestor do tipo diretor
                $project->users()->attach(User::get()->where('role', '=', 1)->random()->id);

                //o project pode ter vários users associados do tipo Gestor
                //considerando as nossas seed para Gestores => id's = [7,..,10]
                //projecto apenas vários Gestores já existentes
                $project->users()->attach(7);
                $project->users()->attach(8);
                $project->users()->attach(9);
            });

        factory(Project::class, 5)
            ->create()
            ->each(function($project) {
                //simulação do estado de submetido a aprovação
                $arrayTimesSubmitted = [1, 2, 3];
                if($project->status == 0){
                    $arrayTimesSubmitted_other = [0, 1, 2];
                    $arraySubmitted = [false, true];
                    $project->submitted = $arraySubmitted[array_rand($arraySubmitted)];
                    if($project->submitted){
                        $project->times_submitted = $arrayTimesSubmitted[array_rand($arrayTimesSubmitted)];
                    }else{
                        $project->times_submitted = $arrayTimesSubmitted_other[array_rand($arrayTimesSubmitted_other)];
                    }
                } elseif ($project->status > 0 && $project->status != 3){
                    $project->submitted = true;
                    $project->times_submitted = $arrayTimesSubmitted[array_rand($arrayTimesSubmitted)];
                } else { //estado cancelado
                    $project->submitted = true;
                    $project->times_submitted = $arrayTimesSubmitted[array_rand($arrayTimesSubmitted)];
                }
                $project->save();

                //o project pode ter vários users associados do tipo Gestor
                //considerando as nossas seed para Gestores => id's = [7,..,10]
                //projecto apenas vários Gestores
                //novos gestores
                $project->users()->saveMany(
                //users do tipo Gestor
                //projecto com 5 novos Gestores
                factory(User::class, 5)->create([
                    'password' => password_hash('123123123', PASSWORD_DEFAULT),
                    'role' => '2',
                    'activated' => '1'
                ]))->make();
            });
    }
}




